//
//  YGUserLoginModel.swift
//  Yogy
//
//  Created by apesnyakevich on 07.09.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

struct YGUserLoginModel
{
    var email: String?
    var password: String?
}

