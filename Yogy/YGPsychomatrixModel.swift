//
//  YGPsychomatrixModel.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 11/09/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

struct YGPsychomatrixModel {
   
    var colors = ["7DCE82", "FEB9E8", "EEE78A", "FF8360", "D2A8E2", "6397F6", "8384CE", "FFAD63"]
}
