//
//  YGDestinyCodeModel.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 07/09/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

enum YGDestinyCodeMode: Int {
    case Destiny = 0
    case Karma
    case Potential
    case All
}

class YGDestinyCodeModel: NSObject {
    
}
