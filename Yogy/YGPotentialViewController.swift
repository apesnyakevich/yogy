//
//  YGPotentialViewController.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 06/09/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGPotentialViewControllerDelegate {
    
}

class YGPotentialViewController: UIViewController, YGPotentialViewControllerDelegate {

    @IBOutlet fileprivate weak var tableView: UITableView!
    
    fileprivate var selectedCellIndex: Int = -1
    
    fileprivate var presenter: YGPotentialPresenterDelegate?
    var dataProvider: YGDataProviderProtocol?
    
    //MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        updatePresenter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    //MARK: - Private
    
    private func updatePresenter() {
        let model = YGPotentialModel()
        let presenter = YGPotentialPresenter(view: self, model: model, dataProvider: dataProvider)
        self.presenter = presenter
    }
    
    fileprivate func updateTableView() {
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
        })
    }
}

//MARK: - TableViewDataSource

extension YGPotentialViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var height: CGFloat = 95.0
        if (indexPath.row == selectedCellIndex) {
            height = 500.0
        }
        return height
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter!.cellCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String.className(YGPotentialTableViewCell.self), for: indexPath) as! YGPotentialTableViewCell
        
        let index = indexPath.row
        
        cell.updateCellWithModel((presenter?.potentialCellModelAt(index))!)
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if (selectedCellIndex != indexPath.row) {
            selectedCellIndex = indexPath.row
        }
        else {
            selectedCellIndex = -1
        }
        updateTableView()
        tableView.scrollToRow(at: indexPath, at: .top, animated: true)
    }
    
}
