//
//  YGFriendEditTableViewCell.swift
//  Yogy
//
//  Created by Anna on 10.08.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGFriendEditTableViewCellDelegate {
    func textFieldUpdate(_ value: String)
}

class YGFriendEditTableViewCell: UITableViewCell, YGFriendEditTableViewCellProtocol {

    @IBOutlet fileprivate weak var titleLabel: UILabel!
    @IBOutlet fileprivate weak var infoTextField: UITextField!
    
    var delegate: YGFriendEditTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.infoTextField.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
    }

    
    func updateInfoWith(_ model: YGFriendEditTableViewCellModel) {
        
        titleLabel.text = model.title!
        
        if (model.placeholder != nil) {
            infoTextField.text = model.placeholder!
        }
    }
    
    override func becomeFirstResponder() -> Bool {
        self.infoTextField.isUserInteractionEnabled = true
        self.infoTextField.becomeFirstResponder()
        
        return true
    }
    
    func updateTextFieldWith(_ text: String) {
        
        self.infoTextField.text = text
    }
    
}

extension YGFriendEditTableViewCell: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.resignFirstResponder()
        infoTextField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        self.resignFirstResponder()
        infoTextField.resignFirstResponder()
        delegate?.textFieldUpdate(infoTextField.text!)
        self.infoTextField.isUserInteractionEnabled = false

        return true
    }
    
}
