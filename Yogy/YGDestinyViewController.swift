//
//  YGDestinyViewController.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 06/09/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGDestinyViewControllerDelegate {
    
}

class YGDestinyViewController: UIViewController, YGDestinyViewControllerDelegate {

    @IBOutlet fileprivate weak var contentScrollView: UIScrollView!
    @IBOutlet fileprivate weak var contentView: UIView!
    
    @IBOutlet var contentViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet var contentViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet fileprivate weak var firstCircleView: UIView!
    @IBOutlet fileprivate weak var secondCircleView: UIView!
    @IBOutlet fileprivate weak var dottedView: UIView!
    @IBOutlet fileprivate weak var outerView: UIView!
    @IBOutlet fileprivate weak var iconBackgroundImageView: UIImageView!
    @IBOutlet fileprivate weak var iconImageView: UIImageView!
    
    @IBOutlet fileprivate weak var destinyView: UIView!
    @IBOutlet fileprivate weak var destinyUpperLabel: UILabel!
    @IBOutlet fileprivate weak var destinyTitleLabel: UILabel!
    @IBOutlet fileprivate weak var destinyTextLabel: UILabel!
    @IBOutlet fileprivate weak var destinyBackgroundView: UIView!
    
    @IBOutlet fileprivate weak var pastUpperLabel: UILabel!
    @IBOutlet fileprivate weak var pastTitleLabel: UILabel!
    @IBOutlet fileprivate weak var pastTextLabel: UILabel!
    @IBOutlet fileprivate weak var pastBackgroundView: UIView!
    @IBOutlet fileprivate weak var pastIconImageView: UIImageView!
    @IBOutlet fileprivate weak var pastCommentLabel: UILabel!
    
    @IBOutlet fileprivate weak var matrixButton: YGBorderedBlueButton!
    
    fileprivate var waveView: UIImageView?
    
    fileprivate weak var psychomatrixController: YGPsychomatrixViewController?
    
    fileprivate var presenter: YGDestinyPresenterDelegate?
    var dataProvider: YGDataProviderProtocol?
    
    //MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        contentScrollView.addSubview(contentView)
        contentScrollView.contentSize = CGSize(width: self.view.frame.size.width, height: contentView.frame.height)
        
        localizeUI()

        setupUI()
        
        updatePresenter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        updateUI()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == String.className(YGPsychomatrixViewController.self) && sender != nil) {
            psychomatrixController = segue.destination as? YGPsychomatrixViewController
        }
    }

    //MARK - Actions
    
    @IBAction func matrixButtonTapped(_ sender: UIButton) {
        if (sender.isSelected) {
            movePsychomatrixUp(false)
        }
        else {
            movePsychomatrixUp(true)
            psychomatrixController?.updateUI()
        }
    }
    
    //MARK: - Move Animations
    
    fileprivate func movePsychomatrixUp(_ direction: Bool) {
        matrixButton.isSelected = direction
        
        contentViewTopConstraint.isActive = direction
        contentViewBottomConstraint.isActive = !direction
        
        UIView.animate(withDuration: 0.4, animations: {
            self.view.layoutIfNeeded()
        }, completion: { (finished) in
            
        })
    }
    
    //MARK: - Private
    
    private func updatePresenter() {
        let model = YGDestinyModel()
        let presenter = YGDestinyPresenter(view: self, model: model, dataProvider: dataProvider)
        self.presenter = presenter
    }
    
    private func localizeUI() {
        matrixButton.setTitle(String.localize("Psychomatrix"), for: .normal)
        
        destinyUpperLabel.text = String.localize("Destiny")
        pastUpperLabel.text = String.localize("Past life experience")
    }
    
    private func setupUI() {
        outerView.borderWidth = 1.0
        outerView.borderColor = UIColor.customBlueColor()
        outerView.cornerRadius = outerView.frame.width/2
        
        destinyBackgroundView.cornerRadius = 6.0
        pastBackgroundView.cornerRadius = 6.0
        
        dottedView.addCircleBorderWithColor(UIColor.customBlueColor())
        firstCircleView.addCircleBorderWithColor(UIColor.customBlueColor())
        secondCircleView.addCircleBorderWithColor(UIColor.customBlueColor())
        
        setupWaveImageView()
    }
    
    private func setupWaveImageView() {
        waveView = UIImageView.init(image: UIImage.init(named: "image_wave"))
        
        iconBackgroundImageView.addSubview(waveView!)
        waveView?.frame = CGRect.init(origin: CGPoint.init(x: -2, y: 0), size: waveView!.size())
        iconBackgroundImageView.clipsToBounds = true
        iconBackgroundImageView.layer.cornerRadius = iconBackgroundImageView.frame.width / 2
        
        startWaveAnimation()
    }
    
    private func startWaveAnimation() {
        
        UIView.animate(withDuration: 20,
                       delay: 0,
                       options: [.repeat, .curveLinear],
                       animations: {
                        self.waveView?.frame = CGRect.init(origin: CGPoint.init(x: -(self.waveView!.frame.width * 2 / 3.1), y: 0), size: self.waveView!.size())
        })
        { (_) in}
    }
    
    private func startCirclesAnimation() {
        UIView.animate(withDuration: 3, delay: 0, options: [.repeat, .autoreverse, .curveEaseOut], animations: {
            self.firstCircleView.transform = CGAffineTransform.identity.scaledBy(x: 0.8, y: 0.8)
            self.secondCircleView.transform = CGAffineTransform.identity.scaledBy(x: 0.8, y: 0.8)
        }, completion: nil)
    }
    
    private func animateTitle() {
        destinyTitleLabel.alpha = 0
        var bottomConstraint: NSLayoutConstraint?
        
        for constraint in destinyView.constraints {
            if constraint.identifier == "bottomConstraint" {
                bottomConstraint  = constraint
                bottomConstraint?.constant += 30
                self.view.layoutIfNeeded()
            }
        }
        
        UIView.animate(withDuration: 1.0,
                       delay: 0.0,
                       options: .curveEaseInOut,
                       animations: {
                        self.destinyTitleLabel.alpha = 1.0
                        bottomConstraint?.constant -= 30
                        self.view.layoutIfNeeded()
        })
    }
    
    private func animateImageView() {
        animateViewAppearance(iconImageView, duration: 0.8, delay: 0.0, needsSizeChange: true)
        animateViewAppearance(iconBackgroundImageView, duration: 1.0, delay: 0.2)
        animateViewAppearance(outerView, duration: 1.0, delay: 0.4)
        animateViewAppearance(dottedView, duration: 1.0, delay: 0.6)
        animateViewAppearance(firstCircleView, duration: 1.0, delay: 0.7)
        animateViewAppearance(secondCircleView, duration: 1.0, delay: 0.8)
        
        startWaveAnimation()
        
        startCirclesAnimation()
    }
    
    private func animateViewAppearance(_ view: UIView,
                                       duration: Double,
                                       delay: Double) {
        
        self.animateViewAppearance(view, duration:duration, delay:delay, needsSizeChange: false)
    }
    
    private func animateViewAppearance(_ view: UIView,
                                       duration: Double,
                                       delay: Double,
                                       needsSizeChange: Bool,
                                       completion: ((Bool) -> Swift.Void)? = nil) {
        
        view.alpha = 0.0
        
        let width = view.frame.width
        let height = view.frame.height
        
        if (needsSizeChange) {
            updateViewWidthConstraintWithConstant(view, constant: 0.0)
            updateViewHeightConstraintWithConstant(view, constant: 0.0)
            self.view.layoutIfNeeded()
        }
        
        UIView.animate(withDuration: duration,
                       delay: TimeInterval(delay),
                       options: .curveEaseInOut,
                       animations: {
                        view.alpha = 1.0
                        
                        if (needsSizeChange) {
                            self.updateViewWidthConstraintWithConstant(view, constant: width)
                            self.updateViewHeightConstraintWithConstant(view, constant: height)
                            self.view.layoutIfNeeded()
                        }
        },
                       completion: {(finished) in
                        if (completion != nil) {
                            completion!(finished)
                        }
        })
    }
    
    private func updateViewWidthConstraintWithConstant(_ view: UIView, constant: CGFloat) {
        for constraint in view.constraints {
            if constraint.identifier == "widthConstraint" {
                constraint.constant = constant
            }
        }
    }
    
    private func updateViewHeightConstraintWithConstant(_ view: UIView, constant: CGFloat) {
        for constraint in view.constraints {
            if constraint.identifier == "heightConstraint" {
                constraint.constant = constant
            }
        }
    }
    
    private func updateUI() {
        iconImageView.image = presenter?.destinyImage()
        animateImageView()
        
        destinyTitleLabel.text = presenter?.destinyTitle()
        destinyTextLabel.text = presenter?.destinyDesctiption()
        animateTitle()
        
        pastTitleLabel.text = presenter?.pastTitle()
        pastTextLabel.text = presenter?.pastDescription()
        
        if (presenter?.isExperienceChecked())! {
            pastIconImageView.image = #imageLiteral(resourceName: "icon_check_active")
            pastCommentLabel.text = String.localize("Checked")
            pastBackgroundView.backgroundColor = UIColor.customGreenColor()
        }
        else {
            pastIconImageView.image = #imageLiteral(resourceName: "icon_check_inactive")
            pastCommentLabel.text = String.localize("Not checked")
            pastBackgroundView.backgroundColor = UIColor.customRedColor()
        }
    }

}
