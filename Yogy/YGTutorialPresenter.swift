//
//  YGTutorialPresenter.swift
//  Yogy
//
//  Created by Anna on 9/5/17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGTutorialPresenterDelegate {
    func buttonTitleAt(_ index: Int) -> String
    func tutorialTextAt(_ index: Int) -> String
}

class YGTutorialPresenter: YGTutorialPresenterDelegate {
    
    private unowned var view: YGTutorialViewControllerDelegate
    private var model: YGTutorialModel
    
    init(view: YGTutorialViewControllerDelegate, model: YGTutorialModel) {
        self.view = view
        self.model = model
        
        loadTutorial()
    }
    
    private func loadTutorial() {
       
        for i in 1...3 {
            let pageTutorial = String.localize("tutorial_page_\(i)")
            model.tutorialPages.append(pageTutorial)
        }
    }

    //MARK: - YGTutorialPresenterDelegate
    
    func buttonTitleAt(_ index: Int) -> String {
        var buttonTitle: String
        
        if (index < (model.tutorialPages.count - 1)) {
            buttonTitle = String.localize("Skip")
        }
        else {
            buttonTitle = String.localize("Continue")
        }
        return buttonTitle
    }
    
    func tutorialTextAt(_ index: Int) -> String {
        return model.tutorialPages[index]
    }
}
