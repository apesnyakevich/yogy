//
//  YGKarmaPointsView.swift
//  Yogy
//
//  Created by Anna on 20.08.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGKarmaPointsViewDelegate {
    func karmaViewRemoveKarmaPoint(_ view: YGKarmaPointsView)
}

class YGKarmaPointsView: UIView {
    
    var delegate: YGKarmaPointsViewDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        addTap()
    }
    
    func addKarmaPoint(positive: Bool) {
        
        let karmaPointImageView = UIImageView(frame: CGRect(origin: verifiedPoint(), size: randomSize()))
        
        karmaPointImageView.image = positive ? UIImage(named: "icon_positiveKarmaPoint") : UIImage(named: "icon_negativeKarmaPoint")
        
        addSubview(karmaPointImageView)
        
        karmaPointImageView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.3,
                       initialSpringVelocity: 0.1,
                       options: UIViewAnimationOptions.beginFromCurrentState,
                       animations: {
                        karmaPointImageView.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }

    private func verifiedPoint() -> CGPoint {
        return randomPoint()
    }
    
    private func randomPoint() -> CGPoint {
        
        let randomX = randomInRange(lo: Int(self.bounds.minX), hi: Int(self.frame.width - 57))
        let randomY = randomInRange(lo: Int(self.bounds.minY), hi: Int(self.frame.height - 57))
        let randomPoint = CGPoint(x: randomX, y: randomY)
        
        return randomPoint
    }
    
    private func randomSize() -> CGSize {
        
        let randomHeight = randomInRange(lo: 27, hi: 54)
        let randomSize = CGSize(width: randomHeight, height: randomHeight)
        
        return randomSize
    }
    
    private func randomInRange(lo: Int, hi : Int) -> Int {
        
        return lo + Int(arc4random_uniform(UInt32(hi - lo + 1)))
    }
}

extension YGKarmaPointsView {
    
    fileprivate func addTap() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(gesture:)))
        addGestureRecognizer(tap)
    }
    
    @objc fileprivate func handleTap(gesture: UITapGestureRecognizer) {
        
        for view in subviews {
            let location = gesture.location(in: self)
            if (view.frame.contains(location)) {
                view.removeFromSuperview()
                delegate?.karmaViewRemoveKarmaPoint(self)
                return
            }
        }
    }
}
