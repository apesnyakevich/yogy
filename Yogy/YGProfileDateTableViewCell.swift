//
//  YGProfileDateTableViewCell.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 05/08/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGProfileDateTableViewCellDelegate: class {
    func datePickerChanged(date:NSDate)
}

class YGProfileDateTableViewCell: UITableViewCell, YGProfileTableViewCellProtocol {

    @IBOutlet private weak var dateTitleLabel: UILabel!
    @IBOutlet private weak var dateValueLabel: UILabel!
    @IBOutlet private var dateTextField:UITextField!
    
    private var datePicker:UIDatePicker!
    var dataProvider: YGDataProviderProtocol?
    
    weak var delegate: YGProfileDateTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if(selected) {
            dateTextField.becomeFirstResponder()
        }
    }

    func updateInfoWithModel(model: YGProfileTableViewCellModel) {
        dateTitleLabel.text = model.title
        dateValueLabel.text = model.description
        
        self.setDatePicker()
    }
    
    func setDatePicker() {
        let customView:UIView = UIView (frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 160))
        customView.backgroundColor = UIColor.white
        
        datePicker = UIDatePicker(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 160))
        datePicker.datePickerMode = .date
        
        if (!(dateValueLabel.text ?? "").isEmpty) {
            datePicker.date = YGAppUtils.outputDateFormatter().date(from: dateValueLabel.text!)!
        }
//        datePicker.maximumDate = Date()
        
        let currentCalendar = Calendar.current
        
        var dateComponents = DateComponents()
        dateComponents.year = -100
        
        let minimumDate = (currentCalendar as NSCalendar).date(byAdding: dateComponents, to: Date(), options: [])
        datePicker.minimumDate = minimumDate

        customView.addSubview(datePicker)
        dateTextField.inputView = customView
        let doneButton:UIButton = UIButton (frame: CGRect.init(x: 100, y: 0, width: 100, height: 44))
        doneButton.setTitle(String.localize("Done").capitalized, for: UIControlState.normal)
        doneButton.addTarget(self, action: #selector(self.doneButtonTap), for: UIControlEvents.touchUpInside)
        doneButton.setTitleColor(UIColor.white, for: UIControlState.normal)
        doneButton.backgroundColor = UIColor.customBlueColor()
        dateTextField.inputAccessoryView = doneButton
    }
    
    func doneButtonTap() {
        delegate?.datePickerChanged(date: datePicker.date as NSDate)
        dateTextField.resignFirstResponder()
    }
    
}
