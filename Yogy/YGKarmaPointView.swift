//
//  YGKarmaView.swift
//  Yogy
//
//  Created by Anna on 17.08.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

struct YGKarmaPointViewModel {
    var text: String
    var isNegative: Bool
}

protocol YGKarmaViewDelegate {
    func addPositiveKarmaPoint()
    func addNegativeKarmaPoint()
}

class YGKarmaPointView: UIView {

    @IBOutlet private weak var textLabel: UILabel!
    @IBOutlet private weak var karmaImageView: UIImageView!
    
    var delegate: YGKarmaViewDelegate?
    
    private var isPositive = true
    
    func updateInfoWithModel(_ model: YGKarmaPointViewModel) {
        textLabel.text = model.text
        
        if (model.isNegative) {
            karmaImageView.image = #imageLiteral(resourceName: "image_karmaPoint_negative")
            isPositive = false
        } else {
            karmaImageView.image = #imageLiteral(resourceName: "image_karmaPoint_positive")
            isPositive = true
        }
    }
    
    @IBAction private func addKarmaPoint() {
        if (isPositive) {
            delegate?.addPositiveKarmaPoint()
        } else {
            delegate?.addNegativeKarmaPoint()
        }
    }

}
