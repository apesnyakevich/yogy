//
//  YGPsychomatrixPresenter.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 11/09/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGPsychomatrixPresenterDelegate {
    func cellCount() -> Int
    
    func cellColorAt(_ index: Int) -> UIColor
    func cellTitleAt(_ index: Int) -> String
    func cellMatrixValueAt(_ index: Int) -> Int
}

class YGPsychomatrixPresenter: YGPsychomatrixPresenterDelegate {

    private var view: YGPsychomatrixViewControllerDelegate
    private var model: YGPsychomatrixModel
    private var dataProvider: YGDataProviderProtocol?
    
    required init(view: YGPsychomatrixViewControllerDelegate, model: YGPsychomatrixModel, dataProvider: YGDataProviderProtocol?) {
        self.view = view
        self.model = model
        self.dataProvider = dataProvider
    }
    
    //MARK: - YGPsychomatrixPresenterDelegate
    
    func cellCount() -> Int {
        let birthDate = dataProvider?.currentUser()?.birthDate
        let matrix = YGAppUtils.psychoMatrix(birthDate!)
        
        return matrix.count
    }
    
    func cellMatrixValueAt(_ index: Int) -> Int {
        let birthDate = dataProvider?.currentUser()?.birthDate
        let matrix = YGAppUtils.psychoMatrix(birthDate!)
        
        return matrix[index]
    }
    
    func cellColorAt(_ index: Int) -> UIColor {
        return UIColor(hex: model.colors[index])
    }
    
    func cellTitleAt(_ index: Int) -> String {
        return String.localize("psychomatrix_title\(index)")
    }
}
