//
//  YGOrganizerViewController.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 31/07/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit
import SwipeCellKit

protocol YGOrganizerViewControllerDelegate {
}

class YGOrganizerViewController: UIViewController, YGOrganizerViewControllerDelegate {

    @IBOutlet fileprivate weak var contentView: UIView!
    @IBOutlet fileprivate weak var categoryView: YGOrganizerCategoryView!
    @IBOutlet fileprivate weak var adviceView: YGOrganizerAdviceView!
    
    @IBOutlet fileprivate weak var tasksTableView: UITableView!
    
    @IBOutlet fileprivate weak var newTaskForm: UIView!
    @IBOutlet fileprivate weak var newTaskFormBackgroundView: UIImageView!
    @IBOutlet fileprivate weak var taskBodyTextView: UITextView!
    
    @IBOutlet fileprivate var newTaskButtonSpaceToContainerConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate var categoryIconImageViewSpaceToContainerConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate weak var newTaskButton: UIButton!
    
    fileprivate var presenter: YGOrganizerPresenterDelegate?
    var dataProvider: YGDataProviderProtocol?
    
    var activeCategoryIndex: Int = 0
    
    private var isNewTaskFormActive = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = String.localize("Organizer")
   
        let leftButton: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "image_arrow_left"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.backButtonTapped(_:)))
        navigationItem.leftBarButtonItem = leftButton
        
        newTaskButton.setTitle(String.localize("New Goal"), for: .normal)
        
        tasksTableView.estimatedRowHeight = 60
        tasksTableView.rowHeight = UITableViewAutomaticDimension
        tasksTableView.isScrollEnabled = false
        
        updatePresenter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        addGestureRecognizers()
        
        updateInfo()
        
        if (activeCategoryIndex != 0) {
            removeSlideMenuGestures()
        }
    }

    
    //MARK: - Private
    
    fileprivate func updateInfo() {

        presenter?.setActiveCategoryIndex(activeCategoryIndex)
        
        updateTasksView()
        
        updateCategoryView()
        
        updateAdviceView()
    }
    
    fileprivate func updatePresenter() {
        
        let model = YGOrganizerModel()
        let presenter = YGOrganizerPresenter.init(view: self, model: model, dataProvider: dataProvider)
        self.presenter = presenter
    }
    
    fileprivate func updateCategoryView() {
        categoryView.updateCategoryInfo(title:presenter?.activeCategoryName(),
                                        hexColor: presenter?.activeCategoryHexColor(),
                                        iconName: presenter?.activeCategoryIconName(),
                                        progress: presenter?.activeCategoryProgress())
    }
    
    fileprivate func updateAdviceView() {
        adviceView.updateAdviceInfo(text: presenter?.activeAdviceText(),
                                    hexColor: presenter?.activeCategoryHexColor())
    }
    
    fileprivate func updateTasksView() {

        let categoryColor = UIColor(hex: (presenter?.activeCategoryHexColor())!)
        newTaskButton.borderColor = categoryColor
        
        presenter?.reloadTasks()
        updateTableView()
    }
    
    //MARK: -
    
    fileprivate func showNewTaskForm() {
        
        newTaskForm.alpha = 1
        newTaskFormBackgroundView.alpha = 1
        categoryIconImageViewSpaceToContainerConstraint.isActive = false
        newTaskButtonSpaceToContainerConstraint.isActive = true
        
        weak var weakSelf = self

        UIView.animate(withDuration: 0.5, animations: {
            weakSelf?.view.layoutIfNeeded()
        }, completion: { (_) in
            if weakSelf?.presenter?.tasksCount() == 0 {
                weakSelf?.taskBodyTextView.becomeFirstResponder()
            }
        })
    }
    
    fileprivate func hideNewTaskForm() {
        
        newTaskForm.alpha = 0
        newTaskFormBackgroundView.alpha = 0
        categoryIconImageViewSpaceToContainerConstraint.isActive = true
        newTaskButtonSpaceToContainerConstraint.isActive = false
        
        dismissKeyboard()
       
        weak var weakSelf = self
        UIView.animate(withDuration: 0.5, animations: {
           weakSelf?.view.layoutIfNeeded()
        })
    }
    
    fileprivate func updateTableView() {
        weak var weakSelf = self
        DispatchQueue.main.async {
            weakSelf?.tasksTableView.reloadData()
        }
    }
    
    fileprivate func moveViewUp() {
        removeSlideMenuGestures()
        newTaskButton.isSelected = true
        isNewTaskFormActive = true
        showNewTaskForm()
    }
    
    fileprivate func moveViewDown() {
        if(activeCategoryIndex == 0) {
            addSlideMenuGestures()
        }
        newTaskButton.isSelected = false
        isNewTaskFormActive = false
        hideNewTaskForm()
    }
    
    fileprivate func animateViewSwipeWithDirection(_ direction: String) {
        let animation = CATransition()
        animation.type = kCATransitionPush
        animation.subtype = direction
        animation.duration = 0.25
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        
        contentView.exchangeSubview(at: 0, withSubviewAt: 0)
        contentView.layer.add(animation, forKey: nil)
    }
    
    fileprivate func clearTexts() {
        taskBodyTextView.text = String.localize("Set your goal")
    }
    
    //MARK: - Gestures
    
    fileprivate func addGestureRecognizers() {
        addSwipeGestureRecognizer(targetView: categoryView, direction: .right)
        addSwipeGestureRecognizer(targetView: categoryView, direction: .left)
        addSwipeGestureRecognizer(targetView: adviceView, direction: .right)
        addSwipeGestureRecognizer(targetView: adviceView, direction: .left)
        addSwipeGestureRecognizer(targetView: contentView, direction: .up)
        addSwipeGestureRecognizer(targetView: contentView, direction: .down)
    }
    
    override func handleSwipeGesture(gesture: UISwipeGestureRecognizer) {
        
        if (gesture.direction == .up) {
            moveViewUp()
        }
        else if (gesture.direction == .down) {
            moveViewDown()
        }
        else if (gesture.direction == .right) {
            if (activeCategoryIndex > 0) {
                activeCategoryIndex -= 1
            }
            else {
                addSlideMenuGestures()
                return
            }
            animateViewSwipeWithDirection(kCATransitionFromLeft)
            updateInfo()
        }
        else if (gesture.direction == .left) {
            removeSlideMenuGestures()

            if (activeCategoryIndex < ((presenter?.categoriesCount())! - 1)) {
                activeCategoryIndex += 1
            }
            else {
                return
            }
            animateViewSwipeWithDirection(kCATransitionFromRight)
            updateInfo()
        }
    }
    //MARK: - Actions
    
    @IBAction func newTaskButtonPressed(_ sender: UIButton) {
        if (newTaskButton.isSelected) {
            moveViewDown()
        }
        else {
            moveViewUp()
        }
    }
    
    override func backButtonTapped(_ sender: Any) {
        moveViewDown()
        addSlideMenuGestures()
        activeCategoryIndex = 0
        self.toggleRight()
    }
    
}

//MARK: - SwipeTableViewCellDelegate

extension YGOrganizerViewController: SwipeTableViewCellDelegate {
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        weak var weakSelf = self
        
        let deleteAction = SwipeAction(style: .destructive, title: "") { action, indexPath in
            weakSelf?.presenter?.deleteTaskAt(indexPath.row)
            weakSelf?.tasksTableView.deleteRows(at: [indexPath], with: .automatic)
            weakSelf?.updateCategoryView()
        }
        
        // customize the action appearance
        deleteAction.backgroundColor = UIColor.customRedColor()
        deleteAction.image = UIImage(named: "icon_delete")
        
        return [deleteAction]
    }
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeTableOptions {
        var options = SwipeTableOptions()
        
        options.transitionStyle = .border
        return options
    }
}

//MARK: - TableViewDataSource

extension YGOrganizerViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter!.tasksCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String.className(YGOrganizerTaskTableViewCell.self), for: indexPath) as! YGOrganizerTaskTableViewCell
        
        let index = indexPath.row
        
        cell.updateTaskInfo(title: presenter?.taskTitleAt(index),
                            text: presenter?.taskTextAt(index),
                            status: (presenter?.taskStatusAt(index))!,
                            index: index)
        
        cell.cellDelegate = self
        cell.delegate = self
        
        tasksTableView.isScrollEnabled = true
        
        return cell
    }
}

//MARK: - UITextField & UITextView

extension YGOrganizerViewController: UITextViewDelegate, UITextFieldDelegate {
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        textView.text = ""
        return true
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            
            if (textView.text.length > 0) {
                presenter?.createTask(title: "", text: taskBodyTextView.text)
                updateTableView()
                updateCategoryView()
            }
            clearTexts()
            return false
            
        }
        return true
    }
}

//MARK: - YGOrganizerTableViewCellDelegate

extension YGOrganizerViewController: YGOrganizerTableViewCellDelegate {
    
    func titleChangedAt(_ index: Int, title: String?) {
        presenter?.updateTaskAt(index, title: title, text: nil)
        tasksTableView.reloadRows(at: [NSIndexPath(row: index, section: 0) as IndexPath], with: .automatic)
    }
    
    func textChangedAt(_ index: Int, text: String?) {
        presenter?.updateTaskAt(index, title: nil, text: text)
        tasksTableView.reloadRows(at: [NSIndexPath(row: index, section: 0) as IndexPath], with: .automatic)
    }
    
    func doneButtonTappedAt(_ index: Int) {
        presenter?.changeTaskStatusAt(index)
        updateCategoryView()
        tasksTableView.reloadRows(at: [NSIndexPath(row: index, section: 0) as IndexPath], with: .automatic)
    }
}
