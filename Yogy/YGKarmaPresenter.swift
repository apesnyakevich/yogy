//
//  YGKarmaPresenter.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 07/09/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGKarmaPresenterDelegate {
    func cellCount() -> Int
    
    func karmaDescriptionAt(_ index: Int) -> String
    func karmaImageAt(_ index: Int) -> UIImage
}

class YGKarmaPresenter: YGKarmaPresenterDelegate {

    private var view: YGKarmaViewControllerDelegate
    private var model: YGKarmaModel
    
    required init(view: YGKarmaViewControllerDelegate, model: YGKarmaModel) {
        self.view = view
        self.model = model
    }
    
    //MARK: - YGKarmaPresenterDelegate
    
    func cellCount() -> Int {
        return 13
    }
    
    func karmaDescriptionAt(_ index: Int) -> String {
        return String.localize("karma_task\(index)")
    }
    
    func karmaImageAt(_ index: Int) -> UIImage {
        return UIImage(named:(String(format: "icon_karma_task%d", index)))!
    }
}
