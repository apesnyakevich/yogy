//
//  YGThemeDayViewController.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 31/07/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

protocol YGThemeDayViewControllerDelegate: class {
    
}

class YGThemeDayViewController: UIViewController, YGThemeDayViewControllerDelegate {

    @IBOutlet fileprivate weak var yogyImageView: UIImageView!
    @IBOutlet fileprivate weak var swipeView: UIView!
    @IBOutlet weak var middleView: UIView!
    
    @IBOutlet fileprivate weak var adviceMeButton: YGBorderedBlueButton!
    
    @IBOutlet fileprivate weak var adviceButtonBottomConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate weak var yogyBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet fileprivate weak var bottomLeftCategoryView: YGThemeDayCategoryView!
    @IBOutlet fileprivate weak var topLeftCategoryView: YGThemeDayCategoryView!
    @IBOutlet fileprivate weak var centralCategoryView: YGThemeDayCategoryView!
    @IBOutlet fileprivate weak var topRightCategoryView: YGThemeDayCategoryView!
    @IBOutlet fileprivate weak var bottomRightCategoryView: YGThemeDayCategoryView!
    
    @IBOutlet var topViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet var topViewTopConstraint: NSLayoutConstraint!
    
    fileprivate var calendarViewController: YGCalendarViewController?
    
    fileprivate var isYogyLoaded: Bool = false
    fileprivate var isTutorialNeedShow: Bool = true
    
    fileprivate var categoryViews = [YGThemeDayCategoryView]()
    
    fileprivate var presenter: YGThemeDayPresenterDelegate?
    var dataProvider: YGDataProviderProtocol?
    
    //MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        if UIApplication.isFirstLaunch() {
            isTutorialNeedShow = true
        }
        
        self.categoryViews = [self.bottomLeftCategoryView, self.topLeftCategoryView, self.centralCategoryView, self.topRightCategoryView, self.bottomRightCategoryView]
                
        updatePresenter()
        
        slideMenuController()?.delegate = self
        adjustNavigationBar()
        adjustCategoryViews()
        
        hideCategoryViews()
        hideYogy()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (isTutorialNeedShow) {
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: String(describing: YGTutorialViewController.self))
            self.present(vc, animated: true, completion: {(_) in
                self.isTutorialNeedShow = false
            })
        }
        else {
            if (!isYogyLoaded) {
                hideYogy()
                moveAdviceButtonDown()
                moveYogyUp()
            }
            else {
                adjustCategoryViews()
                showCategoryViewsAnimated(true)
            }
        }
        addSlideMenuGestures()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == kOrganizerShowSegueIdentifier && sender != nil) {
            let organizer = segue.destination as! YGOrganizerViewController
            let categoryIndex = sender as! Int
            organizer.activeCategoryIndex = categoryIndex
        }
    }
    
    //MARK: - Private
    
    fileprivate func adjustNavigationBar() {
        
        let logo = UIImage(named: "image_logo")
        let topBarImageView = UIImageView(image:logo)
        self.navigationItem.titleView = topBarImageView
        
        self.addLeftBarButtonWithImage(UIImage(named:"icon_menu")!)
        
        let rightButton: UIBarButtonItem = UIBarButtonItem(image: UIImage(named:"icon_friends")!,
                                                           style: UIBarButtonItemStyle.plain,
                                                           target: self,
                                                           action: #selector(self.goToFriendList))
        navigationItem.rightBarButtonItem = rightButton
    }
    
    
    @objc private func goToFriendList() {
        let friendViewController = storyboard?.instantiateViewController(withIdentifier: String(describing: YGFriendListViewController.self)) as! YGFriendListViewController
        
        self.navigationController?.pushViewController(friendViewController, animated: true)
    }
    
    fileprivate func updatePresenter() {
        let model = YGThemeDayModel()
        let presenter = YGThemeDayPresenter.init(view: self, model: model, dataProvider: dataProvider)
        self.presenter = presenter
        self.presenter?.deleteOldTasks()
    }
    
    fileprivate func adjustCategoryViews() {
        
        for categoryView in categoryViews {
            let index = categoryViews.index(of: categoryView)!
            
            categoryView.delegate = self
            categoryView.updateCategoryInfo(index: index,
                                            hexColor: presenter?.categoryColorAt(index),
                                            iconName: presenter?.categoryIconAt(index),
                                            progress: presenter?.categoryProgressAt(index))
        }
    }
    
    fileprivate func loadCalendarViewController() {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        calendarViewController = storyboard.instantiateViewController(withIdentifier: String(describing: YGCalendarViewController.self)) as? YGCalendarViewController
        calendarViewController?.delegate = self
    }
    
    //MARK: - Actions
    
    @IBAction func adviceMeButtonTapped(_ sender: UIButton) {
        moveViewTopToMiddle()
    }

    //MARK: - Gestures
    
    fileprivate func addViewGestures() {
        addSwipeGestureRecognizer(targetView: swipeView, direction: .up)
        addSwipeGestureRecognizer(targetView: swipeView, direction: .left)
    }
    
    override func handleSwipeGesture(gesture: UISwipeGestureRecognizer) {
        
        if (gesture.view == swipeView) {
            if (gesture.direction == .left) {
                toggleRight()
            }
            else if (gesture.direction == .up) {
                moveViewTopToMiddle()
            }
        }
    }
}

//MARK: - SlideMenuControllerDelegate
extension YGThemeDayViewController: SlideMenuControllerDelegate {
    
    func rightDidClose() {
        adjustCategoryViews()
        showCategoryViewsAnimated(true)
    }
}

//MARK: - YGThemeDayCategoryViewDelegate

extension YGThemeDayViewController: YGThemeDayCategoryViewDelegate {
    
    internal func didSelectCategoryAt(_ index: Int) {
        let vc = (self.slideMenuController()?.rightViewController as! UINavigationController).viewControllers[0]
        
        (vc as! YGOrganizerViewController).activeCategoryIndex = index
        
        self.toggleRight()
    }
}

 //MARK: - View move animations

extension YGThemeDayViewController {
    
    fileprivate func moveViewTopToMiddle() {
        adviceMeButton.isSelected = true
        
        topViewTopConstraint.isActive = true
        topViewBottomConstraint.isActive = false
        middleView.isHidden = false
        swipeView.isHidden = true
        middleView.alpha = 0
        addCalendarViewControllerAsChild()

        UIView.animate(withDuration: 0.6, animations: {
            self.swipeView.alpha = 0
            self.middleView.alpha = 1
            self.view.layoutIfNeeded()

        }, completion: { (finished) in
        })
    }
    
    fileprivate func moveViewMiddleToTop() {
        adviceMeButton.isSelected = false
        
        topViewTopConstraint.isActive = false
        topViewBottomConstraint.isActive = true
        
        UIView.animate(withDuration: 0.6, animations: {
            self.swipeView.alpha = 1
            self.middleView.alpha = 0
            self.swipeView.isHidden = false
            self.view.layoutIfNeeded()
        }, completion: { (_) in
            self.middleView.isHidden = true
            self.removeCalendarViewControllerFromChild()

        })
    }
    
}

//MARK: - Yogy move animations

extension YGThemeDayViewController {
    
    fileprivate func hideCategoryViews() {
        for categoryView in self.categoryViews {
            categoryView.alpha = 0
            let _ = categoryView.top(220)
            let _ = categoryView.centerX(0)
        }
        
        self.swipeView.alpha = 0.0
    }
    
    fileprivate func showCategoryViewsAnimated(_ animated: Bool) {
        topLeftCategoryView.animateCategoryView(top: 70.0, centerX: -100.0, delay: 0.0, animated: animated)
        bottomRightCategoryView.animateCategoryView(top: 160, centerX: (view.width()*0.35), delay: 0.1, animated: animated)
        centralCategoryView.animateCategoryView(top: 0.0, centerX: 0.0, delay: 0.3, animated: animated)
        bottomLeftCategoryView.animateCategoryView(top: 160, centerX: -(view.width()*0.35), delay: 0.5, animated: animated)
        topRightCategoryView.animateCategoryView(top: 70.0, centerX: 100.0, delay: 0.6, animated: animated)
        
        swipeView.alpha = 1.0
        addViewGestures()
        
        isYogyLoaded = true
    }
    
    fileprivate func moveAdviceButtonDown() {
        UIView.animate(withDuration: 0.8,
                       delay: 0.1,
                       options: .curveEaseInOut,
                       animations: {
                        self.adviceButtonBottomConstraint.constant = 20.0
                        self.view?.layoutIfNeeded()
        })
    }
    
    fileprivate func moveAdviceButtonUp() {
        self.adviceButtonBottomConstraint.constant += self.adviceMeButton.frame.height/2
    }
    
    fileprivate func hideYogy() {
        self.yogyImageView.alpha = 0
        self.yogyBottomConstraint.constant = self.yogyImageView.frame.height/2
    }
    
    fileprivate func moveYogyUp() {
        UIView.animate(withDuration: 1.2,
                       delay: 0.1,
                       options: .curveEaseInOut,
                       animations: {
                        self.yogyBottomConstraint.constant = 10
                        self.yogyImageView.alpha = 1
                        self.view?.layoutIfNeeded()
        },
                       completion: {(finished) in
                        self.moveYogyDown()
        })
    }
    
    fileprivate func moveYogyDown() {
        UIView.animate(withDuration: 0.5,
                       delay: 0,
                       options: .curveEaseInOut,
                       animations: {
                        self.yogyBottomConstraint.constant = 20
                        self.view?.layoutIfNeeded()
        },
                       completion: {(finished) in
                        if (finished) {
                            self.showCategoryViewsAnimated(true)
                        }
        })
    }
}

//MARK: - Calendar View Controller
extension YGThemeDayViewController {
    
    fileprivate func addCalendarViewControllerAsChild() {
        loadCalendarViewController()
        
        addChildViewController(calendarViewController!)
        middleView.addSubview(calendarViewController!.view)
        calendarViewController!.view.backgroundColor = UIColor.clear
        calendarViewController!.view.frame = view.bounds
    }
    
    fileprivate func removeCalendarViewControllerFromChild() {
        calendarViewController?.removeFromParentViewController()
        calendarViewController?.view.removeFromSuperview()
        calendarViewController = nil
    }
}

//MARK: - YGCalendarViewControllerDelegate
extension YGThemeDayViewController: YGCalendarViewControllerDelegate {
    
    func calendarViewControllerWillHide(_ calendarViewController: YGCalendarViewController) {
        moveViewMiddleToTop()
    }
}
