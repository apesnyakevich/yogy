//
//  YGProfileTextTableViewCell.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 05/08/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

class YGProfileTextTableViewCell: UITableViewCell, YGProfileTableViewCellProtocol {

    @IBOutlet weak var descriptionLabel: UILabel!
    
    func updateInfoWithModel(model: YGProfileTableViewCellModel) {
        
        descriptionLabel.text = model.description
        if (model.color != nil) {
            descriptionLabel.textColor = model.color
        }
    }
}
