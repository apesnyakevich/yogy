//
//  YGProfileModel.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 05/08/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

enum YGProfileCellType: Int {
    case YGProfileCellTypeBirthdate = 0
    case YGProfileCellTypeSubscription
    case YGProfileCellTypeNotifications
    case YGProfileCellTypeSounds
    case YGProfileCellTypeRateApp
    case YGProfileCellTypeTermsConditions
    case YGProfileCellTypePrivacyPolicy
    case YGProfileCellTypeSignOut
    case YGProfileCellTypeCount
}

struct YGProfileModel {

}
