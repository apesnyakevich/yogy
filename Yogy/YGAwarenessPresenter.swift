//
//  YGAwarenessPresenter.swift
//  Yogy
//
//  Created by Anna on 17.08.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGAwarenessPresenterDelegate {
    func loadKarmaTableView()
    func cellCount() -> Int
    func modelFor(_ indexPath: IndexPath) -> YGKarmaInfoModel
    func isCellShowInfo(cellIndex: Int) -> Bool
    func showInfoByCellIndex(_ index: Int)
    func hideInfoByCellIndex(_ cellIndex: Int)
    
    func addNegativeKarmaPoint()
    func addPositiveKarmaPoint()
    
    func removePositiveKarmaPoint()
    func removeNegativeKarmaPoint()
}

class YGAwarenessPresenter: YGAwarenessPresenterDelegate {
    
    private var view: YGAwarenessViewControllerDelegate
    private var model: YGAwarenessModel
    private var dataProvider: YGDataProviderProtocol?
    
    required init(view: YGAwarenessViewControllerDelegate, model: YGAwarenessModel, dataProvider: YGDataProviderProtocol?) {
        self.view = view
        self.model = model
        self.dataProvider = dataProvider
        
        removeAllKarmaPoints()
        loadKarmaPoints()
    }
    
    private func loadKarmaPoints() {
        let countOfNegativeKarmaPoints = dataProvider?.karmaPointCountWith(false)
        let countOfPositiveKarmaPoints = dataProvider?.karmaPointCountWith(true)
        
        model.positiveKarmaPoints.removeAll()
        model.negativeKarmaPoints.removeAll()
        model.positiveKarmaPoints = dataProvider!.allKarmaPointsWith(true)
        model.negativeKarmaPoints = dataProvider!.allKarmaPointsWith(false)
        
        if (countOfNegativeKarmaPoints! > 0) {
            view.showNegativeKarmaPoints(countOfNegativeKarmaPoints!)
        }
        if (countOfPositiveKarmaPoints! > 0) {
            view.showPositiveKarmaPoints(countOfPositiveKarmaPoints!)
        }
    }
    
    private func removeAllKarmaPoints() {
        dataProvider?.deleteOldKarmaPoints()
    }
    
    func loadKarmaTableView() {
        for index in 0...17 {
            var karmaInfo = YGKarmaInfoModel()
            karmaInfo.positiveText = String.localize("karma_positive\(index)")
            karmaInfo.neutralText = String.localize("karma_neutral\(index)")
            karmaInfo.negativeText = String.localize("karma_negative\(index)")
            model.modelsForTableView.append(karmaInfo)
        }
    }
    
    func cellCount() -> Int {
        return model.modelsForTableView.count
    }
    
    func modelFor(_ indexPath: IndexPath) -> YGKarmaInfoModel{
        return model.modelsForTableView[indexPath.row]
    }
    
    func isCellShowInfo(cellIndex: Int) -> Bool {
        
        for index in model.showingInfoCellIndices {
            if (index == cellIndex) {
                return true
            }
        }
        
        return false
    }
    
    func showInfoByCellIndex(_ index: Int) {
        model.showingInfoCellIndices.append(index)
    }
    
    func hideInfoByCellIndex(_ cellIndex: Int) {
        
        for (index, currentIndex) in model.showingInfoCellIndices.enumerated() {
            if (cellIndex ==  currentIndex) {
                model.showingInfoCellIndices.remove(at: index)
            }
        }
    }

    func addNegativeKarmaPoint() {
       self.dataProvider?.insertKarmaPointWith(false)
    }
    
    func addPositiveKarmaPoint() {
        self.dataProvider?.insertKarmaPointWith(true)
    }
    
    func removePositiveKarmaPoint() {
        
        if (model.positiveKarmaPoints.count > 0) {
            dataProvider?.deleteKarmaPoint(model.positiveKarmaPoints.first!)
            model.positiveKarmaPoints.remove(at: 0)
        }
    }
    
    func removeNegativeKarmaPoint() {
        
        if (model.negativeKarmaPoints.count > 0) {
            dataProvider?.deleteKarmaPoint(model.negativeKarmaPoints.first!)
            model.negativeKarmaPoints.remove(at: 0)
        }
    }
}
