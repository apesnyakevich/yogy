//
//  YGPotentialTableCellDetailView.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 08/09/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

class YGPotentialTableCellDetailView: UIView {
    
    @IBOutlet fileprivate weak var iconImageView: UIImageView!
    @IBOutlet fileprivate weak var iconBackgroundImageView: UIImageView!
    @IBOutlet fileprivate weak var lineBorderView: UIView!
    @IBOutlet fileprivate weak var dottedBorderView: UIView!
    @IBOutlet fileprivate weak var progressView: YGCircleView!
    
    @IBOutlet fileprivate weak var descriptionLabel: UILabel!
    @IBOutlet fileprivate weak var descriptionBackgroundView: UIView!
    
    @IBOutlet fileprivate weak var commentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        progressView.cornerRadius = progressView.frame.width/2.0
        
        lineBorderView.cornerRadius = lineBorderView.frame.width/2.0
        lineBorderView.borderWidth = 1.0
        
        dottedBorderView.cornerRadius = dottedBorderView.frame.width/2.0
        
        descriptionBackgroundView.cornerRadius = 6.0
    }
    
    func updateViewWithModel(_ model: YGPotentialCellModel) {
        
        descriptionLabel.text = model.description
        
        if (!(model.comment ?? "").isEmpty) {
            commentLabel.isHidden = false
            commentLabel.text = model.comment
        }
        
        iconImageView.image = model.image()
        iconBackgroundImageView.image = model.backgroundImage()
        
        updateViewColor(model.color())
        
        animateProgressView(model.progress)
    }
    
    private func animateProgressView(_ progress: CGFloat) {
        progressView.animateCircle(duration: 0.5, progress: progress)
    }
    
    private func updateViewColor(_ color: UIColor) {
        
        let circle = CAShapeLayer()
        circle.path = UIBezierPath(roundedRect: dottedBorderView.bounds, cornerRadius: dottedBorderView.frame.width/2).cgPath
        circle.fillColor = UIColor.clear.cgColor
        circle.strokeColor = color.cgColor
        circle.lineWidth = 1;
        circle.lineDashPattern = [4, 2]
        dottedBorderView.layer.addSublayer(circle)
        
        lineBorderView.borderColor = color
        
        progressView.circleColor = color
        
        descriptionBackgroundView.backgroundColor = color
    }
}
