//
//  YGPotentialTableViewCell.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 08/09/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

class YGPotentialTableViewCell: UITableViewCell {

    @IBOutlet fileprivate weak var statusImageView: UIImageView!
    @IBOutlet fileprivate weak var backgroundImageView: UIImageView!
    @IBOutlet fileprivate weak var outerBorderView: UIView!
    @IBOutlet fileprivate weak var innerBorderView: UIView!
    @IBOutlet fileprivate weak var progressView: YGCircleView!
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    @IBOutlet fileprivate weak var detailView: YGPotentialTableCellDetailView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        outerBorderView.cornerRadius = outerBorderView.width()/2
        
        innerBorderView.borderWidth = 1.0
        innerBorderView.cornerRadius = innerBorderView.width()/2
        
        progressView.circleWidth = 5.0
    }

    func updateCellWithModel(_ model: YGPotentialCellModel) {
        titleLabel.text = model.title
        
        statusImageView.image = model.image()
        backgroundImageView.image = model.backgroundImage()
        
        detailView.frame = CGRect.init(origin: detailView.frame.origin, size: CGSize.init(width: self.frame.width, height: detailView.frame.height))
        
        detailView.updateViewWithModel(model)
        
        updateViewColor(model.color())
        
        animateProgressView(model.progress)
    }
    
    private func animateProgressView(_ progress: CGFloat) {
        progressView.animateCircle(duration: 0.5, progress: progress)
    }
    
    private func updateViewColor(_ color: UIColor) {
        
        let circle = CAShapeLayer()
        circle.path = UIBezierPath(roundedRect: outerBorderView.bounds, cornerRadius: outerBorderView.frame.width/2).cgPath
        circle.fillColor = UIColor.clear.cgColor
        circle.strokeColor = color.cgColor
        circle.lineWidth = 1;
        circle.lineDashPattern = [4, 2]
        outerBorderView.layer.addSublayer(circle)
        
        innerBorderView.borderColor = color
        
        progressView.circleColor = color
    }
}
