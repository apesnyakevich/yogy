//
//  YGKarmaPointModel+CoreDataProperties.swift
//  
//
//  Created by Andrew Pesnyakevich on 21/08/2017.
//
//

import Foundation
import CoreData


extension YGKarmaPointModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<YGKarmaPointModel> {
        return NSFetchRequest<YGKarmaPointModel>(entityName: "YGKarmaPointModel")
    }

    @NSManaged public var creationDate: NSDate?
    @NSManaged public var status: Bool

}
