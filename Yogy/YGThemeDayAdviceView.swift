//
//  YGThemeDayAdviceView.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 19/08/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

class YGThemeDayAdviceView: UIView {

    @IBOutlet fileprivate weak var dottedView: UIView!
    @IBOutlet fileprivate weak var outerView: UIView!
    @IBOutlet fileprivate weak var textBackgroundView: UIView!
    @IBOutlet fileprivate weak var backgroundImageView: UIImageView!
    @IBOutlet fileprivate weak var iconImageView: UIImageView!
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    @IBOutlet fileprivate weak var textLabel: UILabel!
    @IBOutlet fileprivate weak var firstCircleView: UIView!
    @IBOutlet fileprivate weak var secondCircleView: UIView!
    
    fileprivate var waveView: UIImageView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        outerView.borderWidth = 1.0
        outerView.borderColor = UIColor.customBlueColor()
        outerView.cornerRadius = outerView.frame.width/2
        
        textBackgroundView.cornerRadius = 6.0
        
        addCircleBorderToView(dottedView)
        addCircleBorderToView(firstCircleView)
        addCircleBorderToView(secondCircleView)
        
        setupWaveImageView()
    }
    
    func updateAdviceTitle(_ title: String?) {
        titleLabel.text = title
        
        animateTextView(titleLabel)
    }
    
    func updateAdviceText(_ text: String?) {
        textLabel.text = text
        
        animateViewAppearance(textLabel, duration: 1.0, delay: 0.0)
    }
    
    func updateAdviceImage(_ image: UIImage?) {
        iconImageView.image = image
        
        animateImageView()
    }
    
    func setTitleViewHidden(_ hidden: Bool) {
        titleLabel.isHidden = hidden
    }
    
    func setAdviceImageViewHidden(_ hidden: Bool) {
        dottedView.isHidden = hidden
    }
    
    private func addCircleBorderToView(_ view: UIView) {
        let circle = CAShapeLayer()
        circle.path = UIBezierPath(roundedRect: view.bounds, cornerRadius: view.frame.width/2).cgPath
        circle.fillColor = UIColor.clear.cgColor
        circle.strokeColor = UIColor.customBlueColor().cgColor
        circle.lineWidth = 1;
        circle.lineDashPattern = [4, 2]
        view.layer.addSublayer(circle)
    }
    
    private func animateImageView() {
        animateViewAppearance(iconImageView, duration: 0.8, delay: 0.0, needsSizeChange: true)
        animateViewAppearance(backgroundImageView, duration: 1.0, delay: 0.2)
        animateViewAppearance(outerView, duration: 1.0, delay: 0.4)
        animateViewAppearance(dottedView, duration: 1.0, delay: 0.6)
        animateViewAppearance(firstCircleView, duration: 1.0, delay: 0.7)
        animateViewAppearance(secondCircleView, duration: 1.0, delay: 0.8)
        
        startCirclesAnimation()
    }
    
    private func setupWaveImageView() {
        waveView = UIImageView.init(image: UIImage.init(named: "image_wave"))
        
        backgroundImageView.addSubview(waveView!)
        waveView?.frame = CGRect.init(origin: CGPoint.init(x: -2, y: 0), size: waveView!.size())
        backgroundImageView.clipsToBounds = true
        backgroundImageView.layer.cornerRadius = backgroundImageView.frame.width / 2
        
        startWaveAnimation()
    }
    
    private func startWaveAnimation() {
        
        UIView.animate(withDuration: 20,
                       delay: 0,
                       options: [.repeat, .curveLinear],
                       animations: {
                        self.waveView?.frame = CGRect.init(origin: CGPoint.init(x: -(self.waveView!.frame.width * 2 / 3.1), y: 0), size: self.waveView!.size())
        })
        { (_) in}
    }
    
    private func startCirclesAnimation() {
        UIView.animate(withDuration: 3, delay: 0, options: [.repeat, .autoreverse, .curveEaseOut], animations: {
            self.firstCircleView.transform = CGAffineTransform.identity.scaledBy(x: 0.7, y: 0.7)
            self.secondCircleView.transform = CGAffineTransform.identity.scaledBy(x: 0.7, y: 0.7)
        }, completion: nil)
    }
    
    private func animateTextView(_ view: UIView) {
        view.alpha = 0
        var bottomConstraint: NSLayoutConstraint?
        
        for constraint in self.constraints {
            if constraint.identifier == "bottomConstraint" {
                bottomConstraint  = constraint
                bottomConstraint?.constant += 30
                self.layoutIfNeeded()
            }
        }
        
        UIView.animate(withDuration: 1.0,
                       delay: 0.0,
                       options: .curveEaseInOut,
                       animations: {
                        view.alpha = 1.0
                        bottomConstraint?.constant -= 30
                        self.layoutIfNeeded()
        })
    }
    
    private func animateViewAppearance(_    view: UIView,
                                       duration: Double,
                                       delay: Double) {
        
        self.animateViewAppearance(view, duration:duration, delay:delay, needsSizeChange: false)
    }
    
    private func animateViewAppearance(_ view: UIView,
                                       duration: Double,
                                       delay: Double,
                                       needsSizeChange: Bool,
                                       completion: ((Bool) -> Swift.Void)? = nil) {
        
        view.alpha = 0.0
        
        let width = view.frame.width
        let height = view.frame.height
        
        if (needsSizeChange) {
            updateViewWidthConstraintWithConstant(view, constant: 0.0)
            updateViewHeightConstraintWithConstant(view, constant: 0.0)
            self.layoutIfNeeded()
        }
        
        UIView.animate(withDuration: duration,
                       delay: TimeInterval(delay),
                       options: .curveEaseInOut,
                       animations: {
                        view.alpha = 1.0
                        
                        if (needsSizeChange) {
                            self.updateViewWidthConstraintWithConstant(view, constant: width)
                            self.updateViewHeightConstraintWithConstant(view, constant: height)
                            self.layoutIfNeeded()
                        }
        },
                       completion: {(finished) in
                        if (completion != nil) {
                            completion!(finished)
                        }
        })
    }
    
    private func updateViewWidthConstraintWithConstant(_ view: UIView, constant: CGFloat) {
        for constraint in view.constraints {
            if constraint.identifier == "widthConstraint" {
                constraint.constant = constant
            }
        }
    }
    
    private func updateViewHeightConstraintWithConstant(_ view: UIView, constant: CGFloat) {
        for constraint in view.constraints {
            if constraint.identifier == "heightConstraint" {
                constraint.constant = constant
            }
        }
    }
}
