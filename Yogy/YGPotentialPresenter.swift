//
//  YGPotentialPresenter.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 08/09/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGPotentialPresenterDelegate {
    func cellCount() -> Int
    func potentialCellModelAt(_ index: Int) -> YGPotentialCellModel
}

class YGPotentialPresenter: YGPotentialPresenterDelegate {

    private var view: YGPotentialViewControllerDelegate
    private var model: YGPotentialModel
    private var dataProvider: YGDataProviderProtocol?
    
    required init(view: YGPotentialViewControllerDelegate, model: YGPotentialModel, dataProvider: YGDataProviderProtocol?) {
        self.view = view
        self.model = model
        self.dataProvider = dataProvider
    }
    
    //MARK: - YGPotentialPresenterDelegate
    
    func cellCount() -> Int {
        return 13
    }
    
    func potentialCellModelAt(_ index: Int) -> YGPotentialCellModel {
        
        let birthDate = dataProvider?.currentUser()?.birthDate
        var model = YGPotentialCellModel(status: YGPotentialCellModelStatus.None, progress: 0.0, title: "", description: "", comment: "")
        model.title = String.localize("potential_title\(index)")
        model.description = String.localize("potential\(index)")
        
        if (birthDate != nil) {
            if (index >= 0 && index < 13) {
                var matrix = [Int]()
                matrix = YGAppUtils.matrixArray(birthDate!)
                let count = matrix[index]
                
                if (index == 0) {
                    if(count == 0) {
                        model.status = YGPotentialCellModelStatus.Red
                        model.progress = 0.25
                    } else if(count == 1) {
                        model.status = YGPotentialCellModelStatus.Green
                        model.progress = 0.33
                    } else if(count == 2) {
                        model.status = YGPotentialCellModelStatus.Green
                        model.progress = 0.67
                    } else {
                        model.status = YGPotentialCellModelStatus.Green
                        model.progress = 1.0
                    }
                    
                } else if (index == 1 || index == 2) {
                    if(count == 0) {
                        model.status = YGPotentialCellModelStatus.Red
                        model.progress = 0.25
                    } else if(count == 1) {
                        model.status = YGPotentialCellModelStatus.Yellow
                        model.progress = 0.5
                    } else if(count == 2) {
                        model.status = YGPotentialCellModelStatus.Green
                        model.progress = 0.67
                        model.comment = String.localize("message_consumer_energy_environment")
                    } else if(count == 3) {
                        model.status = YGPotentialCellModelStatus.Green
                        model.progress = 1.0
                    } else if(count == 4) {
                        model.status = YGPotentialCellModelStatus.Green
                        model.progress = 1.0
                    } else {
                        model.status = YGPotentialCellModelStatus.Green
                        model.progress = 1.0
                        model.comment = String.localize("message_problem_excess_energy")
                    }
                    
                } else if (index == 3) {
                    if(count == 0) {
                        model.status = YGPotentialCellModelStatus.Red
                        model.progress = 0.25
                    } else if(count == 1) {
                        model.status = YGPotentialCellModelStatus.Green
                        model.progress = 0.5
                    } else {
                        model.status = YGPotentialCellModelStatus.Green
                        model.progress = 1.0
                    }
                    
                }
                    //number = 4,...,12
                else {
                    if(count == 0) {
                        model.status = YGPotentialCellModelStatus.Red
                        model.progress = 0.25
                    } else if(count == 1) {
                        model.status = YGPotentialCellModelStatus.Green
                        model.progress = 0.25
                    } else if(count == 2) {
                        model.status = YGPotentialCellModelStatus.Green
                        model.progress = 0.5
                    } else if(count == 3) {
                        model.status = YGPotentialCellModelStatus.Green
                        model.progress = 0.75
                    } else {
                        model.status = YGPotentialCellModelStatus.Green
                        model.progress = 1.0
                    }
                }
            }
        }
        
        return model
    }

}
