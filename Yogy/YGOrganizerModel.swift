//
//  YGOrganizerViewModel.swift
//  Yogy
//
//  Created by Anna on 31.07.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

class YGOrganizerModel: NSObject {
    
    var activeCategoryIndex: Int = 0
    
    var categories = [YGTaskCategoryModel]()
    
    var tasks = [YGTaskModel]()
}
