//
//  YGTaskCategoryModel+CoreDataProperties.swift
//  
//
//  Created by Andrew Pesnyakevich on 04/08/2017.
//
//

import Foundation
import CoreData


extension YGTaskCategoryModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<YGTaskCategoryModel> {
        return NSFetchRequest<YGTaskCategoryModel>(entityName: "YGTaskCategoryModel")
    }

    @NSManaged public var categoryIndex: Int32
    @NSManaged public var colorHex: String?
    @NSManaged public var iconName: String?
    @NSManaged public var title: String?
    @NSManaged public var tasks: NSSet?

}

// MARK: Generated accessors for tasks
extension YGTaskCategoryModel {

    @objc(addTasksObject:)
    @NSManaged public func addToTasks(_ value: YGTaskModel)

    @objc(removeTasksObject:)
    @NSManaged public func removeFromTasks(_ value: YGTaskModel)

    @objc(addTasks:)
    @NSManaged public func addToTasks(_ values: NSSet)

    @objc(removeTasks:)
    @NSManaged public func removeFromTasks(_ values: NSSet)

}
