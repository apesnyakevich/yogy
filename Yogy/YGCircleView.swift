//
//  CircleView.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 01/08/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

class YGCircleView: UIView {
    
    var circleWidth :CGFloat? {
        willSet {
            circleLayer.lineWidth = newValue!
        }
    }
    
    private var circleLayer: CAShapeLayer!
    
    private var _circleColor : UIColor?
    var circleColor: UIColor? {
        set {
            _circleColor = newValue
            circleLayer.strokeColor = newValue!.cgColor
        }
        get { return _circleColor}
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.backgroundColor = UIColor.clear
        
        // Use UIBezierPath as an easy way to create the CGPath for the layer.
        // The path should be the entire circle.
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: frame.size.width / 2.0, y: frame.size.height / 2.0), radius: (frame.size.width - 10)/2, startAngle: CGFloat(.pi * 1.5), endAngle: CGFloat(.pi * 3.5), clockwise: true)
        
        // Setup the CAShapeLayer with the path, colors, and line width
        circleLayer = CAShapeLayer()
        circleLayer.path = circlePath.cgPath
        circleLayer.fillColor = UIColor.clear.cgColor
        circleLayer.lineWidth = 10.0
        
        // Don't draw the circle initially
        circleLayer.strokeEnd = 0.0
        
        // Add the circleLayer to the view's layer's sublayers
        layer.addSublayer(circleLayer)
    }
    
    func animateCircle(duration: TimeInterval, progress: CGFloat) {
        
        // We want to animate the strokeEnd property of the circleLayer
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        
        // Set the animation duration appropriately
        animation.duration = duration
        
        // Animate from 0 (no circle) to 1 (progress)
        animation.fromValue = 0
        animation.toValue = progress
        
        // Do a linear animation (i.e. the speed of the animation stays the same)
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        
        // Set the circleLayer's strokeEnd property to 1.0 now so that it's the
        // right value when the animation ends.
        circleLayer.strokeEnd = progress
        
        // Do the actual animation
        circleLayer.add(animation, forKey: "animateCircle")
    }
}

