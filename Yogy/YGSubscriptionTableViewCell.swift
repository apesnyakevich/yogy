//
//  YGSubscriptionTableViewCell.swift
//  Yogy
//
//  Created by apesnyakevich on 04.09.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGSubscriptionTableViewCellDelegate: class {

}

class YGSubscriptionTableViewCell: UITableViewCell, YGSubscriptionTableViewCellProtocol {

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var subtitleLabel: UILabel!
    @IBOutlet private weak var costLabel: UILabel!
    @IBOutlet private weak var notesLabel: UILabel!
    @IBOutlet private weak var subscriptImageView: UIImageView!
    
    weak var delegate: YGSubscriptionTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func updateInfoWithModel(model: YGSubscriptionTableViewCellModel) {
        titleLabel.text = model.title
        subtitleLabel.text = model.subtitle
        costLabel.text = model.cost
        notesLabel.text = model.notes
        
        if (model.subscriptActive != nil) {
            if(model.subscriptActive)! {
                subscriptImageView.image = UIImage(named:"icon_status_active")
            }
            else {
                subscriptImageView.image = nil
            }
        }

    }
}
