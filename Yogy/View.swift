//
//  UIView.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 11/5/15.
//  Copyright © 2015 Yuji Hato. All rights reserved.
//

import UIKit

extension UIView {
    class func loadNib<T: UIView>(_ viewType: T.Type) -> T {
        let className = String.className(viewType)
        return Bundle(for: viewType).loadNibNamed(className, owner: nil, options: nil)!.first as! T
    }
    
    class func loadNib() -> Self {
        return loadNib(self)
    }
    
    func addCircleBorderWithColor(_ color: UIColor) {
        let circle = CAShapeLayer()
        circle.path = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.frame.width/2).cgPath
        circle.fillColor = UIColor.clear.cgColor
        circle.strokeColor = color.cgColor
        circle.lineWidth = 1;
        circle.lineDashPattern = [4, 2]
        self.layer.addSublayer(circle)
    }
}
