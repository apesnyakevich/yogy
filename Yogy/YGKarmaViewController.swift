//
//  YGKarmaViewController.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 06/09/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGKarmaViewControllerDelegate {
    
}

class YGKarmaViewController: UIViewController, YGKarmaViewControllerDelegate {

    @IBOutlet fileprivate weak var titleView: YGKarmaTitleView!
    @IBOutlet fileprivate weak var karmaTableView: UITableView!
    
    @IBOutlet private var tableToTopConstnaint: NSLayoutConstraint!
    @IBOutlet private var tableToBottomConstraint: NSLayoutConstraint!
    
    var presenter: YGKarmaPresenterDelegate?
    
    fileprivate var lastContentOffset: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        updatePresenter()
        
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        titleView.updateView()
    }
    
    //MARK: - Private
    
    private func updatePresenter() {
        let model = YGKarmaModel()
        let presenter = YGKarmaPresenter(view: self, model: model)
        self.presenter = presenter
    }
    
    private func setupTableView() {
        karmaTableView.estimatedRowHeight = 100
        karmaTableView.rowHeight = UITableViewAutomaticDimension
    }
    
    fileprivate func moveTableDown() {
        tableToTopConstnaint.isActive = false
        tableToBottomConstraint.isActive = true
        
        UIView.animate(withDuration: 0.5) {
            self.titleView.alpha = 1.0
            self.view.layoutIfNeeded()
        }
    }
    
    fileprivate func moveTableUp() {
        tableToTopConstnaint.isActive = true
        tableToBottomConstraint.isActive = false
        self.titleView.alpha = 0.0
        
        UIView.animate(withDuration: 0.5) {
            
            self.view.layoutIfNeeded()
        }
    }
    
    //MARK: - Gestures
    
    private func addGestureRecognizers() {
        addSwipeGestureRecognizer(targetView: view, direction: .down)
        addSwipeGestureRecognizer(targetView: view, direction: .up)
    }
    
    override func handleSwipeGesture(gesture: UISwipeGestureRecognizer) {
        
        if (gesture.direction == .down) {
            moveTableDown()
        } else if (gesture.direction == .up) {
            moveTableUp()
        }
    }
}

//MARK: - TableViewDataSource

extension YGKarmaViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter!.cellCount()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String.className(YGKarmaTableViewCell.self), for: indexPath) as! YGKarmaTableViewCell
        
        let index = indexPath.row
        
        cell.setDescription((presenter?.karmaDescriptionAt(index))!)
        cell.setIcon((presenter?.karmaImageAt(index))!)
        
        return cell
    }
}

//MARK: - UIScrollViewDelegate

extension YGKarmaViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (scrollView.contentOffset.y <= 20) {
            moveTableDown()
        }
        else if (self.lastContentOffset < scrollView.contentOffset.y) {
            moveTableUp()
        }
        
        // update the new position acquired
        self.lastContentOffset = scrollView.contentOffset.y
    }
}

