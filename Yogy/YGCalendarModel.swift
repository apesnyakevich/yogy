//
//  YGCalendarModel.swift
//  Yogy
//
//  Created by Anna on 27.08.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

enum YGThemeDayCalendarMode: Int {
    case Day = 0
    case Month
    case Year
    case All
}

class YGCalendarModel: NSObject {
    
    var activeCalendarModeIndex: Int = YGThemeDayCalendarMode.Day.rawValue
    
    var activeDate: NSDate = NSDate()
    
    var friendForCalculations: YGFriendModel?

}
