//
//  YGKarmaTableViewCell.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 08/09/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

class YGKarmaTableViewCell: UITableViewCell {

    @IBOutlet fileprivate weak var iconBorderView: UIView!
    @IBOutlet fileprivate weak var iconDotView: UIView!
    @IBOutlet fileprivate weak var iconView: UIImageView!
    @IBOutlet fileprivate weak var iconBackgroundView: UIImageView!
    @IBOutlet fileprivate weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        iconBackgroundView.cornerRadius = iconBackgroundView.frame.height/2.0
        
        iconBorderView.cornerRadius = iconBorderView.frame.height/2.0
        iconBorderView.borderWidth = 1.0
        iconBorderView.borderColor = UIColor.customBlueColor()
        
        let borderCircle = CAShapeLayer()
        borderCircle.path = UIBezierPath(roundedRect: iconDotView.bounds, cornerRadius: iconDotView.frame.width/2).cgPath
        borderCircle.fillColor = UIColor.clear.cgColor
        borderCircle.strokeColor = UIColor.customBlueColor().cgColor
        borderCircle.lineWidth = 1;
        borderCircle.lineDashPattern = [4, 2]
        iconDotView.layer.addSublayer(borderCircle)
    }
    
    func setDescription(_ description: String) {
        descriptionLabel.text = description
        setNeedsLayout()
    }
    
    func setIcon(_ icon: UIImage) {
        iconView.image = icon
    }

}
