//
//  YGFriendListPresenter.swift
//  Yogy
//
//  Created by Anna on 07.08.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGFriendListPresenterDelegate {
    func loadFriendList()
    func friendsCount() -> Int
    func friendBy(_ index: Int) -> YGFriendModel?
    func removeFriendBy(_ index: Int)
    
    func editFriendBy(_ index: IndexPath)
    func modelForEditing() -> YGFriendModel?
    func selectFriendBy(_ indexPath: IndexPath)
    func currentFriend() -> YGFriendModel
}

class YGFriendListPresenter {

    fileprivate var view: YGFriendListViewControllerDelegate
    fileprivate var model: YGFriendListModel
    fileprivate var dataProvider: YGDataProviderProtocol?
    
    init(view: YGFriendListViewControllerDelegate, model: YGFriendListModel, dataProvider: YGDataProviderProtocol?) {
        self.view = view
        self.model = model
        self.dataProvider = dataProvider
    }
}

extension YGFriendListPresenter: YGFriendListPresenterDelegate {
    
    func loadFriendList() {
        if (dataProvider != nil) {
            model.friendList = dataProvider!.allFriends()
           
            view.updateInfo()
        }
    }
    
    func friendsCount() -> Int {
        return model.friendList.count
    }
    
    func friendBy(_ index: Int) -> YGFriendModel? {
        if (index < model.friendList.count) {
            return model.friendList[index]
        }
        return nil
    }
    
    func removeFriendBy(_ index: Int) {
        var fullName = String()
        if (model.friendList[index].name != nil && model.friendList[index].surname != nil) {
            fullName = "\(model.friendList[index].name!) \(model.friendList[index].surname!)"
        } else if (model.friendList[index].name != nil && model.friendList[index].surname == nil) {
            fullName = "\(model.friendList[index].name!)"
        } else if (model.friendList[index].name == nil && model.friendList[index].surname != nil) {
            fullName = "\(model.friendList[index].surname!)"
        } else if (model.friendList[index].name == nil && model.friendList[index].surname == nil) {
            fullName = "Unknown"
        }
        
        UserDefaults.standard.removeObject(forKey: "\(fullName)")

        dataProvider?.deleteFriend(friend: friendBy(index)!)
        model.friendList.remove(at: index)
        view.updateInfo()
    }
    
    func editFriendBy(_ index: IndexPath) {
        model.friendModelForEditing = model.friendList[index.row]
    }
    
    func modelForEditing() -> YGFriendModel? {
        let model = self.model.friendModelForEditing
        self.model.friendModelForEditing = nil
        return model
    }
    
    func selectFriendBy(_ indexPath: IndexPath) {
        let friend = model.friendList[indexPath.row]
        
        model.currentFriend = friend
    }
    
    func currentFriend() -> YGFriendModel {
        return model.currentFriend!
    }
}
