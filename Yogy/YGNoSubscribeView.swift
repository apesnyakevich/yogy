//
//  YGNoSubscribeView.swift
//  Yogy
//
//  Created by Anna on 07.08.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGNoSubscribeViewDelegate {
    func subscribeButtonDidPressed()
}

class YGNoSubscribeView: UIView {
    
    var delegate: YGNoSubscribeViewDelegate?

    @IBAction func subscribeButtonDidPressed(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        delegate?.subscribeButtonDidPressed()
    }

}
