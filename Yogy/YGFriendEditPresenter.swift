//
//  YGFriendEditPresenter.swift
//  Yogy
//
//  Created by Anna on 10.08.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGFriendEditPresenterDelegate {
    func rowCount() -> Int
    func updateFriend()
    func friend(_ friend: YGFriendModel?)
    func friendPhoto() -> UIImage?
    func cellSelectedAt(_ indexPath: IndexPath)
    func updateFullName(_ value: String)
    func updateFriendPhoto(_ photo: UIImage)
    func updateDate(_ date: NSDate)
    func cellIdentifierAt(_ indexPath: IndexPath) -> String
    func cellModelAt(_ indexPath: IndexPath) -> YGFriendEditTableViewCellModel
}

class YGFriendEditPresenter: YGFriendEditPresenterDelegate {

    private unowned let view: YGFriendEditViewControllerDelegate
    private var model: YGFriendEditModel
    private var dataProvider: YGDataProviderProtocol?
    
    init(view: YGFriendEditViewControllerDelegate, model: YGFriendEditModel, dataProvider: YGDataProviderProtocol?) {
        self.view = view
        self.model = model
        self.dataProvider = dataProvider
    }
    
    
    func friend(_ friend: YGFriendModel?) {
        model.currentFriend = friend
    }
    
    func rowCount() -> Int {
        return YGFriendEditCellType.YGFriendEditCellTypeCount.rawValue
    }
    
    func cellModelAt(_ indexPath: IndexPath) -> YGFriendEditTableViewCellModel {
        var model = YGFriendEditTableViewCellModel()
        
        switch indexPath.row {
        case YGFriendEditCellType.YGFriendEditCellTypeName.rawValue:
            
            model.title = String.localize("Name")
            model.isDate = false
            
            if (self.model.currentFriend != nil) {
                if (self.model.currentName != nil) {
                    model.placeholder = "\((self.model.currentName!))"
                } else {
                    model.placeholder = "\((self.model.currentFriend!.name)!)"
                }
            }
            break
        case YGFriendEditCellType.YGFriendEditCellTypeSurname.rawValue:
            
            model.title = String.localize("Surname")
            model.isDate = false
            
            if (self.model.currentFriend != nil) {
                if (self.model.currentSurname != nil) {
                    model.placeholder = "\((self.model.currentSurname!))"
                } else {
                    model.placeholder = "\((self.model.currentFriend!.surname)!)"
                }
            }
            break
        case YGFriendEditCellType.YGFriendEditCellTypeBirthdate.rawValue:
            
            model.title = String.localize("Birth Date")
            model.isDate = true
            
            if (self.model.currentFriend != nil) {
                let dateFormatter = YGAppUtils.outputDateFormatter()
                
                if (self.model.currentFriend!.birthDate != nil) {
                    if (self.model.currentDate != nil) {
                        model.placeholder = "\((dateFormatter.string(for: self.model.currentDate!)!))"
                    } else {
                        model.placeholder = "\((dateFormatter.string(for: self.model.currentFriend!.birthDate))!)"
                    }
                }
                
            }
            break
        default:
            break
        }
        
        return model
    }
    
    func cellIdentifierAt(_ indexPath: IndexPath) -> String {
        
        var identifier = String()
        
        switch indexPath.row {
        case YGFriendEditCellType.YGFriendEditCellTypeBirthdate.rawValue:
            
            identifier = String(describing: YGFriendEditBirthdateTableViewCell.self)
            break
        case YGFriendEditCellType.YGFriendEditCellTypeName.rawValue,
             YGFriendEditCellType.YGFriendEditCellTypeSurname.rawValue:
            
            identifier = String(describing: YGFriendEditTableViewCell.self)
            break
        default:
            break
        }
        
        return identifier
    }
    
    func cellSelectedAt(_ indexPath: IndexPath) {
        model.selectedCellIndex = indexPath.row
    }
    
    func friendPhoto() -> UIImage? {
        if (model.currentPhoto != nil) {
            return model.currentPhoto
        } else if (model.currentFriend != nil) {
            let image = YGAppUtils.getImagePhotoWith(model.currentFriend!.photoPath!)
            return image
        } else {
            return nil
        }
    }
    
    func updateFullName(_ value: String) {
        if (model.selectedCellIndex == YGFriendEditCellType.YGFriendEditCellTypeName.rawValue) {
            updateName(value)
        } else if (model.selectedCellIndex == YGFriendEditCellType.YGFriendEditCellTypeSurname.rawValue) {
            updateSurname(value)
        }
    }
    
    func updateDate(_ date: NSDate) {
        model.currentDate = date
    }
    
    private func updateName(_ name: String) {
        model.currentName = name
    }
    
    private func updateSurname(_ surname: String) {
        model.currentSurname = surname
    }
    
    func updateFriendPhoto(_ photo: UIImage) {
        model.currentPhoto = photo
    }
    
    func updateFriend() {
        if (isCorrectInfo()) {
            if (model.currentFriend != nil) {
                updateCurrentFriend()
            } else {
                var photoPath: String
                if (model.currentPhoto != nil) {
                    photoPath = "\(model.currentName!) \(model.currentSurname!)"
                    photoPath = YGAppUtils.saveImageToFile(photoPath, image: model.currentPhoto!)
                } else {
                    photoPath = kUserDummyPhoto //"image_dummy_user"
                }
                
                dataProvider?.insertFriend(name: model.currentName,
                                           surname: model.currentSurname,
                                           photoPath: photoPath,
                                           birthday: model.currentDate)
            }
            view.backToFriendList()
        }
    }
    
    private func isCorrectInfo() -> Bool {
        if (model.currentFriend == nil) {
            if (model.currentName == nil) {
                view.showAlertWith(String.localize("Please enter name"))
                return false
            } else if (model.currentSurname == nil) {
                view.showAlertWith(String.localize("Please enter surname"))
                return false
            } else if (model.currentDate == nil) {
                view.showAlertWith(String.localize("Please enter birthday"))
                return false
            }
            return true
        } else {
            if (model.currentName != nil && model.currentName!.isEmpty) {
                view.showAlertWith(String.localize("Please enter name"))
                return false
            } else if (model.currentSurname != nil && model.currentSurname!.isEmpty) {
                view.showAlertWith(String.localize("Please enter surname"))
                return false
            } 
            return true
        }
        
    }
    
    private func updateCurrentFriend() {
        let name:String? = model.currentName
        let surname:String? = model.currentSurname
        let date: NSDate? = model.currentDate != nil ? model.currentDate : nil
        
        var photoPath: String? = nil
        
        if (name != nil && surname != nil) {
            photoPath = "\(name!) \(surname!)"
        } else if (name != nil && surname == nil) {
            photoPath = "\(name!) \(model.currentFriend!.surname!)"
        } else if (name == nil && surname != nil) {
            photoPath = "\(model.currentFriend!.name!) \(surname!)"
        } else if (name == nil && surname == nil) {
            photoPath = "\(model.currentFriend!.name!) \(model.currentFriend!.surname!)"
        }

        if (model.currentPhoto != nil && photoPath != nil) {
            //UserDefaults.standard.removeObject(forKey: "\(model.currentFriend!.name!) \(model.currentFriend!.surname!)")
            //YGAppUtils.setImagePhotoForKey(photoPath!, image: model.currentPhoto!)
            photoPath = YGAppUtils.saveImageToFile(photoPath!, image: model.currentPhoto!)
        } else {
//            let image = YGAppUtils.getImagePhotoForKey("\(model.currentFriend!.name!) \(model.currentFriend!.surname!)")
//            UserDefaults.standard.removeObject(forKey: "\(model.currentFriend!.name!) \(model.currentFriend!.surname!)")
//
//            YGAppUtils.setImagePhotoForKey(photoPath!, image: image)
        }
        
        
        dataProvider?.updateFriend(friend: model.currentFriend!, name: name, surname: surname, photoPath: photoPath, birthday: date)
    }
}
