//
//  YGOrganizerCategoryView.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 09/08/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

class YGOrganizerCategoryView: UIView {

    @IBOutlet fileprivate weak var progressView: YGCircleView!
    @IBOutlet fileprivate weak var dottedView: UIView!
    @IBOutlet fileprivate weak var outerView: UIView!
    @IBOutlet fileprivate weak var innerView: UIView!
    @IBOutlet fileprivate weak var iconImageView: UIImageView!
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    
    private var circle = CAShapeLayer()
    
    private var progress: CGFloat? = 0.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        outerView.borderWidth = 1.0
        outerView.cornerRadius = outerView.frame.width/2
        
        innerView.cornerRadius = innerView.frame.width/2
    }
    
    func updateCategoryInfo(title: String?, hexColor: String?, iconName: String?, progress: CGFloat?) {
        
        titleLabel.text = title
        
        if (!(iconName ?? "").isEmpty) {
            iconImageView.image = UIImage(named: iconName!)
        }
        
        if (!(hexColor ?? "").isEmpty) {
            self.updateViewColor(hexColor!)
        }
        
        animateViews()
        
        self.progress = progress
    }
    
    private func updateViewColor(_ hex: String) {
        
        titleLabel.textColor = UIColor(hex: hex)
        
        circle = CAShapeLayer()
        circle.path = UIBezierPath(roundedRect: dottedView.bounds, cornerRadius: dottedView.frame.width/2).cgPath
        circle.fillColor = UIColor.clear.cgColor
        circle.strokeColor = UIColor(hex: hex, alpha: 0.8).cgColor
        circle.lineWidth = 1;
        circle.lineDashPattern = [4, 2]
        circle.removeFromSuperlayer()
        dottedView.layer.addSublayer(circle)
        
        outerView.borderColor = UIColor(hex: hex, alpha: 0.8)
        
        innerView.backgroundColor = UIColor(hex: hex, alpha: 0.2)
        
        progressView.circleColor = UIColor(hex: hex)
    }
    
    
    private func animateProgressView(_ progress: CGFloat) {
        progressView.isHidden = false
        progressView.animateCircle(duration: 0.4, progress: progress)
    }
    
    private func animateViews() {
        
        weak var weakSelf = self
        progressView.isHidden = true
        
        animateTitleView()
        
        animateViewAppearance(iconImageView, duration: 0.8, delay: 0.2, needsSizeChange: true)
        animateViewAppearance(innerView, duration: 1.0, delay: 0.4)
        animateViewAppearance(outerView, duration: 1.0, delay: 0.6)
        animateViewAppearance(dottedView, duration: 1.0, delay: 0.8, needsSizeChange: false) { (finished) in
            if (finished) {
                weakSelf?.animateProgressView((weakSelf?.progress!)!)
            }
        }
    }
    
    private func animateTitleView() {
        titleLabel.alpha = 0
        var bottomConstraint: NSLayoutConstraint?
        
        for constraint in self.constraints {
            if constraint.identifier == "bottomConstraint" {
                bottomConstraint  = constraint
                bottomConstraint?.constant += 30
                self.layoutIfNeeded()
            }
        }
        
        UIView.animate(withDuration: 1.0,
                       delay: 0.0,
                       options: .curveEaseInOut,
                       animations: {
                        self.titleLabel.alpha = 1.0
                        bottomConstraint?.constant -= 30
                        self.layoutIfNeeded()
                        })
    }
    
    private func animateViewAppearance(_    view: UIView,
                                        duration: Double,
                                        delay: Double) {
        
        self.animateViewAppearance(view, duration:duration, delay:delay, needsSizeChange: false)
    }
    
    private func animateViewAppearance(_ view: UIView,
                                     duration: Double,
                                        delay: Double,
                              needsSizeChange: Bool,
                                   completion: ((Bool) -> Swift.Void)? = nil) {
        
        view.alpha = 0.0
        
        let width = view.frame.width
        let height = view.frame.height
    
        if (needsSizeChange) {
            updateViewWidthConstraintWithConstant(view, constant: 0.0)
            updateViewHeightConstraintWithConstant(view, constant: 0.0)
            self.layoutIfNeeded()
        }
        
        UIView.animate(withDuration: duration,
                       delay: TimeInterval(delay),
                       options: .curveEaseInOut,
                       animations: {
                        view.alpha = 1.0
                        
                        if (needsSizeChange) {
                            self.updateViewWidthConstraintWithConstant(view, constant: width)
                            self.updateViewHeightConstraintWithConstant(view, constant: height)
                            self.layoutIfNeeded()
                        }
        },
                       completion: {(finished) in
                        if (completion != nil) {
                            completion!(finished)
                        }
        })
    }
    
    private func updateViewWidthConstraintWithConstant(_ view: UIView, constant: CGFloat) {
        for constraint in view.constraints {
            if constraint.identifier == "widthConstraint" {
                constraint.constant = constant
            }
        }
    }
    
    private func updateViewHeightConstraintWithConstant(_ view: UIView, constant: CGFloat) {
        for constraint in view.constraints {
            if constraint.identifier == "heightConstraint" {
                constraint.constant = constant
            }
        }
    }
}
