//
//  YGAwarenessViewController.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 31/07/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGAwarenessViewControllerDelegate {
    func showPositiveKarmaPoints(_ count: Int)
    func showNegativeKarmaPoints(_ count: Int)
}

class YGAwarenessViewController: UIViewController {
    
    @IBOutlet fileprivate weak var tableView: UITableView!
    @IBOutlet fileprivate weak var negativeKarmaPointsView: YGKarmaPointsView!
    @IBOutlet fileprivate weak var positiveKarmaPointsView: YGKarmaPointsView!
    
    @IBOutlet private var tableToTopConstnaint: NSLayoutConstraint!
    @IBOutlet private var tableToBottomConstraint: NSLayoutConstraint!
    
    var dataProvider: YGDataProviderProtocol?
    var presenter: YGAwarenessPresenterDelegate?
    
    fileprivate var lastContentOffset: CGFloat = 0
    
    //MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        updatePresenter()
        setupTableView()

        adjustNavigationBar()
        addGestureRecognizers()
        
        presenter?.loadKarmaTableView()
        
        negativeKarmaPointsView.delegate = self
        positiveKarmaPointsView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
        self.addOnlyLeftGestures()
    }
    
    //MARK: - Private
    
    private func updatePresenter() {
        let model = YGAwarenessModel()
        let presenter = YGAwarenessPresenter(view: self, model: model, dataProvider: dataProvider)
        self.presenter = presenter
    }
    
    private func setupTableView() {
        tableView.estimatedRowHeight = 60
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    fileprivate func updateTableView() {
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
        })
    }
    
    fileprivate func adjustNavigationBar() {
        navigationItem.title = String.localize("Awareness")
        
        self.addLeftBarButtonWithImage(UIImage(named:"icon_menu")!)
    }
    
    fileprivate func moveTableDown() {
        tableToTopConstnaint.isActive = false
        tableToBottomConstraint.isActive = true
        
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    fileprivate func moveTableUp() {
        tableToTopConstnaint.isActive = true
        tableToBottomConstraint.isActive = false
        
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    //MARK: - Gestures
    
    private func addGestureRecognizers() {
        addSwipeGestureRecognizer(targetView: view, direction: .down)
        addSwipeGestureRecognizer(targetView: view, direction: .up)
    }
    
    override func handleSwipeGesture(gesture: UISwipeGestureRecognizer) {
        
        if (gesture.direction == .down) {
            moveTableDown()
        } else if (gesture.direction == .up) {
            moveTableUp()
        }
    }
}

//MARK: - YGAwarenessViewControllerDelegate

extension YGAwarenessViewController: YGAwarenessViewControllerDelegate {
    func showNegativeKarmaPoints(_ count: Int) {
        for _ in 0...count - 1 {
            negativeKarmaPointsView.addKarmaPoint(positive: false)
        }
    }
    
    func showPositiveKarmaPoints(_ count: Int) {
        for _ in 0...count - 1 {
            positiveKarmaPointsView.addKarmaPoint(positive: true)
        }
    }
}

//MARK: - UIScrollViewDelegate

extension YGAwarenessViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (scrollView.contentOffset.y <= 20) {
            moveTableDown()
        }
        else if (self.lastContentOffset < scrollView.contentOffset.y) {
            moveTableUp()
        }
        
        // update the new position acquired
        self.lastContentOffset = scrollView.contentOffset.y
    }
}

//MARK: - UITableViewDelegate, UITableViewDataSource

extension YGAwarenessViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter!.cellCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: String.className(YGAwarenessTableViewCell.self),
                                                 for: indexPath)
        
        (cell as! YGAwarenessTableViewCell).delegate = self
        (cell as! YGAwarenessTableViewCell).index = indexPath.row
        (cell as! YGAwarenessTableViewCell).isShowingInfo = presenter!.isCellShowInfo(cellIndex: indexPath.row)
        (cell as! YGAwarenessTableViewCell).updateInfoWith(presenter!.modelFor(indexPath))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.beginUpdates()
        self.tableView.endUpdates()
    }

}

//MARK: - YGAwarenessTableViewCellDelegate

extension YGAwarenessViewController: YGAwarenessTableViewCellDelegate {
    
    func awarenessCellDidShowInfo(_ cell: YGAwarenessTableViewCell) {
        presenter?.showInfoByCellIndex(cell.index)
        updateTableView()
    }
    
    func awarenessCellDidHideInfo(_ cell: YGAwarenessTableViewCell) {
        presenter?.hideInfoByCellIndex(cell.index)
        updateTableView()
    }
    
    func addPositiveKarmaPoint() {
        
        presenter?.addPositiveKarmaPoint()
        positiveKarmaPointsView.addKarmaPoint(positive: true)
    }
    
    func addNegativeKarmaPoint() {
        
        presenter?.addNegativeKarmaPoint()
        negativeKarmaPointsView.addKarmaPoint(positive: false)
    }
}

//MARK: - YGKarmaPointsViewDelegate

extension YGAwarenessViewController: YGKarmaPointsViewDelegate {
    
    func karmaViewRemoveKarmaPoint(_ view: YGKarmaPointsView) {
        if(view == negativeKarmaPointsView) {
            presenter?.removeNegativeKarmaPoint()
        } else {
            presenter?.removePositiveKarmaPoint()
        }
    }
}
