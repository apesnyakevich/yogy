//
//  YGProfileTableViewCellModel.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 05/08/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

struct YGProfileTableViewCellModel
{    
    var statusActive: Bool?
    var title: String?
    var description: String?
    var color : UIColor?
}
