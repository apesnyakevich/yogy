//
//  YGPsychomatrixTableViewCell.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 11/09/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

class YGPsychomatrixTableViewCell: UITableViewCell {

    @IBOutlet fileprivate weak var titleLabel: UILabel!
    @IBOutlet fileprivate weak var valueLabel: UILabel!
    
    @IBOutlet fileprivate weak var borderValueView: UIView!
    @IBOutlet fileprivate weak var maxValueView: UIView!
    @IBOutlet fileprivate weak var normalValueView: UIView!
    @IBOutlet fileprivate weak var currentValueView: UIView!
    
    @IBOutlet var normalViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet var currentViewWidthConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        normalViewWidthConstraint.constant = maxValueView.frame.width * 5.0 / 6.0
        
        maxValueView.borderWidth = 1.0
        maxValueView.cornerRadius = maxValueView.frame.height/2.0
        
        normalValueView.borderWidth = 1.0
        normalValueView.cornerRadius = normalValueView.frame.height/2.0
        
        currentValueView.cornerRadius = currentValueView.frame.height/2.0
    }
    
    func updateTitle(_ title: String) {
        titleLabel.text = title
    }
    
    func updateMatrixValue(_ value: Int) {
        valueLabel.text = "\(value)"
        let currentValue = (value < 6) ? CGFloat(value) : 6.0
        
        currentViewWidthConstraint.constant = maxValueView.frame.width * currentValue / 6.0
        UIView.animate(withDuration: 3.0, animations: {
            self.setNeedsLayout()
        })
    }
    
    func updateColor(_ color: UIColor) {
        let circle = CAShapeLayer()
        circle.path = UIBezierPath(roundedRect: borderValueView.bounds, cornerRadius: borderValueView.frame.width/2).cgPath
        circle.fillColor = UIColor.clear.cgColor
        circle.strokeColor = color.cgColor
        circle.lineWidth = 1;
        circle.lineDashPattern = [4, 2]
        borderValueView.layer.addSublayer(circle)
        
        normalValueView.borderColor = color
        maxValueView.borderColor = color
        
        currentValueView.backgroundColor = color
    }
}
