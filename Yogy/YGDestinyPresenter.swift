//
//  YGDestinyPresenter.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 11/09/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGDestinyPresenterDelegate {
    
    func destinyImage() -> UIImage
    func destinyTitle() -> String
    func destinyDesctiption() -> String
    
    func pastTitle() -> String
    func pastDescription() -> String
    
    func isExperienceChecked() -> Bool
}

class YGDestinyPresenter: YGDestinyPresenterDelegate {
    
    private var view: YGDestinyViewControllerDelegate
    private var model: YGDestinyModel
    private var dataProvider: YGDataProviderProtocol?
    
    required init(view: YGDestinyViewControllerDelegate, model: YGDestinyModel, dataProvider: YGDataProviderProtocol?) {
        self.view = view
        self.model = model
        self.dataProvider = dataProvider
    }
    
    //MARK: - Private
    
    private func destinyNumber() -> Int {
        let birthDate = dataProvider?.currentUser()?.birthDate
        return YGAppUtils.referenceNumber2(birthDate!)
    }
    
    private func pastNumber() -> Int {
        let birthDate = dataProvider?.currentUser()?.birthDate
        return YGAppUtils.referenceNumber4(birthDate!)
    }
    
    //MARK: - YGDestinyPresenterDelegate
    
    func destinyImage() -> UIImage {
        return UIImage(named:(String(format: "icon_karma_task%d", destinyNumber())))!
    }
    
    func destinyTitle() -> String {
        return String.localize("destiny_title\(destinyNumber())")
    }
    
    func destinyDesctiption() -> String {
        return String.localize("destiny_description\(destinyNumber())")
    }
    
    func pastTitle() -> String {
        return String.localize("destiny_title\(pastNumber())")
    }
    
    func pastDescription() -> String {
        return String.localize("destiny_description\(pastNumber())")
    }
    
    func isExperienceChecked() -> Bool {
        let isChecked = (pastNumber() != destinyNumber())
        return isChecked
    }
}
