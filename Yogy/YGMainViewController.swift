//
//  YGMainViewController.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 31/07/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class YGMainViewController: UITabBarController {

    var dataProvider: YGDataProviderProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        
        self.addDefaultCategories()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    private func addDefaultCategories() {
        
        if UIApplication.isFirstLaunch() {
            dataProvider?.deleteAllCategories()
            
            dataProvider?.insertCategory(index: 0, title: "Relax", colorCode: "EEE78A", iconName: "image_category_relax")
            dataProvider?.insertCategory(index: 1, title: "Health", colorCode: "FF8360", iconName: "image_category_health")
            dataProvider?.insertCategory(index: 2, title: "Family", colorCode: "7DCE82", iconName: "image_category_family")
            dataProvider?.insertCategory(index: 3, title: "Business", colorCode: "63D2F6", iconName: "image_category_business")
            dataProvider?.insertCategory(index: 4, title: "Creativity", colorCode: "D2A8E2", iconName: "image_category_creativity")
        }
    }
}

extension YGMainViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
    
    }
    
    func leftDidOpen() {
 
    }
    
    func leftWillClose() {
  
        self.hideKeyboardWhenTappedAround()
    }
    
    func leftDidClose() {
        
    }
    
    func rightWillOpen() {
      
    }
    
    func rightDidOpen() {
       
    }
    
    func rightWillClose() {
      
    }
    
    func rightDidClose() {

    }
}
