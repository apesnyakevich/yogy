//
//  YGFriendModel+CoreDataProperties.swift
//  
//
//  Created by apesnyakevich on 14.08.17.
//
//

import Foundation
import CoreData


extension YGFriendModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<YGFriendModel> {
        return NSFetchRequest<YGFriendModel>(entityName: "YGFriendModel")
    }

    @NSManaged public var birthDate: NSDate?
    @NSManaged public var name: String?
    @NSManaged public var photoPath: String?
    @NSManaged public var surname: String?
    @NSManaged public var user: YGUserModel?

}
