//
//  YGDestinyCodeViewController.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 31/07/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

class YGDestinyCodeViewController: UIViewController {

    @IBOutlet fileprivate weak var destinyModeSegment: UISegmentedControl!
    
    @IBOutlet fileprivate weak var destinyView: UIView!
    @IBOutlet fileprivate weak var karmaView: UIView!
    @IBOutlet fileprivate weak var potentialView: UIView!
    
    fileprivate var activeDestinyModeIndex: Int = YGDestinyCodeMode.Destiny.rawValue
    
    //MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        adjustNavigationBar()
        
        localizeUI()
        
        addViewGestures()
        
        updateChildViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
        self.addOnlyLeftGestures()
    }
    
    //MARK: - Private
    
    fileprivate func adjustNavigationBar() {
        self.addLeftBarButtonWithImage(UIImage(named:"icon_menu")!)
    }
    
    private func localizeUI() {
        destinyModeSegment.setTitle(String.localize("Destiny"), forSegmentAt: YGDestinyCodeMode.Destiny.rawValue)
        destinyModeSegment.setTitle(String.localize("Karma"), forSegmentAt: YGDestinyCodeMode.Karma.rawValue)
        destinyModeSegment.setTitle(String.localize("Potential"), forSegmentAt: YGDestinyCodeMode.Potential.rawValue)
    }
    
    private func updateChildViews() {
        destinyView.isHidden = (activeDestinyModeIndex != YGDestinyCodeMode.Destiny.rawValue)
        karmaView.isHidden = (activeDestinyModeIndex != YGDestinyCodeMode.Karma.rawValue)
        potentialView.isHidden = (activeDestinyModeIndex != YGDestinyCodeMode.Potential.rawValue)
    }
    
    //MARK: - Actions
    
    @IBAction func destinySegmentChanged(_ sender: UISegmentedControl) {
        
        if (sender.selectedSegmentIndex > activeDestinyModeIndex) {
            activeDestinyModeIndex = sender.selectedSegmentIndex
            animateViewSwipeWithDirection(kCATransitionFromRight)
        }
        else if (sender.selectedSegmentIndex < activeDestinyModeIndex) {
            activeDestinyModeIndex = sender.selectedSegmentIndex
            animateViewSwipeWithDirection(kCATransitionFromLeft)
        }
    }
    
    //MARK: - Gestures
    
    fileprivate func addViewGestures() {
        addSwipeGestureRecognizer(targetView: view, direction: .left)
        addSwipeGestureRecognizer(targetView: view, direction: .right)
    }
    
    override func handleSwipeGesture(gesture: UISwipeGestureRecognizer) {
        
        if (gesture.view == view) {
            if (gesture.direction == .left) {
                if (activeDestinyModeIndex < (YGThemeDayCalendarMode.All.rawValue - 1)) {
                    activeDestinyModeIndex += 1
                    destinyModeSegment.selectedSegmentIndex = activeDestinyModeIndex
                    animateViewSwipeWithDirection(kCATransitionFromRight)
                }
            }
            else if (gesture.direction == .right) {
                if (activeDestinyModeIndex > 0) {
                    activeDestinyModeIndex -= 1
                    destinyModeSegment.selectedSegmentIndex = activeDestinyModeIndex
                    animateViewSwipeWithDirection(kCATransitionFromLeft)
                }
            }
        }
    }
    
    fileprivate func animateViewSwipeWithDirection(_ direction: String) {
        let animation = CATransition()
        animation.type = kCATransitionPush
        animation.subtype = direction
        animation.duration = 0.5
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        
        self.view.exchangeSubview(at: 0, withSubviewAt: 0)
        self.view.layer.add(animation, forKey: nil)
        
        updateChildViews()
    }

}
