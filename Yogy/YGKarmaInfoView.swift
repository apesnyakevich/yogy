//
//  YGKarmaInfoView.swift
//  Yogy
//
//  Created by Anna on 17.08.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

class YGKarmaInfoView: UIView {

    @IBOutlet private weak var infoLabel: UILabel!

    
    func updateInfoWith(_ text: String) {
        infoLabel.text = text
    }
}
