//
//  YGRegisterViewController.swift
//  Yogy
//
//  Created by apesnyakevich on 05.09.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit
import Alamofire

class YGRegisterViewController: UIViewController {

    @IBOutlet weak var registerLabel: UILabel!
    @IBOutlet weak var notesLabel: UILabel!
    @IBOutlet weak var registerButton: UIButton!
    
    @IBOutlet weak var firstNameTextField: YGSelectableTextFieldView!
    @IBOutlet weak var lastNameTextField: YGSelectableTextFieldView!
    @IBOutlet weak var dateBirthTextField: YGSelectableTextFieldView!
    @IBOutlet weak var emailTextField: YGSelectableTextFieldView!
    @IBOutlet weak var passwordTextField: YGSelectableTextFieldView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var modelRegister = YGUserRegisterModel()
    
    //MARK: - View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setModelRegister()
        
        registerButton.cornerRadius = registerButton.height()/2
        notesLabel.text = ""
        notesLabel.textColor = UIColor.customRedColor()
        
        setUIControls()
    }
    
    func setModelRegister() {
        modelRegister.name = ""
        modelRegister.surname = ""
        modelRegister.email = ""
        YGAppUtils.setUserEmail(modelRegister.email!)
        modelRegister.birthString = ""
        modelRegister.password = ""
        if(isNetworkDebug) {
            modelRegister.name = "medvedko"
            modelRegister.surname = "medvedko"
//            modelRegister.birthString = "2017-09-11"
            modelRegister.email = "medvedko11@gmail.com"
            modelRegister.password = "Abvgde!1"
            
            firstNameTextField.inputTextField.text = modelRegister.name
            lastNameTextField.inputTextField.text = modelRegister.surname
//            dateBirthTextField.inputTextField.text = modelRegister.birthString
            emailTextField.inputTextField.text = modelRegister.email
            passwordTextField.inputTextField.text = modelRegister.password
        }
    }
    
    func setUIControls() {
        
        registerLabel.text = String.localize("Register")
        registerButton.setTitle(String.localize("Register enter"), for: UIControlState.normal)
        
        firstNameTextField.inputTextField.placeholder = String.localize("Name")
        lastNameTextField.inputTextField.placeholder = String.localize("Surname")
        dateBirthTextField.inputTextField.placeholder = String.localize("Birth Date")
        emailTextField.inputTextField.placeholder = String.localize("E-mail")
        passwordTextField.inputTextField.placeholder = String.localize("Password")
        
        firstNameTextField.inputTextField.tag = YGRegistrationTag.YGRegistrationTagFirstName.rawValue
        lastNameTextField.inputTextField.tag  = YGRegistrationTag.YGRegistrationTagLastName.rawValue
        dateBirthTextField.inputTextField.tag = YGRegistrationTag.YGRegistrationTagDateBirth.rawValue
        emailTextField.inputTextField.tag = YGRegistrationTag.YGRegistrationTagEmail.rawValue
        passwordTextField.inputTextField.tag = YGRegistrationTag.YGRegistrationTagPassword.rawValue
        
        firstNameTextField.inputTextField.autocapitalizationType = UITextAutocapitalizationType.words
        lastNameTextField.inputTextField.autocapitalizationType = UITextAutocapitalizationType.words
        
        emailTextField.inputTextField.keyboardType = UIKeyboardType.emailAddress
        passwordTextField.inputTextField.isSecureTextEntry = true
        emailTextField.inputTextField.text = modelRegister.email
        
        firstNameTextField.delegate = self
        lastNameTextField.delegate = self
        dateBirthTextField.delegate = self
        emailTextField.delegate = self
        passwordTextField.delegate = self
    }
    
    @IBAction func registerButtonTouchUpInside(_ sender: UIButton) {
        
        if(!YGAppUtils.validate(name: firstNameTextField.inputTextField.text!)) {
            notesLabel.text = String.localize("error_message_name_must_contain_chars")
        }
        else if(!YGAppUtils.validate(name: lastNameTextField.inputTextField.text!)) {
            notesLabel.text = String.localize("error_message_surname_must_contain_chars")
        }
        else if(!YGAppUtils.validate(email: emailTextField.inputTextField.text!)) {
            notesLabel.text = String.localize("error_message_enter_valid_email")
        }
        else if(!YGAppUtils.validate(password: passwordTextField.inputTextField.text!)) {
            notesLabel.text = String.localize("error_message_password_must_contain_digits")
        }
        else {
            activityIndicator.startAnimating()
            notesLabel.text = ""
            print("validate OK")
            registerRequest()
        }
    }
    
    // MARK: - Alamofire Network
    
//    { Email : "medvedko4@gmail.com", UserName : "medvedko", UserSurname : "medvedko", Password : "Abvgde!1", BirthDate : "2017-09-03" }
    
    func registerRequest() {
        
        var parameters: Parameters = [:]
            parameters = [
            kYGNetworkEmailKey: modelRegister.email!,
            kYGNetworkNameKey: modelRegister.name!,
            kYGNetworkSurnameKey: modelRegister.surname!,
            kYGNetworkPasswordKey: modelRegister.password!,
            kYGNetworkBirthDateKey: modelRegister.birthString! ]
        
        Alamofire.request(kUserRegisterUrlString,
                          method: .post,
                          parameters: parameters,
                          encoding: URLEncoding.default).responseJSON
            { response in
                
                self.activityIndicator.stopAnimating()
                
                if(isNetworkDebug) {
                    print("Request: \(String(describing: response.request))")   // original url request
                    print("Response: \(String(describing: response.response))") // http url response
                    print("Result: \(response.result)")                         // response serialization result
                    
                    if let json = response.result.value {
                        print("JSON: \(json)") // serialized json response
                    }
                }
                
                if let value = response.result.value as? [String:Any] {
                    
                    let keyExists = value[kYGNetworkErrorDescriptionKey] != nil
                    if(keyExists) {
                        let errorDescrition = value[kYGNetworkErrorDescriptionKey] as! String
                        print(errorDescrition)
                        self.notesLabel.text = errorDescrition
                    } else {
                        YGAppUtils.setUserEmail(self.modelRegister.email!)
                        print("Register Ok!")
                    }
                }
            }
    }
}

// MARK: - YGSelectableTextFieldViewDelegate

extension YGRegisterViewController: YGSelectableTextFieldViewDelegate {
    
    func setSelectionInfo(tag: Int, selectionString: String) {
        
        switch tag {
        case YGRegistrationTag.YGRegistrationTagFirstName.rawValue:
            if(YGAppUtils.validate(name: selectionString)) {
                notesLabel.text = ""
                modelRegister.name = selectionString
                firstNameTextField.isValid = true
            } else {
                notesLabel.text = String.localize("error_message_name_must_contain_chars")
                firstNameTextField.isValid = false
            }
            break
        case YGRegistrationTag.YGRegistrationTagLastName.rawValue:
            modelRegister.surname = selectionString
            if(YGAppUtils.validate(name: selectionString)) {
                notesLabel.text = ""
                modelRegister.surname = selectionString
                lastNameTextField.isValid = true
            } else {
                notesLabel.text = String.localize("error_message_surname_must_contain_chars")
                lastNameTextField.isValid = false
            }
            break
        case YGRegistrationTag.YGRegistrationTagDateBirth.rawValue:
            modelRegister.birthString = selectionString
            break
        case YGRegistrationTag.YGRegistrationTagEmail.rawValue:
            if(YGAppUtils.validate(email: selectionString)) {
                notesLabel.text = ""
                modelRegister.email = selectionString
                emailTextField.isValid = true
            } else {
                notesLabel.text = String.localize("error_message_enter_valid_email")
                emailTextField.isValid = false
            }
            break
        case YGRegistrationTag.YGRegistrationTagPassword.rawValue:
            if(YGAppUtils.validate(password: selectionString)) {
                notesLabel.text = ""
                modelRegister.password = selectionString
                passwordTextField.isValid = true
            } else {
                notesLabel.text = String.localize("error_message_password_must_contain_digits")
                passwordTextField.isValid = false
            }
            break
        default:
            break
        }
    }
}
