//
//  YGThemeDayPresenter.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 07/08/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGThemeDayPresenterDelegate {
    func categoryAt(_ index: Int) -> YGTaskCategoryModel
    func categoryColorAt(_ index: Int) -> String?
    func categoryIconAt(_ index: Int) -> String?
    func categoryProgressAt(_ index: Int) -> CGFloat?
    
    func deleteOldTasks()
}

class YGThemeDayPresenter : YGThemeDayPresenterDelegate {
    
    private unowned let view: YGThemeDayViewControllerDelegate
    private var model: YGThemeDayModel
    private var dataProvider: YGDataProviderProtocol?
    
    required init(view: YGThemeDayViewControllerDelegate, model: YGThemeDayModel, dataProvider: YGDataProviderProtocol?) {
        self.view = view
        self.model = model
        self.dataProvider = dataProvider
        
        if (dataProvider != nil) {
            self.model.categories = (dataProvider?.allTaskCategories())!
        }
    }
    
    //MARK: -
    
    func categoryAt(_ index: Int) -> YGTaskCategoryModel {
        return self.model.categories[index]
    }
    
    func categoryColorAt(_ index: Int) -> String? {
        var colorHex: String? = "000000"
        if (index < self.model.categories.count) {
            colorHex = (self.model.categories[index]).colorHex
        }
        return colorHex
    }
    
    func categoryIconAt(_ index: Int) -> String? {
        var icon: String? = ""
        if (index < self.model.categories.count) {
            icon = (self.model.categories[index]).iconName
        }
        return icon
    }
    
    func categoryProgressAt(_ index: Int) -> CGFloat? {
        var progress: CGFloat = 0.0
        if (index < self.model.categories.count) {
            progress = (self.model.categories[index]).progress()
        }
        return progress
    }
    
    func deleteOldTasks() {
        dataProvider?.deleteOldTasks()
    }
}
