//
//  YGConstants.swift
//  Yogy
//
//  Created by Anna on 31.07.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

let isNetworkDebug = true

let kUserPhotoName      = "img_user_photo"
let kUserDummyPhoto     = "image_dummy_user"
let kUserPhotoNameKey   = "kUserPhotoName"

let kYGFriendEditVCSegue            = "kYGFriendEditVCSegue"
let kOrganizerShowSegueIdentifier   = "_kOrganizerShowSegueIdentifier"
let kCalendarViewControllerSegue    = "_ShowCalendarViewController"
let kYGSubscriptionVCSegue          = "kYGSubscriptionVCSegue"
let kYGRegistrationVCSegue          = "kYGRegistrationVCSegue"
let kYGLoginVCSegue                 = "kYGLoginVCSegue"

let kSubscriptionNumberKey = "kSubscriptionNumberKey"
let kUserEmailKey          = "kUserEmailKey"

let kYGValidationEmailRegex             = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}" //"^([\\w-\\.]+([\\w-]+\\.)+[\\w-]{2,4})?$"
let kYGValidationNameRegex              = "^[^\\x00-\\x1f\\x21-\\x21\\x23-\\x26\\x2a-\\x2b\\x2f-\\x40\\x5b-\\x60\\x7b-\\xbf]{2,20}$"
let kYGValidationNameLengthRegex        = "^.{2,20}"
let kYGValidationPasswordLengthRegex    = "^.{6,20}"
let kYGValidationPasswordRegex          = "^(?=.*\\d)(?=.*[a-zA-Z]).{6,20}$"

let kUserRegisterUrlString  = "http://promserver.azurewebsites.net/api/Account/Register"
let kUserLoginUrlString     = "http://promserver.azurewebsites.net/api/Account/Login"

let kYGNetworkEmailKey              = "Email"
let kYGNetworkNameKey               = "UserName"
let kYGNetworkUserNameKey           = "userName"
let kYGNetworkSurnameKey            = "UserSurname"
let kYGNetworkPasswordKey           = "Password"
let kYGNetworkBirthDateKey          = "BirthDate"
let kYGNetworkErrorDescriptionKey   = "error_description"

enum YGRegistrationTag: Int {
    case YGRegistrationTagFirstName = 0
    case YGRegistrationTagLastName
    case YGRegistrationTagDateBirth
    case YGRegistrationTagEmail
    case YGRegistrationTagPassword
}

enum YGLoginTag: Int {
    case YGLoginTagEmail = 0
    case YGLoginTagPassword
}



