//
//  YGCalendarPresenter.swift
//  Yogy
//
//  Created by Anna on 27.08.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGCalendarPresenterDelegate {
    func setActiveCalendarModeIndex(_ index: Int)
    func setActiveDate(_ date: NSDate)
    func setFriendForCalculations(_ friend: YGFriendModel)
    
    func friendName() -> String
    
    func adviceTitle() -> String
    func adviceText() -> String
    func adviceImage() -> UIImage
}

class YGCalendarPresenter: YGCalendarPresenterDelegate {
    
    private unowned let view: YGCalendarViewControllerProtocol
    private var model: YGCalendarModel
    private var dataProvider: YGDataProviderProtocol?
    
    required init(view: YGCalendarViewControllerProtocol, model: YGCalendarModel, dataProvider: YGDataProviderProtocol?) {
        self.view = view
        self.model = model
        self.dataProvider = dataProvider
    }
    
    //MARK: - YGThemeDayPresenterDelegate
    
    func setActiveCalendarModeIndex(_ index: Int) {
        model.activeCalendarModeIndex = index
    }
    
    func setActiveDate(_ date: NSDate) {
        model.activeDate = date
    }
    
    func setFriendForCalculations(_ friend: YGFriendModel) {
        model.friendForCalculations = friend
    }
    
    func friendName() -> String {
        if (model.friendForCalculations != nil) {
            return "\(model.friendForCalculations!.name!) \(model.friendForCalculations!.surname!)"
        } else {
            return ""
        }
    }
    
    func adviceTitle() -> String {
        var title = ""
        
        if (dataProvider != nil) {
            var birthDate = dataProvider?.currentUser()?.birthDate
            
            if (model.friendForCalculations != nil) {
                birthDate = model.friendForCalculations!.birthDate!
            }
            
            switch model.activeCalendarModeIndex {
            case YGThemeDayCalendarMode.Day.rawValue:
                let pdn = YGAppUtils.personalNumberDay(birthDate: birthDate!, currentDate: model.activeDate)
                title = String.localize(String(format: "theme_title_pdn%d", pdn))
                break
            case YGThemeDayCalendarMode.Month.rawValue:
                let pmn = YGAppUtils.personalNumberMonth(birthDate: birthDate!, currentDate: model.activeDate)
                title = String.localize(String(format: "theme_title_pmn%d", pmn))
                break
            case YGThemeDayCalendarMode.Year.rawValue:
                let pyn = YGAppUtils.personalNumberYear(birthDate: birthDate!, currentDate: model.activeDate)
                title = String.localize(String(format: "theme_title_pyn%d", pyn))
                break
            default:
                break
            }
        }
        return title
    }
    
    func adviceText() -> String {
        var advice = ""
        
        if (dataProvider != nil) {
            var birthDate = dataProvider?.currentUser()?.birthDate
            
            if (model.friendForCalculations != nil) {
                birthDate = model.friendForCalculations!.birthDate!
            }
            
            switch model.activeCalendarModeIndex {
            case YGThemeDayCalendarMode.Day.rawValue:
                let pdn = YGAppUtils.personalNumberDay(birthDate: birthDate!, currentDate: model.activeDate)
                advice = String.localize(String(format: "theme_pdn%d", pdn))
                break
            case YGThemeDayCalendarMode.Month.rawValue:
                let pmn = YGAppUtils.personalNumberMonth(birthDate: birthDate!, currentDate: model.activeDate)
                advice = String.localize(String(format: "theme_pmn%d", pmn))
                break
            case YGThemeDayCalendarMode.Year.rawValue:
                let pyn = YGAppUtils.personalNumberYear(birthDate: birthDate!, currentDate: model.activeDate)
                advice = String.localize(String(format: "theme_pyn%d", pyn))
                break
            default:
                break
            }
        }
        return advice
    }
    
    func adviceImage() -> UIImage {
        var image = UIImage()
        
        if (dataProvider != nil) {
            var birthDate = dataProvider?.currentUser()?.birthDate
            
            if (model.friendForCalculations != nil) {
                birthDate = model.friendForCalculations!.birthDate!
            }
            
            switch model.activeCalendarModeIndex {
            case YGThemeDayCalendarMode.Day.rawValue:
                let pdn = YGAppUtils.personalNumberDay(birthDate: birthDate!, currentDate: model.activeDate)
                image = UIImage(named:(String(format: "icon_pdn%d", pdn)))!
                break
            case YGThemeDayCalendarMode.Month.rawValue:
                let pmn = YGAppUtils.personalNumberMonth(birthDate: birthDate!, currentDate: model.activeDate)
                image = UIImage(named:(String(format: "icon_pmn%d", pmn)))!
                break
            case YGThemeDayCalendarMode.Year.rawValue:
                let pyn = YGAppUtils.personalNumberYear(birthDate: birthDate!, currentDate: model.activeDate)
                image = UIImage(named:(String(format: "icon_pyn%d", pyn)))!
                break
            default:
                break
            }
        }
        return image
    }

}
