//
//  YGProfileSoundsTableViewCell.swift
//  Yogy
//
//  Created by apesnyakevich on 16.08.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

class YGProfileSoundsTableViewCell: UITableViewCell, YGProfileTableViewCellProtocol {
    
    @IBOutlet weak var enableSwitch: UISwitch!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    func updateInfoWithModel(model: YGProfileTableViewCellModel) {
        
        if enableSwitch.isOn {
            descriptionLabel.text = String.localize("Sounds enabled")
        } else {
            descriptionLabel.text = String.localize("Sounds disabled")
        }
    }
    
    @IBAction func switchStateChanged(switchState: UISwitch)
    {
        if switchState.isOn {
            descriptionLabel.text = String.localize("Sounds enabled")
        } else {
            descriptionLabel.text = String.localize("Sounds disabled")
        }
    }
}
