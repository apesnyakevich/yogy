//
//  YGOrganizerViewPresenter.swift
//  Yogy
//
//  Created by Anna on 31.07.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGOrganizerPresenterDelegate {
    
    func setActiveCategoryIndex(_ index: Int)
    func categoriesCount() -> Int
    
    func activeCategoryHexColor() -> String?
    func activeCategoryIconName() -> String?
    func activeCategoryName() -> String?
    func activeCategoryProgress() -> CGFloat?
    
    func activeAdviceText() -> String?
    
    func reloadTasks()
    func tasksCount() -> Int
    func taskTitleAt(_ index: Int) -> String?
    func taskTextAt(_ index: Int) -> String?
    func taskStatusAt(_ index: Int) -> Bool?
    
    func createTask(title: String?, text: String)
    func updateTaskAt(_ index: Int, title: String?, text: String?)
    func deleteTaskAt(_ index: Int)
    func changeTaskStatusAt(_ index: Int)
}

class YGOrganizerPresenter: YGOrganizerPresenterDelegate {
    
    private var view: YGOrganizerViewControllerDelegate
    private var model: YGOrganizerModel
    private var dataProvider: YGDataProviderProtocol?
    
    required init(view: YGOrganizerViewControllerDelegate, model: YGOrganizerModel, dataProvider: YGDataProviderProtocol?) {
        self.view = view
        self.model = model
        self.dataProvider = dataProvider
        
        if (dataProvider != nil) {
            self.model.categories = (dataProvider?.allTaskCategories())!
        }
    }
    
    //MARK: - YGOrganizerPresenterDelegate
    
    func setActiveCategoryIndex(_ index: Int) {
        model.activeCategoryIndex = index
    }
    
    func categoriesCount() -> Int {
        return model.categories.count
    }
    
    //MARK: -
    
    func activeCategoryHexColor() -> String? {
        return activeCategory()?.colorHex
    }
    
    func activeCategoryIconName() -> String? {
        return activeCategory()?.iconName
    }
    
    func activeCategoryName() -> String? {
        return activeCategory()?.title
    }
    
    func activeCategoryProgress() -> CGFloat? {
        return activeCategory()?.progress()
    }
    
    //MARK: -
    
    func activeAdviceText() -> String? {
        var advice: String? = ""
        
        let birthdate = dataProvider?.currentUser()?.birthDate
        if (birthdate != nil) {
            let pdn = YGAppUtils.personalNumberDay(birthDate: birthdate!, currentDate: NSDate())
            let activeCategory = model.categories[model.activeCategoryIndex]
            
            advice = String.localize(String(format: "organizer_category%d_pdn%d", activeCategory.categoryIndex, pdn))
        }
        return advice
    }
    
    //MARK: -
    
    func reloadTasks() {
        model.tasks.removeAll()
        model.tasks.append(contentsOf: activeCategory()?.tasks?.allObjects as! [YGTaskModel])
        filterTasks()
    }
    
    func tasksCount() -> Int {
        return model.tasks.count
    }
    
    func taskTitleAt(_ index: Int) -> String? {
        return (model.tasks[index]).title
    }
    
    func taskTextAt(_ index: Int) -> String? {
        return (model.tasks[index]).text
    }
    
    func taskStatusAt(_ index: Int) -> Bool? {
        return (model.tasks[index]).completed
    }
    
    //MARK: -
    
    func createTask(title: String?, text: String) {
        let taskTitle = checkTaskTitle(title, text: text)
        
        dataProvider?.insertTask(title: taskTitle, text: text, category: activeCategory()!)
        reloadTasks()
    }
    
    func updateTaskAt(_ index: Int, title: String?, text: String?) {
        let task = model.tasks[index]
        
        dataProvider?.updateTask(task: task, title: title, text: text)
        reloadTasks()
    }
    
    func deleteTaskAt(_ index: Int) {
        let task = model.tasks[index]
        
        dataProvider?.deleteTask(task: task)
        reloadTasks()
    }
    
    func changeTaskStatusAt(_ index: Int) {
        let task = model.tasks[index]
        
        dataProvider?.markTaskCompletion(task: task, isCompleted: !task.completed)
        reloadTasks()
    }
    
    //MARK: - Private
    
    private func activeCategory() -> YGTaskCategoryModel? {
        let activeCategory = model.categories[model.activeCategoryIndex]
        return activeCategory
    }
    
    private func filterTasks() {
        guard model.tasks.count > 0 else {
            return
        }
    
        model.tasks.sort(by: { $0.creationDate?.compare($1.creationDate! as Date) == .orderedDescending })
    }
    
    private func checkTaskTitle(_ title: String?, text: String) -> String {
        
        var taskTitle = title
        if title!.isEmpty {
            let bodySize = text.characters.count
            if (bodySize < 15) {
                taskTitle = text
            } else {
                let startIndex = text.index(text.startIndex, offsetBy: 14)
                taskTitle = text.substring(to: startIndex)
            }
        }
        return taskTitle!
    }
    
}
