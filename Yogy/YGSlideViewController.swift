//
//  YGSlideViewController.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 01/08/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class YGSlideViewController: SlideMenuController {
    override func awakeFromNib() {
        YGAppAppearance.performStartupConfiguration()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let mainViewController = storyboard.instantiateViewController(withIdentifier:String(describing: YGMainViewController.self)) as! YGMainViewController
        self.mainViewController = mainViewController
        self.delegate = mainViewController
        
        let profileViewController = storyboard.instantiateViewController(withIdentifier:String(describing: YGProfileViewController.self)) as! YGProfileViewController
        let profileNC: UINavigationController = UINavigationController(rootViewController: profileViewController)
        self.leftViewController = profileNC
        
        let organizerViewController = storyboard.instantiateViewController(withIdentifier: String(describing: YGOrganizerViewController.self)) as! YGOrganizerViewController
        let organizerNC: UINavigationController = UINavigationController(rootViewController: organizerViewController)
        self.rightViewController = organizerNC
        
        self.automaticallyAdjustsScrollViewInsets = true
        
        super.awakeFromNib()
    }
}
