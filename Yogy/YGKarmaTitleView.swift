//
//  YGKarmaTitleView.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 08/09/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

class YGKarmaTitleView: UIView {

    @IBOutlet fileprivate weak var borderImageView: UIImageView!
    @IBOutlet fileprivate weak var outerDotView: UIView!
    @IBOutlet fileprivate weak var innerDotView: UIView!
    
    @IBOutlet fileprivate weak var leftImageView: UIImageView!
    @IBOutlet fileprivate weak var rightImageView: UIImageView!
    @IBOutlet fileprivate weak var topImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        drawCircles()
        
        rotateBorderViewWithDelay(0.0)
    }
    
    func updateView() {
        animateViews()
    }
    
    //MARK: - Private
    
    private func drawCircles() {
        topImageView.cornerRadius = topImageView.frame.height/2.0
        leftImageView.cornerRadius = leftImageView.frame.height/2.0
        rightImageView.cornerRadius = rightImageView.frame.height/2.0
        
        addBorderToView(outerDotView, color: UIColor.customRedColor())
        addBorderToView(innerDotView, color: UIColor.customRedColor())
    }
    
    private func addBorderToView(_ targetView: UIView, color: UIColor) {
        let borderCircle = CAShapeLayer()
        borderCircle.path = UIBezierPath(roundedRect: targetView.bounds, cornerRadius: targetView.frame.width/2).cgPath
        borderCircle.fillColor = UIColor.clear.cgColor
        borderCircle.strokeColor = color.cgColor
        borderCircle.lineWidth = 1;
        borderCircle.lineDashPattern = [4, 2]
        targetView.layer.addSublayer(borderCircle)
    }
    
    private func animateViews() {
        rotateView(targetView: rightImageView, startAngle: .pi/3 - 1.1)
        rotateView(targetView: topImageView, startAngle: .pi*5/3 - 0.5)
        rotateView(targetView: leftImageView, startAngle: .pi + 0.1)
    }
    
    private func rotateBorderViewWithDelay(_ delay: CGFloat) {
        UIView.animate(withDuration: 15.0, delay: TimeInterval(delay), options: .curveLinear, animations: {
            self.borderImageView.transform = self.borderImageView.transform.rotated(by: .pi)
        }) { finished in
            self.rotateBorderViewWithDelay(0.0)
        }
    }
    
    private func rotateView(targetView: UIView, startAngle: CGFloat) {
     
        let orbit = CAKeyframeAnimation(keyPath: "position")
        let circlePath = UIBezierPath(arcCenter: CGPoint(x:0, y: self.frame.height/2-20.0), radius: 70.0, startAngle: startAngle, endAngle:.pi*2+startAngle , clockwise: true)
        orbit.path = circlePath.cgPath
        orbit.duration = 30.0
        orbit.isAdditive = true
        orbit.repeatCount = Float(CGFloat.greatestFiniteMagnitude)
        orbit.calculationMode = kCAAnimationPaced

        targetView.layer.add(orbit, forKey: "orbit")
    }
}
