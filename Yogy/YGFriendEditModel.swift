//
//  YGFriendEditModel.swift
//  Yogy
//
//  Created by Anna on 10.08.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

enum YGFriendEditCellType: Int {
    case YGFriendEditCellTypeName = 0
    case YGFriendEditCellTypeSurname
    case YGFriendEditCellTypeBirthdate
    case YGFriendEditCellTypeCount
}

class YGFriendEditModel: NSObject {
    
    var currentFriend: YGFriendModel?

    var currentName: String?
    var currentSurname: String?
    var currentDate: NSDate?
    var currentPhoto: UIImage?
    
    var selectedCellIndex: Int?
}
