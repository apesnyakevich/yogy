//
//  UIColor.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 11/5/15.
//  Copyright © 2015 Yuji Hato. All rights reserved.
//

import UIKit

extension UIColor {

    convenience init(hex: String) {
        self.init(hex: hex, alpha:1)
    }

    convenience init(hex: String, alpha: CGFloat) {
        var hexWithoutSymbol = hex
        if hexWithoutSymbol.hasPrefix("#") {
            hexWithoutSymbol = hex.substring(1)
        }
        
        let scanner = Scanner(string: hexWithoutSymbol)
        var hexInt:UInt32 = 0x0
        scanner.scanHexInt32(&hexInt)
        
        var r:UInt32!, g:UInt32!, b:UInt32!
        switch (hexWithoutSymbol.length) {
        case 3: // #RGB
            r = ((hexInt >> 4) & 0xf0 | (hexInt >> 8) & 0x0f)
            g = ((hexInt >> 0) & 0xf0 | (hexInt >> 4) & 0x0f)
            b = ((hexInt << 4) & 0xf0 | hexInt & 0x0f)
            break;
        case 6: // #RRGGBB
            r = (hexInt >> 16) & 0xff
            g = (hexInt >> 8) & 0xff
            b = hexInt & 0xff
            break;
        default:
            // TODO:ERROR
            break;
        }
        
        self.init(
            red: (CGFloat(r)/255),
            green: (CGFloat(g)/255),
            blue: (CGFloat(b)/255),
            alpha:alpha)
    }
    
    static func customDarkGrayColor() -> UIColor {
        return UIColor(hex:"333333")
    }
    
    static func customLightGrayColor() -> UIColor {
        return UIColor(hex:"8C8C8C")
    }
    
    static func customRedColor() -> UIColor {
        return UIColor(hex:"FF6B5E")
    }
    
    static func customBlueColor() -> UIColor {
        return UIColor(hex:"4DA7FF")
    }
    
    static func customPurpleColor() -> UIColor {
        return UIColor(hex:"6567BA")
    }
    
    static func customYellowColor() -> UIColor {
        return UIColor(hex:"EEE78A")
    }
    
    static func customGreenColor() -> UIColor {
        return UIColor(hex:"7DCE82")
    }
    
}
