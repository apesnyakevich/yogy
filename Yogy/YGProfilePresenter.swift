//
//  YGProfilePresenter.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 05/08/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGProfilePresenterDelegate {
    
    func cellSelectedAt(_ indexPath: IndexPath)
    func rowCount() -> Int
    func cellIdentifierAt(_ indexPath: IndexPath) -> String
    func cellModelAt(_ indexPath: IndexPath) -> YGProfileTableViewCellModel
    
    func userName() -> String?
    func userSurname() -> String?
    func userPhoto() -> UIImage?
    
    func resetBirthdate(_ date: NSDate)
    func resetUserPhoto(_ photo: UIImage)
    func resetUserPhotoPath(_ path: String)
}

class YGProfilePresenter: YGProfilePresenterDelegate {
    
    private unowned let view: YGProfileViewControllerDelegate
    private var model: YGProfileModel
    private var dataProvider: YGDataProviderProtocol?
    
    required init(view: YGProfileViewControllerDelegate, model: YGProfileModel, dataProvider: YGDataProviderProtocol?) {
        self.view = view
        self.model = model
        self.dataProvider = dataProvider
        
        addDefaultUser()
    }

    private func addDefaultUser() {
        if UIApplication.isFirstLaunch() {
            dataProvider?.deleteCurrentUser()
            dataProvider?.insertUser(name: "Dummy", surname: "User", email: "a@a.aa", password: "123", birthday: NSDate(timeInterval: -946080000, since: Date()))
        }
    }
    
    //MARK: - YGProfilePresenterDelegate
    
    func rowCount() -> Int {
        return YGProfileCellType.YGProfileCellTypeCount.rawValue
    }
    
    func cellSelectedAt(_ indexPath: IndexPath) {
        
    }
    
    func cellIdentifierAt(_ indexPath: IndexPath) -> String {
        
        var identifier = String()
        
        switch indexPath.row {
            case YGProfileCellType.YGProfileCellTypeBirthdate.rawValue:
                identifier = String(describing: YGProfileDateTableViewCell.self)
                break
            case YGProfileCellType.YGProfileCellTypeSubscription.rawValue:
                identifier = String(describing: YGProfileButtonTextTableViewCell.self)
                break
            case YGProfileCellType.YGProfileCellTypeSounds.rawValue:
                identifier = String(describing: YGProfileSoundsTableViewCell.self)
                break
            case YGProfileCellType.YGProfileCellTypeNotifications.rawValue,
                 YGProfileCellType.YGProfileCellTypeRateApp.rawValue,
                 YGProfileCellType.YGProfileCellTypeTermsConditions.rawValue,
                 YGProfileCellType.YGProfileCellTypePrivacyPolicy.rawValue,
                 YGProfileCellType.YGProfileCellTypeSignOut.rawValue:
                identifier = String(describing: YGProfileTextTableViewCell.self)
                break
            default:
                break
        }
        
        return identifier
    }
    
    func cellModelAt(_ indexPath: IndexPath) -> YGProfileTableViewCellModel {
        var model = YGProfileTableViewCellModel()
        
        switch indexPath.row {
            case YGProfileCellType.YGProfileCellTypeBirthdate.rawValue:
                
                model.title = String.localize("Birth Date")
                
                let formatter = YGAppUtils.outputDateFormatter()
                
                if (dataProvider?.currentUser()?.birthDate != nil) {
                    model.description = formatter.string(for: dataProvider?.currentUser()?.birthDate)
                }
                break
            case YGProfileCellType.YGProfileCellTypeSubscription.rawValue:
                model.statusActive = dataProvider?.currentUser()?.subscriptionEnabled
                if (model.statusActive != nil) {
                    model.description = (model.statusActive)! ? String.localize("Subscription enabled") : String.localize("Subscription disabled")
                }
                else {
                    model.description = String.localize("Subscription disabled")
                }
                break
            case YGProfileCellType.YGProfileCellTypeNotifications.rawValue:
                model.description = String.localize("Notifications")
                break
            case YGProfileCellType.YGProfileCellTypeSounds.rawValue:
                model.description = String.localize("Sounds enabled")
                break
            case YGProfileCellType.YGProfileCellTypeRateApp.rawValue:
                model.description = String.localize("Rate App")
                break
            case YGProfileCellType.YGProfileCellTypeTermsConditions.rawValue:
                model.description = String.localize("Terms and Conditions")
                break
            case YGProfileCellType.YGProfileCellTypePrivacyPolicy.rawValue:
                model.description = String.localize("Privacy Policy")
                break
            case YGProfileCellType.YGProfileCellTypeSignOut.rawValue:
                model.description = String.localize("Sign Out")
                model.color = UIColor.customRedColor()
                break
            default:
                break
        }
        
        return model
    }
    
    //MARK: - 
    
    func userName() -> String? {
        return dataProvider?.currentUser()?.name
    }
    
    func userSurname() -> String? {
        return dataProvider?.currentUser()?.surname
    }
    
    func userPhoto() -> UIImage? {
        var photo = UIImage(named: kUserDummyPhoto)!
        if (dataProvider?.currentUser()?.photoPath != nil) {
            photo = YGAppUtils.getImagePhotoWith((dataProvider?.currentUser()?.photoPath)!)
        }
        return photo
    }
    
    //MARK: -
    
    func resetBirthdate(_ date: NSDate) {
        dataProvider?.updateCurrentUserBirthdate(date: date)
//        print(YGAppUtils.potentialCellModel(date, 0))
    }
    
    func resetUserPhoto(_ photo: UIImage) {
        YGAppUtils.setImagePhotoForKey(kUserPhotoNameKey, image: photo)
    }
    
    func resetUserPhoto(_ name : String, _ photo: UIImage) {
        YGAppUtils.setImagePhotoForKey(kUserPhotoNameKey, image: photo)
    }
    
    func resetUserPhotoPath(_ path: String) {
        dataProvider?.updateCurrentUserPhotoPath(path: path)
    }
    
}
