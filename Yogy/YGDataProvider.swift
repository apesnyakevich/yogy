//
//  YGDataProvider.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 04/08/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit
import MagicalRecord

class YGDataProvider: NSObject, YGDataProviderProtocol {
        
    func mainContext() -> NSManagedObjectContext {
        
        return NSManagedObjectContext.mr_default()
    }
    
    //MARK: - Users
    
    func currentUser() -> YGUserModel? {
        
        return YGUserModel.mr_findFirst(in: mainContext())
    }
    
    func insertUser(name: String, surname: String, email: String, password: String, birthday: NSDate) {
        
        let userModel = YGUserModel.mr_createEntity(in: mainContext())
        userModel?.name = name
        userModel?.password = password
        userModel?.surname = surname
        userModel?.email = email
        userModel?.birthDate = birthday
        userModel?.subscriptionEnabled = false
        
        let image: UIImage = UIImage.init(named:kUserDummyPhoto)!
        let path: String = YGAppUtils.saveImageToFile(kUserDummyPhoto, image: image)
        
//        if (YGAppUtils.fileExistAt(path)) {
//            userModel?.photoPath = path
        if (YGAppUtils.fileExistWith(kUserDummyPhoto)) {
            userModel?.photoPath = path
        } else {
            userModel?.photoPath = kUserDummyPhoto
        }
//        YGAppUtils.setImageDummyPhotoForKey(kUserPhotoNameKey)
        
        mainContext().mr_saveToPersistentStoreAndWait()
    }

    func updateCurrentUserPhotoPath(path: String) {
        
        let userCurrent = currentUser()
        userCurrent?.photoPath = path
        
        mainContext().mr_saveToPersistentStoreAndWait()
    }

    func updateCurrentUserBirthdate(date:NSDate) {
        
        let userCurrent = currentUser()
        userCurrent?.birthDate = date
        
        mainContext().mr_saveToPersistentStoreAndWait()
    }
    
    func deleteCurrentUser() {
        
        YGUserModel.mr_deleteAll(matching:NSPredicate(format:"email.length > 0"), in:mainContext())

        mainContext().mr_saveToPersistentStoreAndWait()
    }
    
    //MARK: - Friends
    func allFriends() -> [YGFriendModel] {
        
        return YGFriendModel.mr_findAll()! as! [YGFriendModel]
    }
    
    func insertFriend(name: String?, surname: String?, photoPath: String?, birthday: NSDate?) {
        
        let friendModel = YGFriendModel.mr_createEntity(in: mainContext())
        friendModel?.name = name
        friendModel?.surname = surname
        friendModel?.photoPath = photoPath
        friendModel?.birthDate = birthday
        friendModel?.user = currentUser()
        
        mainContext().mr_saveToPersistentStoreAndWait()
    }
    
    func updateFriend(friend:YGFriendModel, name:String?, surname:String?, photoPath:String?, birthday:NSDate?) {
    
        if (name != nil) { friend.name = name }
        if (surname != nil) { friend.surname = surname }
        if (photoPath != nil) { friend.photoPath = photoPath }
        if (birthday != nil) { friend.birthDate = birthday }
        
        mainContext().mr_saveToPersistentStoreAndWait()
    }
    
    func deleteFriend(friend: YGFriendModel) {
        
        friend.mr_deleteEntity()
        
        mainContext().mr_saveToPersistentStoreAndWait()
    }
    
    //MARK: - Task Categories
    
    func allTaskCategories() -> [YGTaskCategoryModel] {
        
        let categories = YGTaskCategoryModel.mr_findAll()
        return categories as! [YGTaskCategoryModel]
    }
    
    func insertCategory(index: Int, title: String?, colorCode: String?, iconName: String?) {
        
        let categoryModel = YGTaskCategoryModel.mr_createEntity(in: mainContext())
        categoryModel?.categoryIndex = Int32(index)
        categoryModel?.title = title
        categoryModel?.colorHex = colorCode
        categoryModel?.iconName = iconName
        
        mainContext().mr_saveToPersistentStoreAndWait()
    }
    
    func deleteAllCategories() {
        
        YGTaskCategoryModel.mr_deleteAll(matching:NSPredicate(format:"categoryIndex > -1"))
        
        mainContext().mr_saveToPersistentStoreAndWait()
    }

    //MARK: - Tasks
    
    func insertTask(title:String, text:String, category:YGTaskCategoryModel) {
        
        let taskModel = YGTaskModel.mr_createEntity(in: mainContext())
        taskModel?.title = title
        taskModel?.text = text
        taskModel?.completed = false
        taskModel?.creationDate = NSDate()
        taskModel?.category = category

        mainContext().mr_saveToPersistentStoreAndWait()
    }
    
    func updateTask(task:YGTaskModel, title:String?, text:String?) {
    
        if (title != nil) { task.title = title }
        if (text != nil) { task.text = text }
    
        mainContext().mr_saveToPersistentStoreAndWait()
    }
    
    func markTaskCompletion(task: YGTaskModel, isCompleted: Bool) {
        
        task.completed = isCompleted
        task.completionDate = (isCompleted) ? NSDate() : nil
       
        mainContext().mr_saveToPersistentStoreAndWait()
    }
    
    func deleteTask(task: YGTaskModel) {
        
        task.mr_deleteEntity()
        
        mainContext().mr_saveToPersistentStoreAndWait()
    }
    
    func deleteOldTasks() {
        let tasks = YGTaskModel.mr_findAll() as! [YGTaskModel]
        
        let currentDate = Date()
        let calendar = NSCalendar.current
        
        for task in tasks {
            if (task.completed && task.completionDate != nil) {
                if (!calendar.isDate(currentDate, equalTo: task.completionDate! as Date, toGranularity: .day)) {
                    deleteTask(task: task)
                }
            }
        }
        mainContext().mr_saveToPersistentStoreAndWait()
    }
    
    //MARK: - Karma points
   
    func allKarmaPointsWith(_ status: Bool) -> [YGKarmaPointModel] {
        let points = YGKarmaPointModel.mr_findAllSorted(by: "creationDate", ascending: true, with: NSPredicate(format: "status = %@", NSNumber(booleanLiteral: status)))
        return points as! [YGKarmaPointModel]
    }

    func insertKarmaPointWith(_ status: Bool) {
        let karmaPointModel = YGKarmaPointModel.mr_createEntity(in: mainContext())
        karmaPointModel?.status = status
        karmaPointModel?.creationDate = Date() as NSDate
        
        mainContext().mr_saveToPersistentStoreAndWait()
    }
    
    func karmaPointCountWith(_ status: Bool) -> Int {
        let count =  YGKarmaPointModel.mr_countOfEntities(with: NSPredicate(format: "status = %@", NSNumber(booleanLiteral: status)), in: mainContext())
        return Int(count)
    }
    
    func deleteOldKarmaPoints() {
        let points = YGKarmaPointModel.mr_findAll() as! [YGKarmaPointModel]
        
        let currentDate = Date()
        let calendar = NSCalendar.current
        
        for point in points {
            if (point.creationDate != nil) {
                let result:ComparisonResult = calendar.compare(point.creationDate! as Date, to: currentDate, toGranularity: .day)
                if (result == ComparisonResult.orderedAscending) {
                    deleteKarmaPoint(point)
                }
            }
        }
        mainContext().mr_saveToPersistentStoreAndWait()
    }
    
    func deleteAllKarmaPoints() {
        YGKarmaPointModel.mr_deleteAll(matching: NSPredicate(format:"status >= 0"), in: mainContext())
        mainContext().mr_saveToPersistentStoreAndWait()
    }
    
    func deleteKarmaPoint(_ point: YGKarmaPointModel) {
        point.mr_deleteEntity()
        mainContext().mr_saveToPersistentStoreAndWait()
    }
    
}

