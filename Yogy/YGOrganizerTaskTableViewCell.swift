//
//  YGOrganizerTableViewCell.swift
//  Yogy
//
//  Created by Anna on 8/1/17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit
import SwipeCellKit

protocol YGOrganizerTableViewCellDelegate : class {
    func titleChangedAt(_ index: Int, title: String?)
    func textChangedAt(_ index: Int, text: String?)
    func doneButtonTappedAt(_ index: Int)
}

class YGOrganizerTaskTableViewCell: SwipeTableViewCell, UITextFieldDelegate, UITextViewDelegate {

    @IBOutlet private weak var taskTitle: UITextField!
    @IBOutlet private weak var taskBody: UITextView!
    @IBOutlet private weak var doneButton: UIButton!
    
    weak var cellDelegate: YGOrganizerTableViewCellDelegate?
    
    private var taskIndex = Int()

    func updateTaskInfo(title: String?, text: String?, status:Bool, index: Int) {
        
        taskIndex = index
        doneButton.isSelected = status
        taskTitle.text = title
        taskBody.text = text
    }

    @IBAction func doneButtonTap(_ sender: Any) {
        cellDelegate?.doneButtonTappedAt(taskIndex)
    }
    
    //MARK: - UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        cellDelegate?.titleChangedAt(taskIndex, title: textField.text)
        textField.resignFirstResponder()
        return false
    }
    
    //MARK: - UITextViewDelegate
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            cellDelegate?.textChangedAt(taskIndex, text: textView.text)
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}
