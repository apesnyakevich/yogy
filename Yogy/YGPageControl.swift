//
//  YGPageControl.swift
//  Yogy
//
//  Created by Anna on 9/5/17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

class YGPageControl: UIPageControl {
    
    let activeImage:UIImage = UIImage(named: "image_selectedPoint.png")!
    let activeImageBack:UIImage = UIImage(named: "image_selectedPoint_back.png")!

    let inactiveImage:UIImage = UIImage(named: "image_unselecnedPoint.png")!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.pageIndicatorTintColor = UIColor.clear
        self.currentPageIndicatorTintColor = UIColor.clear
        self.clipsToBounds = false
    }
    
    func updateDots() {
        var i = 0
        for view in self.subviews {
            view.removeAllSubviews()
            var dotImage = self.inactiveImage
            var backImage = UIImage()
            if i == currentPage {
                dotImage = activeImage
                backImage = activeImageBack
            }
            view.clipsToBounds = false
            
            let backImageView = UIImageView(image:backImage)
            let dotImageView = UIImageView(image:dotImage)
            
            view.addSubview(backImageView)
            view.addSubview(dotImageView)
            backImageView.center = CGPoint.init(x: view.bounds.midX, y: view.bounds.midY)
            dotImageView.center = CGPoint.init(x: backImageView.frame.midX, y: backImageView.frame.midY)
            
            i = i + 1
        }
    }
        
}
