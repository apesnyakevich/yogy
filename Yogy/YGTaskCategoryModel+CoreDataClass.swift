//
//  YGTaskCategoryModel+CoreDataClass.swift
//  
//
//  Created by Andrew Pesnyakevich on 04/08/2017.
//
//

import UIKit
import CoreData


public class YGTaskCategoryModel: NSManagedObject {
    
    func progress() -> CGFloat {
        
        var progress: CGFloat = 0.0
        
        let tasksCount = (self.tasks?.allObjects.count)!
        if (tasksCount > 0) {
            for task in (self.tasks?.allObjects)! {
                if ((task as! YGTaskModel).completed) {
                    progress += (1.0/CGFloat(tasksCount))
                }
            }
        }
        return progress
    }
}
