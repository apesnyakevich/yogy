//
//  YGCalendarViewController.swift
//  Yogy
//
//  Created by Anna on 26.08.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit
import CVCalendar

protocol YGCalendarViewControllerProtocol: class {
    
}

protocol YGCalendarViewControllerDelegate {
    func calendarViewControllerWillHide(_ calendarViewController: YGCalendarViewController)
}

class YGCalendarViewController: UIViewController, YGCalendarViewControllerProtocol {

    @IBOutlet fileprivate weak var calendarMenuView: CVCalendarMenuView!
    @IBOutlet fileprivate weak var calendarDayView: CVCalendarView!
    @IBOutlet fileprivate weak var monthYearLabel: UILabel!
    
    @IBOutlet var middleViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet var middleViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet fileprivate weak var calendarButton: YGBorderedBlueButton!
    @IBOutlet fileprivate weak var calendarModeSegment: UISegmentedControl!
    
    @IBOutlet fileprivate weak var adviceView: YGThemeDayAdviceView!
    @IBOutlet fileprivate weak var calendarView: YGThemeDayCalendarView!

    
    fileprivate var activeCalendarModeIndex: Int = YGThemeDayCalendarMode.Day.rawValue
    fileprivate var friendForCalculations: YGFriendModel?

  
    fileprivate var presenter: YGCalendarPresenterDelegate?
    var dataProvider: YGDataProviderProtocol?
    
    var delegate: YGCalendarViewControllerDelegate?
    

    //MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        calendarView.delegate = self
        calendarDayView.delegate = self
        calendarMenuView.delegate = self
        
        updatePresenter()
        addViewGestures()
        removeSlideMenuGestures()
        localizeUI()
    }
    
    deinit {
        print("lol")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (friendForCalculations != nil) {
            presenter?.setFriendForCalculations(friendForCalculations!)
        }

        updateCalendarView()
        updateAdviceView()
        updateNavigationBar()
        
        navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        calendarDayView.commitCalendarViewUpdate()
        calendarMenuView.commitMenuViewUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.navigationBar.isHidden = true
        addSlideMenuGestures()
    }

    //MARK: - Private
    
    fileprivate func updatePresenter() {
        let model = YGCalendarModel()
        let presenter = YGCalendarPresenter.init(view: self, model: model, dataProvider: dataProvider)
        self.presenter = presenter
    }
    
    fileprivate func updateAdviceView() {
        adviceView.updateAdviceTitle(presenter?.adviceTitle())
        adviceView.updateAdviceText(presenter?.adviceText())
        adviceView.updateAdviceImage(presenter?.adviceImage())
    }
    
    fileprivate func updateCalendarView() {
        calendarView.calendarMode = activeCalendarModeIndex
    }
    
    private func localizeUI() {
        calendarModeSegment.setTitle(String.localize("Day"), forSegmentAt: YGDestinyCodeMode.Destiny.rawValue)
        calendarModeSegment.setTitle(String.localize("Month"), forSegmentAt: YGDestinyCodeMode.Karma.rawValue)
        calendarModeSegment.setTitle(String.localize("Year"), forSegmentAt: YGDestinyCodeMode.Potential.rawValue)
    }
    
    func updateNavigationBar() {
        self.title = presenter?.friendName()
        
        let leftButton: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "image_arrow_left"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.backButtonTapped(_:)))
        navigationItem.leftBarButtonItem = leftButton
    }
    
    //MARK: - Actions
    
    @IBAction func calendarButtonTapped(_ sender: UIButton) {
        if (sender.isSelected) {
            moveCalendarDown()
        }
        else {
            moveCalendarUp()
        }
    }
    
    @IBAction func calendarSegmentChanged(_ sender: UISegmentedControl) {
        
        if (sender.selectedSegmentIndex > activeCalendarModeIndex) {
            activeCalendarModeIndex = sender.selectedSegmentIndex
            animateMiddleViewSwipeWithDirection(kCATransitionFromRight)
        }
        else if (sender.selectedSegmentIndex < activeCalendarModeIndex) {
            activeCalendarModeIndex = sender.selectedSegmentIndex
            animateMiddleViewSwipeWithDirection(kCATransitionFromLeft)
        }
    }
    
    private var isSwipeInButton = false
    
    @IBAction func swipeInButton(_ sender: Any) {
        isSwipeInButton = true
    }
    
    
    @IBAction func showAdvicePopUp(_ sender: Any) {
        if (!isSwipeInButton) {
            let popupView = YGCalendarAdvicePopupView.init(frame: view.frame)
            popupView.loadAdviсe(presenter!.adviceText())
            view.addSubview(popupView)
            popupView.runAnimation()
        } else {
            isSwipeInButton = false
        }
    }
    
    
    //MARK: - Move Animations
    
    fileprivate func moveCalendarUp() {
        calendarButton.isSelected = true
        adviceView.setTitleViewHidden(true)
        
        middleViewTopConstraint.isActive = true
        middleViewBottomConstraint.isActive = false
        
        UIView.animate(withDuration: 0.4, animations: {
            self.view.layoutIfNeeded()
        }, completion: { (finished) in
            self.calendarView.isHidden = false
            self.adviceView.setAdviceImageViewHidden(true)
        })
    }
    
    fileprivate func moveCalendarDown() {
        calendarButton.isSelected = false
        adviceView.setTitleViewHidden(false)
        adviceView.setAdviceImageViewHidden(false)

        middleViewTopConstraint.isActive = false
        middleViewBottomConstraint.isActive = true
        
        UIView.animate(withDuration: 0.4, animations: {
            self.calendarView.isHidden = true
            self.view.layoutIfNeeded()
        }, completion: { (finished) in
        })
    }
    
    //MARK: - Gestures
    
    fileprivate func addViewGestures() {
        addSwipeGestureRecognizer(targetView: adviceView, direction: .up)
        addSwipeGestureRecognizer(targetView: adviceView, direction: .down)
        
        addSwipeGestureRecognizer(targetView: calendarView, direction: .up)
        addSwipeGestureRecognizer(targetView: calendarView, direction: .down)
        
        addSwipeGestureRecognizer(targetView: view, direction: .left)
        addSwipeGestureRecognizer(targetView: view, direction: .right)
    }
    
    override func handleSwipeGesture(gesture: UISwipeGestureRecognizer) {
        
       if (gesture.view == adviceView || gesture.view == calendarView) {
            if (gesture.direction == .up) {
                moveCalendarUp()
            }
            else if (gesture.direction == .down){
                if (calendarButton.isSelected) {
                    moveCalendarDown()
                }
                else {
                   delegate?.calendarViewControllerWillHide(self)
                }
            }
        }
        else if (gesture.view == view) {
            if (gesture.direction == .left) {
                if (activeCalendarModeIndex < (YGThemeDayCalendarMode.All.rawValue - 1)) {
                    activeCalendarModeIndex += 1
                    calendarModeSegment.selectedSegmentIndex = activeCalendarModeIndex
                    animateMiddleViewSwipeWithDirection(kCATransitionFromRight)
                }
            }
            else if (gesture.direction == .right) {
                if (activeCalendarModeIndex > 0) {
                    activeCalendarModeIndex -= 1
                    calendarModeSegment.selectedSegmentIndex = activeCalendarModeIndex
                    animateMiddleViewSwipeWithDirection(kCATransitionFromLeft)
                }
            }
        }
    }
    
    fileprivate func animateMiddleViewSwipeWithDirection(_ direction: String) {
        let animation = CATransition()
        animation.type = kCATransitionPush
        animation.subtype = direction
        animation.duration = 0.5
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        
        self.view.exchangeSubview(at: 0, withSubviewAt: 0)
        self.view.layer.add(animation, forKey: nil)
        
        presenter?.setActiveCalendarModeIndex(activeCalendarModeIndex)
        
        updateCalendarView()
        updateAdviceView()
    }
    
    //MARK: - Public
    
    func setFriendForCalculations(_ friend: YGFriendModel) {
        self.friendForCalculations = friend
    }
}

//MARK: - CVCalendarViewDelegate

extension YGCalendarViewController: CVCalendarViewDelegate, CVCalendarMenuViewDelegate, CVCalendarViewAppearanceDelegate {
    
    func presentationMode() -> CalendarMode {
        return .monthView
    }
    
    func firstWeekday() -> Weekday {
        return .monday
    }
    
    func dayLabelWeekdaySelectedBackgroundColor() -> UIColor {
        return UIColor.customBlueColor()
    }
    
    func dayLabelPresentWeekdayTextColor() -> UIColor {
        return UIColor.customBlueColor()
    }
    
    func dayLabelPresentWeekdaySelectedTextColor() -> UIColor {
        return UIColor.white
    }
    
    func dayLabelPresentWeekdaySelectedBackgroundColor() -> UIColor {
        return UIColor.customBlueColor()
    }
    
    func didShowNextMonthView(_ date: Date) {
        monthYearLabel.text = YGAppUtils.outputMonthYearDateFormatter().string(from: date)
        presenter?.setActiveDate(date as NSDate)
        updateAdviceView()
    }
    
    func didShowPreviousMonthView(_ date: Date) {
        monthYearLabel.text = YGAppUtils.outputMonthYearDateFormatter().string(from: date)
        presenter?.setActiveDate(date as NSDate)
        updateAdviceView()
    }
    
    func didSelectDayView(_ dayView: DayView, animationDidFinish: Bool) {
        
        if (presenter != nil) {
            let selectedDate: Date = YGAppUtils.dateFromDate(Date(), dayView.date.day)
            presenter?.setActiveDate(selectedDate as NSDate)
            updateAdviceView()
        }
    }
}

//MARK: - YGThemeDayCalendarViewDelegate

extension YGCalendarViewController: YGThemeDayCalendarViewDelegate {
    
    func calendarDateChanged(newDate: Date) {
        presenter?.setActiveDate(newDate as NSDate)
        updateAdviceView()
    }
}
