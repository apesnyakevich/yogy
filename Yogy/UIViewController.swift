//
//  UIViewControllerExtension.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 1/19/15.
//  Copyright (c) 2015 Yuji Hato. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

extension UIViewController {
    
    func setNavigationBarItem() {
        self.addLeftBarButtonWithImage(UIImage(named: "ic_menu_black_24dp")!)
        self.addRightBarButtonWithImage(UIImage(named: "ic_notifications_black_24dp")!)
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
        self.slideMenuController()?.addLeftGestures()
        self.slideMenuController()?.addRightGestures()
    }
    
    func removeNavigationBarItem() {
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItem = nil
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func addSlideMenuGestures() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.rootController.addRightGestures()
        appDelegate.rootController.addLeftGestures()
    }
    
    func addOnlyLeftGestures() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.rootController.removeRightGestures()
        appDelegate.rootController.addLeftGestures()
    }
    
    func removeSlideMenuGestures() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.rootController.removeRightGestures()
        appDelegate.rootController.removeLeftGestures()
    }
    
    func addSwipeGestureRecognizer(targetView: UIView, direction: UISwipeGestureRecognizerDirection) {
        
        let swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipeGesture(gesture:)))
        swipeGesture.direction = direction
        swipeGesture.cancelsTouchesInView = false
        targetView.addGestureRecognizer(swipeGesture)
    }
    
    func handleSwipeGesture(gesture: UISwipeGestureRecognizer) {
        
    }
    
    //MARK: - Actions
    
    func pop() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        self.pop()
    }
}

