//
//  YGSubscriptionViewController.swift
//  Yogy
//
//  Created by apesnyakevich on 04.09.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGSubscriptionViewControllerDelegate: class {
    
}

class YGSubscriptionViewController: UIViewController, YGSubscriptionViewControllerDelegate {

    @IBOutlet weak var tableView: UITableView!

    fileprivate var presenter: YGSubscriptionPresenterDelegate?
    var dataProvider: YGDataProviderProtocol?

    //MARK: - View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = String.localize("Subscription")
        
        updatePresenter()
        
        tableView.tableFooterView = UIView()
        tableView.estimatedRowHeight = 165
        tableView.rowHeight = UITableViewAutomaticDimension
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }

    //MARK: - Actions

    @IBAction func leftBarButtonTapped(_ sender: Any) {
        //        self.toggleLeft()
    }
    
    @IBAction func rightBarButtonTapped(_ sender: Any) {
        self.toggleLeft()
    }
    
    //MARK: - Private
    fileprivate func updatePresenter() {
        let model = YGSubscriptionModel()
        let presenter = YGSubscriptionPresenter.init(view:self, model:model, dataProvider: dataProvider)
        self.presenter = presenter
    }
}


    //MARK: - UITableViewDataSource & UITableViewDelegate

    extension YGSubscriptionViewController: UITableViewDataSource, UITableViewDelegate {
        
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            return 0.5
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return (indexPath.row == 0) ? 130 : 165
        }
        
        func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            
            let headerView = UIView(frame: CGRect(x:0, y:0, width: tableView.frame.width, height: 0.5))
            headerView.backgroundColor = tableView.separatorColor
            
            return headerView
        }
        
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return presenter!.rowCount()
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier:presenter!.cellIdentifierAt(indexPath), for: indexPath)
            
            (cell as! YGSubscriptionTableViewCellProtocol).updateInfoWithModel(model: presenter!.cellModelAt(indexPath))
            
            return cell
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            tableView.deselectRow(at: indexPath, animated: true)
            
            let cell = tableView.cellForRow(at: indexPath)
            cell?.layoutIfNeeded()
            presenter?.cellSelectedAt(indexPath)
            
            YGAppUtils.setSubscriptionNumber(indexPath.row)
            tableView .reloadData()
        }
    }

