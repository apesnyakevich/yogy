//
//  YGAwarenessTableViewCell.swift
//  Yogy
//
//  Created by Anna on 17.08.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGAwarenessTableViewCellDelegate {
    func awarenessCellDidShowInfo(_ cell: YGAwarenessTableViewCell)
    func awarenessCellDidHideInfo(_ cell: YGAwarenessTableViewCell)
    func addNegativeKarmaPoint()
    func addPositiveKarmaPoint()
}

class YGAwarenessTableViewCell: UITableViewCell {
    
    @IBOutlet fileprivate weak var infoView: YGKarmaInfoView!
    @IBOutlet fileprivate weak var positiveKarmaView: YGKarmaPointView!
    @IBOutlet fileprivate weak var negativeKarmaView: YGKarmaPointView!
    @IBOutlet fileprivate weak var infoButton: UIButton!
    
    @IBOutlet fileprivate var showInfoConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate var infoViewHeightConstraint: NSLayoutConstraint!
    
    var isShowingInfo = false
    
    var index = 0

    var delegate: YGAwarenessTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        hideInfo()
    }
    
    func updateInfoWith(_ model: YGKarmaInfoModel) {
        
        let positiveModel = YGKarmaPointViewModel(text: model.positiveText!, isNegative: false)
        let negativeModel = YGKarmaPointViewModel(text: model.negativeText!, isNegative: true)
    
        
        positiveKarmaView.updateInfoWithModel(positiveModel)
        negativeKarmaView.updateInfoWithModel(negativeModel)
        infoView.updateInfoWith(model.neutralText!)
        
        positiveKarmaView.delegate = self
        negativeKarmaView.delegate = self
        
        if (isShowingInfo) {
            showInfo()
            infoButton.isHidden = true
        } else {
            hideInfo()
            infoButton.isHidden = false
            infoButton.alpha = 1
        }
    }
    
    private func hideInfo() {
        infoViewHeightConstraint.isActive = true
        infoView.isHidden = true
        showInfoConstraint.isActive = false
        infoButton.isHidden = false
    }
    
    private func showInfo() {
        infoViewHeightConstraint.isActive = false
        infoView.isHidden = false
        showInfoConstraint.isActive = true
    }

    @IBAction private func showInfo(_ sender: UIButton) {
        showInfo()
        
        UIView.animate(withDuration: 0.5, animations: {
            sender.alpha = 0
        }) { (_) in
            sender.isHidden = true
        }
        delegate?.awarenessCellDidShowInfo(self)
    }
    
    @IBAction private func hideInfo(_ sender: Any) {
        hideInfo()
        
        infoButton.isHidden = false
        infoButton.alpha = 0

        UIView.animate(withDuration: 0.5, animations: {
            self.infoButton.alpha = 1
        })
        delegate?.awarenessCellDidHideInfo(self)
    }
}


extension YGAwarenessTableViewCell: YGKarmaViewDelegate {
    
    func addNegativeKarmaPoint() {
        delegate?.addNegativeKarmaPoint()
    }
    
    func addPositiveKarmaPoint() {
        delegate?.addPositiveKarmaPoint()
    }
}
