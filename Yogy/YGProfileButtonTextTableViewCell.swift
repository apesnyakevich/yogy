//
//  YGProfileButtonTextTableViewCell.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 05/08/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

class YGProfileButtonTextTableViewCell: UITableViewCell, YGProfileTableViewCellProtocol {

    @IBOutlet weak var subscriptionButton: UIButton!
    @IBOutlet weak var subscriptionLabel: UILabel!

    func updateInfoWithModel(model: YGProfileTableViewCellModel) {
        
        if (model.statusActive != nil) {
            if (model.statusActive!) {
                subscriptionButton.setImage(UIImage(named:"icon_status_active"), for:.normal)
            }
            else {
                subscriptionButton.setImage(UIImage(named:"icon_status_inactive"), for:.normal)
            }
        }
        
        subscriptionLabel.text = model.description
    }
}
