//
//  YGUserModel+CoreDataProperties.swift
//  
//
//  Created by apesnyakevich on 14.08.17.
//
//

import Foundation
import CoreData


extension YGUserModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<YGUserModel> {
        return NSFetchRequest<YGUserModel>(entityName: "YGUserModel")
    }

    @NSManaged public var birthDate: NSDate?
    @NSManaged public var email: String?
    @NSManaged public var name: String?
    @NSManaged public var password: String?
    @NSManaged public var photoPath: String?
    @NSManaged public var subscriptionEnabled: Bool
    @NSManaged public var surname: String?
    @NSManaged public var friends: NSSet?

}

// MARK: Generated accessors for friends
extension YGUserModel {

    @objc(addFriendsObject:)
    @NSManaged public func addToFriends(_ value: YGFriendModel)

    @objc(removeFriendsObject:)
    @NSManaged public func removeFromFriends(_ value: YGFriendModel)

    @objc(addFriends:)
    @NSManaged public func addToFriends(_ values: NSSet)

    @objc(removeFriends:)
    @NSManaged public func removeFromFriends(_ values: NSSet)

}
