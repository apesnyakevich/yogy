//
//  YGFriendEditTableViewCellProtocol.swift
//  Yogy
//
//  Created by Anna on 8/11/17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGFriendEditTableViewCellProtocol {
    func updateInfoWith(_ model: YGFriendEditTableViewCellModel)
}
