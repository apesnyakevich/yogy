//
//  YGCalendarMonthView.swift
//  Yogy
//
//  Created by apesnyakevich on 17.08.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGCalendarMonthViewDelegate: class {
    func monthChanged(_ month:Int)
}

class YGCalendarMonthView: UIView {
    
    var monthButtons:[UIButton]!
    var monthNames:[String]!
    var selectedMonth: Int!
    weak var delegate: YGCalendarMonthViewDelegate?
    
    @IBOutlet fileprivate var contentView: UIView!
    @IBOutlet private weak var monthButton0: UIButton!
    @IBOutlet private weak var monthButton1: UIButton!
    @IBOutlet private weak var monthButton2: UIButton!
    @IBOutlet private weak var monthButton3: UIButton!
    @IBOutlet private weak var monthButton4: UIButton!
    @IBOutlet private weak var monthButton5: UIButton!
    @IBOutlet private weak var monthButton6: UIButton!
    @IBOutlet private weak var monthButton7: UIButton!
    @IBOutlet private weak var monthButton8: UIButton!
    @IBOutlet private weak var monthButton9: UIButton!
    @IBOutlet private weak var monthButton10: UIButton!
    @IBOutlet private weak var monthButton11: UIButton!
    
    //MARK: - View lifecycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed(String(describing: YGCalendarMonthView.self), owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        monthNames = [String.localize("January"), String.localize("February"), String.localize("March"),
                      String.localize("April"), String.localize("May"), String.localize("June"),
                      String.localize("July"), String.localize("August"), String.localize("September"),
                      String.localize("October"), String.localize("November"), String.localize("December")
                    ]
        
        monthButtons = [monthButton0!, monthButton1!, monthButton2!, monthButton3!, monthButton4!, monthButton5!,
                        monthButton6!, monthButton7!, monthButton8!, monthButton9!, monthButton10!, monthButton11!]
        
        self.setMonthCurrent()
        self.adjustAllButtons()
    }
    
    //MARK: - Methods
    
    private func setMonthCurrent() {
        let dateNow: Date = Date()
        let monthCurrent = Calendar.current.component(.month, from: dateNow)
        
        monthButtons[monthCurrent - 1].isSelected = true
        selectedMonth = monthCurrent
    }

    private func adjustAllButtons() {
        monthButtons[selectedMonth - 1].isSelected = true
        
        for (index, button) in monthButtons.enumerated() {
            button.cornerRadius = button.frame.height/2.0
            button.setBackgroundImage(UIImage(color:UIColor.customBlueColor(), size:button.size()), for: .selected)
            
            button.setTitle(monthNames[index], for: .normal)
        }
    }
    
    //MARK: - Actions
    
    @IBAction func didButtonTapped(_ sender: UIButton) {
        for (index, button) in monthButtons.enumerated() {
            if (button == sender) {
                button.isSelected = true
                selectedMonth = index + 1
                print(selectedMonth)
                
                delegate?.monthChanged(selectedMonth)
                
            } else {
                button.isSelected = false
            }
        }
    }
}
