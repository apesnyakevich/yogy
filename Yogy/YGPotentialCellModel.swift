//
//  YGPotentialCellModel.swift
//  Yogy
//
//  Created by apesnyakevich on 05.09.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

enum YGPotentialCellModelStatus: Int {
    case Red = 0
    case Yellow
    case Green
    case None
}

struct YGPotentialCellModel
{
    var status: YGPotentialCellModelStatus!
    var progress: CGFloat
    var title: String?
    var description: String?
    var comment: String?
    
    func color() -> UIColor {
        var color: UIColor
        
        if (status == YGPotentialCellModelStatus.Red) {
            color = UIColor.customRedColor()
        } else if (status == YGPotentialCellModelStatus.Yellow) {
            color = UIColor.customYellowColor()
        } else  {
            color = UIColor.customGreenColor()
        }
        
        return color
    }
    
    func image() -> UIImage {
        var image: UIImage
        
        if (status == YGPotentialCellModelStatus.Red) {
            image = #imageLiteral(resourceName: "icon_finger_negative")
        } else if (status == YGPotentialCellModelStatus.Yellow) {
            image = #imageLiteral(resourceName: "icon_finger_neutral")
        } else  {
            image = #imageLiteral(resourceName: "icon_finger_positive")
        }
        
        return image
    }
    
    func backgroundImage() -> UIImage {
        var image: UIImage
        
        if (status == YGPotentialCellModelStatus.Red) {
            image = #imageLiteral(resourceName: "icon_red_ellipse")
        } else if (status == YGPotentialCellModelStatus.Yellow) {
            image = #imageLiteral(resourceName: "icon_yellow_ellipse")
        } else  {
            image = #imageLiteral(resourceName: "icon_green_ellipse")
        }
        
        return image
    }
}
