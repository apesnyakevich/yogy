//
//  YGPsychomatrixViewController.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 11/09/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGPsychomatrixViewControllerDelegate {
    
}

class YGPsychomatrixViewController: UIViewController, YGPsychomatrixViewControllerDelegate {

    @IBOutlet fileprivate weak var matrixTableView: UITableView!
    
    fileprivate var presenter: YGPsychomatrixPresenterDelegate?
    var dataProvider: YGDataProviderProtocol?
    
    //MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        updatePresenter()
        
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        updateUI()
    }
    
    func updateUI() {
        matrixTableView.reloadData()
    }
    
    //MARK: - Private
    
    private func updatePresenter() {
        let model = YGPsychomatrixModel()
        let presenter = YGPsychomatrixPresenter(view: self, model: model, dataProvider: dataProvider)
        self.presenter = presenter
    }
    
    private func setupTableView() {
        matrixTableView.rowHeight = 70.0
    }
}

//MARK: - TableViewDataSource

extension YGPsychomatrixViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter!.cellCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String.className(YGPsychomatrixTableViewCell.self), for: indexPath) as! YGPsychomatrixTableViewCell
        
        let index = indexPath.row
        
        cell.updateColor((presenter?.cellColorAt(index))!)
        cell.updateTitle((presenter?.cellTitleAt(index))!)
        cell.updateMatrixValue((presenter?.cellMatrixValueAt(index))!)
        
        return cell
    }
}


