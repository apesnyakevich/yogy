//
//  YGThemeDayModel.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 07/08/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

struct YGThemeDayModel {
    var categories = [YGTaskCategoryModel]()
}
