//
//  YGSubscriptionModel.swift
//  Yogy
//
//  Created by apesnyakevich on 04.09.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

enum YGSubscriptionCellType: Int {
    case YGSubscriptionCellTypeFree = 0
    case YGSubscriptionCellTypeStandard
    case YGSubscriptionCellTypePremium
    case YGSubscriptionCellTypeFull
    case YGSubscriptionCellTypeCount
}

struct YGSubscriptionModel {
    
}

