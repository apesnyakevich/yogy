//
//  YGFriendEditViewController.swift
//  Yogy
//
//  Created by Anna on 10.08.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGFriendEditViewControllerDelegate: class {
    func backToFriendList()
    func showAlertWith(_ message: String)
}

class YGFriendEditViewController: UIViewController {

    @IBOutlet fileprivate weak var titleLabel: UILabel!
    @IBOutlet fileprivate weak var editPhotoButton: UIButton!
    @IBOutlet fileprivate weak var friendEditTableView: UITableView!
    @IBOutlet fileprivate weak var editFriendButton: UIButton!
    
    @IBOutlet fileprivate weak var photoImageView: UIImageView!
    
    fileprivate var presenter: YGFriendEditPresenterDelegate?
    var dataProvider: YGDataProviderProtocol?
    private var currentFriendModel: YGFriendModel?
    
    fileprivate var imagePicker = YGImagePicker()
    
    //MARK: - View Controller lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        
        updatePresenter()
        hideKeyboardWhenTappedAround()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        photoImageView.layer.cornerRadius = 0.5 * photoImageView.bounds.size.width
        photoImageView.clipsToBounds = true
        
        updatePhotoView()
        loadTitles()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func friendModel(_ model: YGFriendModel?) {
        currentFriendModel = model
    }
    
    private func updatePresenter() {
        let model = YGFriendEditModel()
        let presenter = YGFriendEditPresenter(view: self, model: model, dataProvider: dataProvider)
        self.presenter = presenter
        
        if (currentFriendModel != nil) {
            self.presenter?.friend(currentFriendModel)
        }
    }
    
    private func loadTitles() {
        var title = String()
        if (currentFriendModel != nil) {
            title = "Edit friend"
        } else {
            title = "Add friend"
        }
        self.titleLabel.text = title
    }

    //MARK: - Actions
    
    @IBAction func editFriend(_ sender: Any) {
        presenter?.updateFriend()
    }

    @IBAction func photoButtonTap(_ sender: Any) {
        showImagePicker(sender)
    }
    
    //MARK: - Private
    
    fileprivate func updatePhotoView(){
        let photo = presenter?.friendPhoto()
        if (photo != nil) {
            photoImageView.image = photo
        }
    }
    
    fileprivate func showImagePicker(_ sender: Any) {
        view.endEditing(true)
        
        // Create An UIAlertController with Action Sheet
        
        let optionMenuController = UIAlertController(title: nil, message: String.localize("Choose photo source"), preferredStyle: .actionSheet)
        
        // Create UIAlertAction for UIAlertController
        let addAction = UIAlertAction(title: String.localize("Camera"), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.imagePicker.cameraAccessRequest()
            }
        })
        let saveAction = UIAlertAction(title: String.localize("Gallery"), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.requestPermissionAndPresentPicker(sender)
        })
        
        let cancelAction = UIAlertAction(title: String.localize("Cancel"), style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        // Add UIAlertAction in UIAlertController
        optionMenuController.addAction(addAction)
        optionMenuController.addAction(saveAction)
        optionMenuController.addAction(cancelAction)
        
        // Present UIAlertController with Action Sheet
        self.present(optionMenuController, animated: true, completion: nil)
    }
    
    fileprivate func presentImagePicker(sourceType: UIImagePickerControllerSourceType) {
        imagePicker.controller.sourceType = sourceType
        DispatchQueue.main.async {
            self.present(self.imagePicker.controller, animated: true, completion: nil)
        }
    }
    
    fileprivate func requestPermissionAndPresentPicker(_ sender: Any) {
        imagePicker.galleryAccessRequest()
    }

}

extension YGFriendEditViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter!.rowCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: presenter!.cellIdentifierAt(indexPath), for: indexPath)
        
        (cell as! YGFriendEditTableViewCellProtocol).updateInfoWith(presenter!.cellModelAt(indexPath))
        
        if (cell.isKind(of: YGFriendEditBirthdateTableViewCell.self)) {
            (cell as! YGFriendEditBirthdateTableViewCell).delegate = self
        } else {
            (cell as! YGFriendEditTableViewCell).delegate = self
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        presenter?.cellSelectedAt(indexPath)
        
        let cell = tableView.cellForRow(at: indexPath)
        cell?.becomeFirstResponder()
    }
    
}

extension YGFriendEditViewController : YGFriendEditViewControllerDelegate {
    func backToFriendList() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func showAlertWith(_ message: String) {
        let alert = UIAlertController.init(title: String.localize("Warning!"), message: message, preferredStyle: .alert)
        let doneAction = UIAlertAction.init(title: String.localize("OK"), style: .cancel, handler: nil)
        
        alert.addAction(doneAction)
        
       present(alert, animated: true, completion: nil)
    }
    
}

extension YGFriendEditViewController: YGFriendEditBirthdateTableViewCellDelegate {
    func datePickerChanged(date: NSDate) {
        DispatchQueue.main.async {
            self.presenter?.updateDate(date)
            self.friendEditTableView.reloadData()
        }
    }
}

extension YGFriendEditViewController: YGFriendEditTableViewCellDelegate {
    func textFieldUpdate(_ value: String) {
        presenter?.updateFullName(value)
    }
}

//MARK: - YGImagePickerDelegate

extension YGFriendEditViewController: YGImagePickerDelegate {
    
    
    func imagePickerDelegate(didSelect image: UIImage, imageName:String, delegatedForm: YGImagePicker) {
        
        presenter?.updateFriendPhoto(image)
        
        updatePhotoView()
        
        imagePicker.dismiss()
    }
    
    func imagePickerDelegate(didCancel delegatedForm: YGImagePicker) {
        imagePicker.dismiss()
    }
    
    func imagePickerDelegate(canUseGallery accessIsAllowed: Bool, delegatedForm: YGImagePicker) {
        if accessIsAllowed {
            presentImagePicker(sourceType: .photoLibrary)
        }
    }
    
    func imagePickerDelegate(canUseCamera accessIsAllowed: Bool, delegatedForm: YGImagePicker) {
        if accessIsAllowed {
            presentImagePicker(sourceType: .camera)
        }
    }
}

