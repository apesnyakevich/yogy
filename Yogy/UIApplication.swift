//
//  UserDefaults.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 07/08/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

private var firstLaunch : Bool = false

extension UIApplication {
    
    static func isFirstLaunch() -> Bool {
        let firstLaunchFlag = "isFirstLaunchFlag"
        let isFirstLaunch = !UserDefaults.standard.bool(forKey: firstLaunchFlag)
        if (isFirstLaunch) {
            firstLaunch = isFirstLaunch
            UserDefaults.standard.set(true, forKey: firstLaunchFlag)
            UserDefaults.standard.synchronize()
        }
        return firstLaunch || isFirstLaunch
    }
}
