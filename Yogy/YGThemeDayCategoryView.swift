//
//  YGThemeDayCategoryView.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 07/08/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGThemeDayCategoryViewDelegate: class {
    func didSelectCategoryAt(_ index: Int)
}

class YGThemeDayCategoryView: UIView {
    
    @IBOutlet fileprivate var contentView: UIView!
    @IBOutlet fileprivate weak var categoryButton: UIButton!
    @IBOutlet fileprivate weak var categoryImageView: UIImageView!
    
    @IBOutlet fileprivate weak var outerBorderView: UIView!
    @IBOutlet fileprivate weak var innerBorderView: UIView!
    @IBOutlet fileprivate weak var progressView: YGCircleView!
    
    //MARK: - Properties
    
    weak var delegate: YGThemeDayCategoryViewDelegate?
    
    private var categoryIndex: Int?
    
    private var categoryColorHex: String?
    
    private var progress: CGFloat? = 0.0
    
    func updateCategoryInfo(index: Int, hexColor: String?, iconName: String?, progress: CGFloat?) {
        
        categoryIndex = index
        
        if (!(iconName ?? "").isEmpty) {
            categoryImageView.image = UIImage(named: iconName!)
        }
        
        if (!(hexColor ?? "").isEmpty) {
            updateViewColor(hexColor!)
        }
        
        self.progress = progress
    }
    
    //MARK: - View lifecycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed(String(describing: YGThemeDayCategoryView.self), owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        categoryButton.isExclusiveTouch = true
        
        outerBorderView.cornerRadius = outerBorderView.width()/2
        outerBorderView.borderWidth = 1.0
        
        innerBorderView.cornerRadius = innerBorderView.width()/2
        
        progressView.circleWidth = 4.0
    }
    
    func animateCategoryView(top: CGFloat, centerX: CGFloat, delay: CGFloat, animated: Bool) {
        
        progressView.animateCircle(duration: 0.0, progress: 0)
        
        if (animated) {
            UIView.animate(withDuration: 0.3,
                           delay: TimeInterval(delay),
                           options: .curveEaseInOut,
                           animations: {
                            let _ = self.top(top)
                            let _ = self.centerX(centerX)
                            self.alpha = 1
                            self.superview?.layoutIfNeeded()
            },
                           completion: {(finished) in
                            if (finished) {
                                self.animateProgressView(self.progress!)
                            }
            })
        }
        else {
            let _ = self.top(top)
            let _ = self.centerX(centerX)
            self.alpha = 1
        }
    }
    
    private func animateProgressView(_ progress: CGFloat) {
        progressView.isHidden = false
        progressView.animateCircle(duration: 0.5, progress: progress)
    }
    
    private func updateViewColor(_ hex: String) {
        
        categoryColorHex = hex
        
        let circle = CAShapeLayer()
        circle.path = UIBezierPath(roundedRect: contentView.bounds, cornerRadius: contentView.frame.width/2).cgPath
        circle.fillColor = UIColor.clear.cgColor
        circle.strokeColor = UIColor(hex: hex, alpha: 0.5).cgColor
        circle.lineWidth = 1;
        circle.lineDashPattern = [4, 2]
        contentView.layer.addSublayer(circle)
        
        outerBorderView.borderColor = UIColor(hex: hex, alpha: 0.8)
        
        innerBorderView.backgroundColor = UIColor(hex: hex, alpha: 0.2)
        
        progressView.circleColor = UIColor(hex: hex)
    }
    
    @IBAction func categoryButtonTap(_ sender: Any) {
        let hex: String = categoryColorHex!
        
        categoryButton.isUserInteractionEnabled = false
        
        UIView.animate(withDuration: 0.2,
                       delay: 0.0,
                       options: .curveEaseInOut,
                       animations: {
                        self.innerBorderView.backgroundColor = UIColor(hex: hex, alpha: 0.6)
        },
                       completion: {(finished) in
                        self.innerBorderView.backgroundColor = UIColor(hex: hex, alpha: 0.2)
                        self.categoryButton.isUserInteractionEnabled = true
                        
                        self.delegate?.didSelectCategoryAt(self.categoryIndex!)
        })
    }
    
}
