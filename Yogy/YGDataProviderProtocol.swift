//
//  YGDataProviderProtocol.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 04/08/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import Foundation
import MagicalRecord

protocol YGDataProviderProtocol {
    
    //MARK: - Users
    func currentUser() -> YGUserModel? //first user in table
    func insertUser(name: String, surname: String, email: String, password: String, birthday: NSDate)
    func updateCurrentUserPhotoPath(path: String)
    func updateCurrentUserBirthdate(date: NSDate)
    func deleteCurrentUser() //delete all users in table (always one)
    
    //MARK: - Friends
    func allFriends() -> [YGFriendModel]
    func insertFriend(name: String?, surname: String?, photoPath: String?, birthday: NSDate?)
    func updateFriend(friend: YGFriendModel, name: String?, surname: String?, photoPath: String?, birthday: NSDate?)
    func deleteFriend(friend: YGFriendModel)
    
    //MARK: - Task Categories
    func allTaskCategories() -> [YGTaskCategoryModel] //sorted by category index
    func insertCategory(index: Int, title: String?, colorCode: String?, iconName: String?)
    func deleteAllCategories()
    
    //MARK: - Tasks
    func insertTask(title: String, text: String, category: YGTaskCategoryModel)
    func updateTask(task: YGTaskModel, title: String?, text: String?)
    func markTaskCompletion(task: YGTaskModel, isCompleted: Bool)
    func deleteTask(task: YGTaskModel)
    func deleteOldTasks()
    
    //MARK: - Karma points
    
    func allKarmaPointsWith(_ status: Bool) -> [YGKarmaPointModel]
    func insertKarmaPointWith(_ status: Bool)
    func karmaPointCountWith(_ status: Bool) -> Int
    func deleteOldKarmaPoints()
    func deleteAllKarmaPoints()
    func deleteKarmaPoint(_ point: YGKarmaPointModel)
    
}
