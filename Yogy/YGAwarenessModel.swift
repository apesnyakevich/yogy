//
//  YGAwarenessModel.swift
//  Yogy
//
//  Created by Anna on 17.08.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

struct YGKarmaInfoModel {
    var positiveText: String?
    var neutralText: String?
    var negativeText: String?
}

class YGAwarenessModel: NSObject {

    var modelsForTableView = [YGKarmaInfoModel]()
    
    var showingInfoCellIndices = [Int]()
    
    var positiveKarmaPoints = [YGKarmaPointModel]()
    var negativeKarmaPoints = [YGKarmaPointModel]()

}
