//
//  YGFriendEditBirthdateTableViewCell.swift
//  Yogy
//
//  Created by Anna on 8/11/17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGFriendEditBirthdateTableViewCellDelegate: class {
    func datePickerChanged(date:NSDate)
}

class YGFriendEditBirthdateTableViewCell: UITableViewCell, YGFriendEditTableViewCellProtocol {

    @IBOutlet weak var dateTextField: UITextField!
    
    private var datePicker:UIDatePicker!
    var delegate: YGFriendEditBirthdateTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setDatePicker()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    
    func updateInfoWith(_ model: YGFriendEditTableViewCellModel) {
        if (model.placeholder != nil) {
            dateTextField.text = model.placeholder
        }
    }

    
    override func becomeFirstResponder() -> Bool {
        dateTextField.isUserInteractionEnabled = true
        dateTextField.becomeFirstResponder()

        return true
    }
    
    /// A date formatter to format the `date` property of `datePicker`.
    lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        return dateFormatter
    }()
    
    func setDatePicker() {
        let customView:UIView = UIView (frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 160))
        customView.backgroundColor = UIColor.white
        
        datePicker = UIDatePicker(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 160))
        datePicker.datePickerMode = .date
        
        if (!(dateTextField.text ?? "").isEmpty) {
            datePicker.date = YGAppUtils.outputDateFormatter().date(from: dateTextField.text!)!
        } 
//        datePicker.maximumDate = Date()
        
        let currentCalendar = Calendar.current
        
        var dateComponents = DateComponents()
        dateComponents.year = -100
        
        let minimumDate = (currentCalendar as NSCalendar).date(byAdding: dateComponents, to: Date(), options: [])
        datePicker.minimumDate = minimumDate
        
        customView.addSubview(datePicker)
        dateTextField.inputView = customView
        let doneButton:UIButton = UIButton (frame: CGRect.init(x: 100, y: 0, width: 100, height: 44))
        doneButton.setTitle(String.localize("Done").capitalized, for: UIControlState.normal)
        doneButton.addTarget(self, action: #selector(self.doneButtonTap), for: UIControlEvents.touchUpInside)
        doneButton.setTitleColor(UIColor.white, for: UIControlState.normal)
        doneButton.backgroundColor = UIColor.customBlueColor()
        dateTextField.inputAccessoryView = doneButton
    }
    
    func doneButtonTap() {
        delegate?.datePickerChanged(date: datePicker.date as NSDate)
        
        let formatter = YGAppUtils.outputDateFormatter()
        dateTextField.text = ("\(formatter.string(from: datePicker.date))")
        dateTextField.resignFirstResponder()
        dateTextField.isUserInteractionEnabled = false

    }
    

}
