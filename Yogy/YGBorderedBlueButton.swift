//
//  YGBorderedBlueButton.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 15/08/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

class YGBorderedBlueButton: UIButton {

    private var border = CAShapeLayer()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.cornerRadius = self.frame.height/2.0
        
        self.borderColor = UIColor.customBlueColor()
        self.setTitleColor(UIColor.customBlueColor(), for: .selected)
    }
    
    override var isSelected: Bool {
        willSet {
            if (newValue) {
                self.backgroundColor = UIColor.clear
                
                border = CAShapeLayer()
                border.path = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.frame.width/2).cgPath
                border.fillColor = UIColor.clear.cgColor
                border.strokeColor = borderColor.cgColor
                border.lineWidth = 2;
                border.lineDashPattern = [4, 2]
                border.removeFromSuperlayer()
                self.layer.addSublayer(border)
            }
            else {
                self.backgroundColor = borderColor
                border.removeFromSuperlayer()
            }
        }
    }

}
