//
//  YGProfileTableViewCellProtocol.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 05/08/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGProfileTableViewCellProtocol {

    func updateInfoWithModel(model: YGProfileTableViewCellModel)
}
