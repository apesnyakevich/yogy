//
//  YGSubscriptionTableViewCellModel.swift
//  Yogy
//
//  Created by apesnyakevich on 04.09.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

struct YGSubscriptionTableViewCellModel
{
    var subscriptActive: Bool?
    var title: String?
    var cost: String?
    var subtitle: String?
    var notes: String?
}
