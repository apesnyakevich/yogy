//
//  YGAppUtils.swift
//  Yogy
//
//  Created by apesnyakevich on 08.08.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit
import Foundation

struct YGAppUtils {
    
    //MARK: - Personal numbers
    
    static func personalNumberYear(birthDate: NSDate, currentDate:NSDate) -> Int {
        
        let birthDay = Calendar.current.component(.day, from: birthDate as Date)
        let birthMonth = Calendar.current.component(.month, from: birthDate as Date)
        let currentYear = Calendar.current.component(.year, from: currentDate as Date)
        
        if birthDay > 0 && birthMonth > 0 && currentYear > 0 {
            return personalNumberOfYear(birthDay: birthDay, birthMonth: birthMonth, currentYear: currentYear)
        } else {
            return 0
        }
    }

    static func personalNumberMonth(birthDate: NSDate, currentDate:NSDate) -> Int {
        
        let birthDay = Calendar.current.component(.day, from: birthDate as Date)
        let birthMonth = Calendar.current.component(.month, from: birthDate as Date)
        let currentYear = Calendar.current.component(.year, from: currentDate as Date)
        let currentMonth = Calendar.current.component(.month, from: currentDate as Date)
        
        var personalNumberYear:Int = 0
        
        if birthDay > 0 && birthMonth > 0 && currentYear > 0 {
            personalNumberYear = personalNumberOfYear(birthDay: birthDay, birthMonth: birthMonth, currentYear: currentYear)
        }
        
        if personalNumberYear > 0 && currentMonth > 0 {
            return personalNumberOfMonth(personalNumberOfYear: personalNumberYear, currentMonth: currentMonth)
        } else {
            return 0
        }
    }

    static func personalNumberDay(birthDate: NSDate, currentDate:NSDate) -> Int {
        
        let birthDay = Calendar.current.component(.day, from: birthDate as Date)
        let birthMonth = Calendar.current.component(.month, from: birthDate as Date)
        let currentYear = Calendar.current.component(.year, from: currentDate as Date)
        let currentMonth = Calendar.current.component(.month, from: currentDate as Date)
        let currentDay = Calendar.current.component(.day, from: currentDate as Date)
        
        var personalNumberYear:Int = 0
        if birthDay > 0 && birthMonth > 0 && currentYear > 0 {
            personalNumberYear = personalNumberOfYear(birthDay: birthDay, birthMonth: birthMonth, currentYear: currentYear)
        }
        
        var personalNumberMonth:Int = 0
        if personalNumberYear > 0 && currentMonth > 0 {
            personalNumberMonth = personalNumberOfMonth(personalNumberOfYear: personalNumberYear, currentMonth: currentMonth)
        }
        
        if personalNumberMonth > 0 && currentDay > 0 {
            return personalNumberOfDay(personalNumberOfMonth: personalNumberMonth, currentDay: currentDay)
        } else {
            return 0
        }
    }
    
    //Сумма всех цифр в числе (например, число 839, сумма 8+3+9 = 20)
    static func sumOfNumberDigits(_ number: Int) -> Int {
        var sum:Int = 0
        var num:Int = number
        while num != 0 {
            sum = sum + (num % 10) //остаток от деления
            num = num / 10         //целочисленное деление
        }
        return sum
    }
    
    //Сумма всех цифр в числе (например, число 839, сумма 8+3+9 = 20, далее 2+0 = 2, итог 2)
    static func numberDigitsSum(_ number: Int) -> Int {
        var sum:Int = 0
        var num:Int = number
        repeat {
            sum = self.sumOfNumberDigits(num)
            num = sum
        } while sum > 9
        return sum
    }
    
    static func personalNumberOfYear(birthDay: Int, birthMonth:Int, currentYear:Int) -> Int {
        
        if birthDay > 0 && birthMonth > 0 && currentYear > 0 {
            
            var sumOfNumberDigits:Int = 0
            
            sumOfNumberDigits = self.numberDigitsSum(birthDay)
            sumOfNumberDigits = sumOfNumberDigits + self.numberDigitsSum(birthMonth)
            sumOfNumberDigits = sumOfNumberDigits + self.numberDigitsSum(currentYear)
            
            sumOfNumberDigits = self.numberDigitsSum(sumOfNumberDigits)
            
            return sumOfNumberDigits
        } else {
            return 0
        }
    }
    
    static func personalNumberOfMonth(personalNumberOfYear: Int, currentMonth:Int) -> Int {
        
        if personalNumberOfYear > 0 && currentMonth > 0 {
            
            var sumOfNumberDigits:Int = 0
            
            sumOfNumberDigits = personalNumberOfYear + currentMonth
            sumOfNumberDigits = self.numberDigitsSum(sumOfNumberDigits)
            
            return sumOfNumberDigits
        } else {
            return 0
        }
    }
    
    static func personalNumberOfDay(personalNumberOfMonth: Int, currentDay:Int) -> Int {
        
        if personalNumberOfMonth > 0 && currentDay > 0 {
            
            var sumOfNumberDigits:Int = 0
            
            sumOfNumberDigits = personalNumberOfMonth + currentDay
            sumOfNumberDigits = self.numberDigitsSum(sumOfNumberDigits)
            
            return sumOfNumberDigits
        } else {
            return 0
        }
    }
    
    // MARK: - Digital matrix
    
    //Количество разрядов в числе, например, для 1234 кол-во разрядов = 4
    // max n < 10 000 000 000
    //(есть и другие алгоритмы, но этот самый быстрый)
    static func getCountsOfDigits(_ n: Int) -> Int
    {
        if (n < 100000) {
            if (n < 100) {
                return (n < 10) ? 1 : 2
            } else {
                if (n < 1000) {
                    return 3
                } else {
                    return (n < 10000) ? 4 : 5
                }
            }
        } else {
            if (n < 10000000) {
                return (n < 1000000) ? 6 : 7
            } else {
                if (n < 100000000) {
                    return 8;
                } else {
                    return (n < 1000000000) ? 9 : 10
                }
            }
        }
    }
    
    static func sumOfNumberDigitsInDate(_ date: NSDate) -> Int {
        
        var sum:Int = 0
        let birthDay    = Calendar.current.component(.day, from: date as Date)
        let birthMonth  = Calendar.current.component(.month, from: date as Date)
        let birthYear   = Calendar.current.component(.year, from: date as Date)
        
        sum = sumOfNumberDigits(birthDay) + sumOfNumberDigits(birthMonth) + sumOfNumberDigits(birthYear)
        return sum
    }
    
    //Опорное число 1
    static func referenceNumber1 (_ birthDate: NSDate) -> Int {
        return sumOfNumberDigitsInDate(birthDate)
    }
    
    //Опорное число 2
    static func referenceNumber2 (_ birthDate: NSDate) -> Int {
        let referenceNumber: Int = referenceNumber1(birthDate)
        return sumOfNumberDigits(referenceNumber)
    }
    
    //Опорное число 3
    static func referenceNumber3 (_ birthDate: NSDate) -> Int {
        var n:Int = 0
        n = referenceNumber1(birthDate)
        
        let birthDay:Int = Calendar.current.component(.day, from: birthDate as Date)
        
        var firstDayNumber:Int = 0
        
        if(birthDay < 10) {
            firstDayNumber = birthDay
        } else {
            firstDayNumber = birthDay / 10
        }
        
        n = abs(n - firstDayNumber * 2)
        
        return n
    }
    
    //Опорное число 4
    static func referenceNumber4 (_ birthDate: NSDate) -> Int {
        var n:Int = 0
        n = referenceNumber3(birthDate)
        n = sumOfNumberDigits(n)
        return n
    }
    
    //Опорное число 5
    static func referenceNumber5 (_ birthDate: NSDate) -> Int {
        return referenceNumber1(birthDate) + referenceNumber3(birthDate)
    }
    
    //Опорное число 6
    static func referenceNumber6 (_ birthDate: NSDate) -> Int {
        return referenceNumber2(birthDate) + referenceNumber4(birthDate)
    }
    
    static func matrixArray (_ birthDate: NSDate) -> [Int] {
        
        var array = [0,0,0,0,0,0,0,0,0,0,0,0,0]     //count = 13
        
        let formatter = DateFormatter()
        formatter.dateFormat = "ddMMyyyy"
        var string:String = formatter.string(from: birthDate as Date)
        
        string = string.appending(String(referenceNumber1(birthDate)))
        string = string.appending(String(referenceNumber2(birthDate)))
        string = string.appending(String(referenceNumber3(birthDate)))
        string = string.appending(String(referenceNumber4(birthDate)))
        string = string.appending(String(referenceNumber5(birthDate)))
        string = string.appending(String(referenceNumber6(birthDate)))
        
        print(string)
        
        var index:Int = 0
        while index < 13 {
            let arr = string.indexes(of: String(index))
            if(arr != nil) {
                let arrCount = arr?.count
                if(arrCount! > 0) {
                    array[index] = arrCount!
                }
            }
            index += 1
        }
        return array
    }
    
    static func psychoMatrix(_ birthDate: NSDate) -> [Int] {
        
        var array = [0,0,0,0,0,0,0,0,0,0] //10 элементов
        var outArray = [0,0,0,0,0,0,0,0]  //8 элементов
        
        let formatter = DateFormatter()
        formatter.dateFormat = "ddMMyyyy"
        var string:String = formatter.string(from: birthDate as Date)
        
        string = string.appending(String(referenceNumber1(birthDate)))
        string = string.appending(String(referenceNumber2(birthDate)))
        string = string.appending(String(referenceNumber3(birthDate)))
        string = string.appending(String(referenceNumber4(birthDate)))
        
        //        print(string)
        
        var idx:Int = 0
        while idx < 10 {
            let arr = string.indexes(of: String(idx))
            if(arr != nil) {
                let arrCount = arr?.count
                if(arrCount! > 0) {
                    array[idx] = arrCount!
                }
            }
            idx += 1
        }
        
        //        print(array)
        
        //Удаляем элемент 0, т.к. в нем содержится кол-во вхождений подстроки "0"
        var arr2 = Array(array.dropFirst())
        
        //        print(arr2)
        
        outArray[0] = arr2[0] +  arr2[3] +  arr2[6]
        outArray[1] = arr2[1] +  arr2[4] +  arr2[7]
        outArray[2] = arr2[0] +  arr2[1] +  arr2[2]
        outArray[3] = arr2[3] +  arr2[4] +  arr2[5]
        outArray[4] = arr2[6] +  arr2[7] +  arr2[8]
        outArray[5] = arr2[2] +  arr2[5] +  arr2[8]
        outArray[6] = arr2[0] +  arr2[4] +  arr2[8]
        outArray[7] = arr2[2] +  arr2[4] +  arr2[6]
        
        return outArray
    }
    
    //MARK: - Date
    
    static func outputDateFormatter() -> DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM yyyy"

        return formatter
    }
    
    static func serverDateFormatter() -> DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }
        
    static func outputMonthYearDateFormatter() -> DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM yyyy"
        
        return formatter
    }
    
    static func dateFromDate(_ date: Date, _ newDay: Int) -> Date
    {
        let calendar = Calendar.current
        let month = calendar.component(.month, from: date)
        let year = calendar.component(.year, from: date)
        
        var components = DateComponents()
        components.day = newDay
        components.month = month
        components.year = year
        
        let dateOut = calendar.date(from: components)!
        
//        print(dateOut)
        
        return dateOut
    }
    
    //MARK: - User Defaults
    
    static func getImagePhotoForKey(_ key: String) -> UIImage {
        
        let defaults:UserDefaults = UserDefaults.standard
        var data:Data? = defaults.data(forKey: key)
        if (data == nil) {
            setImageDummyPhotoForKey(key)
            data = defaults.data(forKey: key)!
        }
        let image:UIImage = UIImage.init(data:data!)!
        return image
    }
    
    static func setImagePhotoForKey(_ key: String, image: UIImage) {
        
        let defaults:UserDefaults = UserDefaults.standard
        let data = UIImageJPEGRepresentation(image, 1.0)
        defaults.setValue(data, forKeyPath: key)
    }
    
    static func setImageDummyPhotoForKey(_ key: String) {
        let defaults:UserDefaults = UserDefaults.standard
        let image:UIImage = UIImage(named:"image_dummy_user")!
        let data = UIImageJPEGRepresentation(image, 1.0)
        defaults.setValue(data, forKeyPath: key)
    }
    
    static func setSubscriptionNumber(_ number: Int) {
        UserDefaults.standard.set(number, forKey: kSubscriptionNumberKey)
    }
    
    static func getSubscriptionNumber() -> Int {
        return UserDefaults.standard.integer(forKey: kSubscriptionNumberKey)
    }
    
    static func setUserEmail(_ email: String) {
        UserDefaults.standard.set(email, forKey: kUserEmailKey)
    }
    
    static func getUserEmail() -> String {
        let email:String = UserDefaults.standard.string(forKey: kUserEmailKey)!
        return  (!email.isEmpty) ? email : ""
    }
    
    //MARK: - File manager
    
    static func imageIsNull(_ image: UIImage)-> Bool {
        let size = CGSize(width: 0, height: 0)
        if (image.size.width == size.width) {
            return false
        } else {
            return true
        }
    }
    
    static func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
 
    static func saveImageToFile(_ fileName: String, image : UIImage) -> String {
        var filePath: String = .init()
        let fileManager = FileManager.default
//        let fileNameExt: String = "\(fileName).png"
        
        do {
            let documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
            let fileURL = documentsURL.appendingPathComponent(fileName)         //("\(fileName).png")
            if let pngImageData = UIImagePNGRepresentation(image) {
                try pngImageData.write(to: fileURL, options: .atomic)
                filePath = fileURL.path
                return fileName//filePath
            }
        } catch {
            print(error)
        }
        return filePath
    }

    static func fileExistAt(_ path: String) -> Bool {
        return FileManager.default.fileExists(atPath: path)
    }
    
    static func getDocumentsPath () -> String {
        var documentsPath: String = .init()
        let fileManager = FileManager.default
        let documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
        documentsPath = documentsURL.path
        return documentsPath
    }
    
    
    static func fileExistWith(_ name: String) -> Bool {
        var filePath: String = .init()
        let fileManager = FileManager.default
        let documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
        let fileURL = documentsURL.appendingPathComponent(name)
        filePath = fileURL.path
        return FileManager.default.fileExists(atPath: filePath)
    }
    
    static func getImagePhotoAt(_ path: String) -> UIImage {
        if FileManager.default.fileExists(atPath: path) {
            return UIImage(contentsOfFile: path)!
        }
        else {
            let image: UIImage = UIImage(named: kUserDummyPhoto)!
            return image
        }
    }
    
    static func getImagePhotoWith(_ name: String) -> UIImage {
        let url: URL = URL(fileURLWithPath: self.getDocumentsPath()).appendingPathComponent(name)
        let filePath: String = url.path
        let fileManager = FileManager.default
        
        if fileManager.fileExists(atPath: filePath) {
            return UIImage(contentsOfFile: filePath)!
        }
        else {
            let image: UIImage = UIImage(named: kUserDummyPhoto)!
            return image
        }
    }
    
    //MARK: - Validation

    static func validate(name: String) -> Bool {
        let matches = name.range(of: kYGValidationNameRegex, options: .regularExpression)
        if let _ = matches {
            return true
        }
        return false
    }
    
    static func validate(email: String) -> Bool {
        let matches = email.range(of: kYGValidationEmailRegex, options: .regularExpression)
        if let _ = matches {
            return true
        }
        return false
    }
    
    static func validate(password: String) -> Bool {
        let matches = password.range(of: kYGValidationPasswordRegex, options: .regularExpression)
        if let _ = matches {
            return true
        }
        return false
    }

}
