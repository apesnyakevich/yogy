//
//  YGFriendListTableViewCell.swift
//  Yogy
//
//  Created by Anna on 08.08.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit
import SwipeCellKit

class YGFriendListTableViewCell: SwipeTableViewCell {

    @IBOutlet private weak var photoImageView: UIImageView!
    @IBOutlet private weak var fullNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func updateWith(_ model: YGFriendModel) {
        self.photoImageView.layer.cornerRadius = self.photoImageView.height() / 2
        self.photoImageView.clipsToBounds = true
        if (!model.name!.isEmpty && !model.surname!.isEmpty) {
            fullNameLabel.text = "\(model.name!) \(model.surname!)"
        } else if (!model.name!.isEmpty && model.surname!.isEmpty) {
            fullNameLabel.text = "\(model.name!)"
        } else if (model.name!.isEmpty && !model.surname!.isEmpty) {
            fullNameLabel.text = "\(model.surname!)"
        } else if (model.name!.isEmpty && model.surname!.isEmpty) {
            fullNameLabel.text = "Unknown"
        }
        
        if (model.photoPath != nil) {
            photoImageView.image = YGAppUtils.getImagePhotoWith(model.photoPath!) //getImagePhotoForKey("\(fullNameLabel.text!)")
        }
    }

}
