//
//  YGSelectableTextFieldView.swift
//  Yogy
//
//  Created by apesnyakevich on 06.09.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGSelectableTextFieldViewDelegate: class {
    
    func setSelectionInfo(tag: Int, selectionString: String)
}

class YGSelectableTextFieldView: UIView {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var inputTextField: UITextField!
    
    weak var textFieldDelegate: UITextFieldDelegate?

    private var border = CAShapeLayer()
    private var datePicker:UIDatePicker!
    
    var selectionString: String = ""
    
    weak var delegate: YGSelectableTextFieldViewDelegate?
    
    var isValid:Bool {
        set {
            if(!newValue) {
                //No valid: border - dash red line
                setContentViewBorder(UIColor.customRedColor())
            }
            else if(inputTextField.text?.length == 0) {
                //Text is empty - dash blue line
                setContentViewBorder(UIColor.customBlueColor())
            } else {
                //Valid true: border - blue line
                contentView.cornerRadius = contentView.height()/2
                contentView.borderWidth = 1.0
                contentView.borderColor = UIColor.customBlueColor()
            }
        }
        get {
            return self.isValid
        }
    }

    //MARK: - View lifecycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed(String(describing: YGSelectableTextFieldView.self), owner: self, options: nil)
        
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        isValid = true
    }

    func setContentViewBorder(_ customColor: UIColor) {
        
        contentView.borderColor = UIColor.clear
        let layer = CAShapeLayer()
        layer.path = UIBezierPath(roundedRect: contentView.bounds, cornerRadius: contentView.frame.width/2).cgPath
        layer.fillColor = UIColor.clear.cgColor
        layer.strokeColor = customColor.cgColor
        layer.lineWidth = 1;
        layer.lineDashPattern = [4, 2]
        contentView.layer.addSublayer(layer)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setDatePicker() {
        let customView:UIView = UIView (frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 160))
        customView.backgroundColor = UIColor.white
        
        datePicker = UIDatePicker(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 160))
        datePicker.datePickerMode = .date
        
        if (!selectionString.isEmpty) {
            datePicker.date = YGAppUtils.outputDateFormatter().date(from: selectionString)!
        }
        
        let currentCalendar = Calendar.current
        
        var dateComponents = DateComponents()
        dateComponents.year = -100
        
        let minimumDate = (currentCalendar as NSCalendar).date(byAdding: dateComponents, to: Date(), options: [])
        datePicker.minimumDate = minimumDate
        
        customView.addSubview(datePicker)
        inputTextField.inputView = customView
        let doneButton:UIButton = UIButton (frame: CGRect.init(x: 100, y: 0, width: 100, height: 44))
        doneButton.setTitle(String.localize("Done").capitalized, for: UIControlState.normal)
        doneButton.addTarget(self, action: #selector(self.doneButtonTap), for: UIControlEvents.touchUpInside)
        doneButton.setTitleColor(UIColor.white, for: UIControlState.normal)
        doneButton.backgroundColor = UIColor.customBlueColor()
        inputTextField.inputAccessoryView = doneButton
    }
    
    func doneButtonTap() {
        selectionString = YGAppUtils.serverDateFormatter().string(from: datePicker.date)
        
        inputTextField.text = YGAppUtils.outputDateFormatter().string(from: datePicker.date)
        inputTextField.resignFirstResponder()
        delegate?.setSelectionInfo(tag: inputTextField.tag, selectionString: selectionString)
    }
}

// MARK: - UITextFieldDelegate

extension YGSelectableTextFieldView: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if (textField.tag != YGRegistrationTag.YGRegistrationTagDateBirth.rawValue) {
            textField.becomeFirstResponder()
        } else {
            setDatePicker()
        }
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        selectionString = textField.text!
        delegate?.setSelectionInfo(tag: textField.tag, selectionString: selectionString)
        textField.resignFirstResponder()
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

