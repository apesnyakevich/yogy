//
//  YGLoginViewController.swift
//  Yogy
//
//  Created by apesnyakevich on 05.09.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit
import Alamofire

class YGLoginViewController: UIViewController {

    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var notesLabel: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var emailTextField: YGSelectableTextFieldView!
    @IBOutlet weak var passwordTextField: YGSelectableTextFieldView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var modelLogin = YGUserLoginModel()
    
    //MARK: - View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setModelLogin()
        
        loginButton.cornerRadius = loginButton.height()/2
        notesLabel.text = ""
        notesLabel.textColor = UIColor.customRedColor()
        
        setUIControls()
    }
    
    func setModelLogin() {
        modelLogin.email = YGAppUtils.getUserEmail()
        modelLogin.password = ""
        if(isNetworkDebug) {
            modelLogin.email = "medvedko3@gmail.com"
            modelLogin.password = "Abvgde!1"
            passwordTextField.inputTextField.text = modelLogin.password
        }
    }
    
    func setUIControls() {
        
        loginLabel.text = String.localize("Login")
        loginButton.setTitle(String.localize("Enter"), for: UIControlState.normal)
        
        emailTextField.inputTextField.placeholder = String.localize("E-mail")
        passwordTextField.inputTextField.placeholder = String.localize("Password")

        emailTextField.inputTextField.tag = YGLoginTag.YGLoginTagEmail.rawValue
        passwordTextField.inputTextField.tag = YGLoginTag.YGLoginTagPassword.rawValue
        
        emailTextField.inputTextField.keyboardType = UIKeyboardType.emailAddress
        passwordTextField.inputTextField.isSecureTextEntry = true
        
        emailTextField.delegate = self
        passwordTextField.delegate = self
        
        emailTextField.inputTextField.text = modelLogin.email
    }
    
    @IBAction func loginButtonTouchUpInside(_ sender: UIButton) {
        
        if(!YGAppUtils.validate(email: emailTextField.inputTextField.text!)) {
            notesLabel.text = String.localize("error_message_enter_valid_email")
        }
        else if(!YGAppUtils.validate(password: passwordTextField.inputTextField.text!)) {
            notesLabel.text = String.localize("error_message_password_must_contain_digits")
        }
        else {
            activityIndicator.startAnimating()
            notesLabel.text = ""
            print("validate OK")
            loginRequest()
        }
    }
    
    // MARK: - Alamofire Network

    func loginRequest() {
        
        var parameters: Parameters = [:]
            parameters = [
                kYGNetworkEmailKey: modelLogin.email!,
                kYGNetworkPasswordKey: modelLogin.password! ]
        
        Alamofire.request(kUserLoginUrlString,
                          method: .post,
                          parameters: parameters,
                          encoding: URLEncoding.default).responseJSON
            { response in
                
                self.activityIndicator.stopAnimating()
                
                if(isNetworkDebug) {
                    print("Request: \(String(describing: response.request))")   // original url request
                    print("Response: \(String(describing: response.response))") // http url response
                    print("Result: \(response.result)")                         // response serialization result
                                
                    if let json = response.result.value {
                        print("JSON: \(json)") // serialized json response
                    }
                }
                
                if let value = response.result.value as? [String:Any] {
                    
                    let keyExists = value[kYGNetworkErrorDescriptionKey] != nil
                    if(keyExists) {
                        let errorDescrition = value[kYGNetworkErrorDescriptionKey] as! String
                        print(errorDescrition)
                        self.notesLabel.text = errorDescrition
                    } else {
                        YGAppUtils.setUserEmail(self.modelLogin.email!)
                        print("Login Ok!")
                    }
                }
            }
    }
}

// MARK: - YGSelectableTextFieldViewDelegate

extension YGLoginViewController: YGSelectableTextFieldViewDelegate {
    
    func setSelectionInfo(tag: Int, selectionString: String) {
        
        switch tag {
        case YGLoginTag.YGLoginTagEmail.rawValue:
            if(YGAppUtils.validate(email: selectionString)) {
                notesLabel.text = ""
                modelLogin.email = selectionString
                emailTextField.isValid = true
            } else {
                notesLabel.text = String.localize("error_message_enter_valid_email")
                emailTextField.isValid = false
            }
            break
        case YGLoginTag.YGLoginTagPassword.rawValue:
                notesLabel.text = ""
                modelLogin.password = selectionString
                passwordTextField.isValid = true
            break
        default:
            break
        }
    }
}
