//
//  SwinjectStoryboard+Setup.swift
//  SwinjectSimpleExample
//
//  Created by Yoichi Tagaya on 11/20/15.
//  Copyright © 2015 Swinject Contributors. All rights reserved.
//

import SwinjectStoryboard

extension SwinjectStoryboard {
    class func setup() {
        defaultContainer.storyboardInitCompleted(YGProfileViewController.self) { r, c in
            c.dataProvider = r.resolve(YGDataProviderProtocol.self)
        }
        
        defaultContainer.storyboardInitCompleted(YGMainViewController.self) { r, c in
            c.dataProvider = r.resolve(YGDataProviderProtocol.self)
        }
        
        defaultContainer.storyboardInitCompleted(YGFriendListViewController.self) { r, c in
            c.dataProvider = r.resolve(YGDataProviderProtocol.self)
        }
        
        defaultContainer.storyboardInitCompleted(YGFriendEditViewController.self) { r, c in
            c.dataProvider = r.resolve(YGDataProviderProtocol.self)
        }

        defaultContainer.storyboardInitCompleted(YGThemeDayViewController.self) { r, c in
            c.dataProvider = r.resolve(YGDataProviderProtocol.self)
        }
        
        defaultContainer.storyboardInitCompleted(YGOrganizerViewController.self) { r, c in
            c.dataProvider = r.resolve(YGDataProviderProtocol.self)
        }
        
        defaultContainer.storyboardInitCompleted(YGAwarenessViewController.self) { r, c in
            c.dataProvider = r.resolve(YGDataProviderProtocol.self)
        }
        
        defaultContainer.storyboardInitCompleted(YGCalendarViewController.self) { r, c in
            c.dataProvider = r.resolve(YGDataProviderProtocol.self)
        }
        
        defaultContainer.storyboardInitCompleted(YGPotentialViewController.self) { r, c in
            c.dataProvider = r.resolve(YGDataProviderProtocol.self)
        }
        
        defaultContainer.storyboardInitCompleted(YGDestinyViewController.self) { r, c in
            c.dataProvider = r.resolve(YGDataProviderProtocol.self)
        }
        
        defaultContainer.storyboardInitCompleted(YGPsychomatrixViewController.self) { r, c in
            c.dataProvider = r.resolve(YGDataProviderProtocol.self)
        }
        
        defaultContainer.register(YGDataProviderProtocol.self) { _ in
            YGDataProvider() }

    }
}
