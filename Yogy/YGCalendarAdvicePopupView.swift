//
//  YGCalendarAdvicePopupView.swift
//  Yogy
//
//  Created by Anna on 09.09.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

class YGCalendarAdvicePopupView: UIView {

    @IBOutlet fileprivate var contentView: UIView!
    @IBOutlet weak var adviceLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialsSetting()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initialsSetting()
    }
    
    private func initialsSetting() {
        Bundle.main.loadNibNamed(String(describing: YGCalendarAdvicePopupView.self), owner: self, options: nil)
        self.addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        adviceLabel.transform = CGAffineTransform.init(scaleX: 0.01, y: 0.01)
    }
    
    
    func loadAdviсe(_ text: String) {
        adviceLabel.text = text
    }
    
    func runAnimation() {
        UIView.animate(withDuration: 0.6 / 1.5 , animations: {
            self.adviceLabel.transform = CGAffineTransform.identity.scaledBy(x: 1.1, y: 1.1)
        }) { (complition) in
            UIView.animate(withDuration: 0.6 / 2, animations: {
                self.adviceLabel.transform = CGAffineTransform.identity.scaledBy(x: 0.9, y: 0.9)
            }) { (complition) in
                UIView.animate(withDuration: 0.6 / 2, animations: {
                    self.adviceLabel.transform = CGAffineTransform.identity
                })
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        UIView.animate(withDuration: 0.3/1.5, animations: {
            self.adviceLabel.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
        }) { (complition) in
            self.removeFromSuperview()
        }
    }
    
    

}
