//
//  YGSubscriptionTableViewCellProtocol.swift
//  Yogy
//
//  Created by apesnyakevich on 04.09.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGSubscriptionTableViewCellProtocol {
    
    func updateInfoWithModel(model: YGSubscriptionTableViewCellModel)
}
