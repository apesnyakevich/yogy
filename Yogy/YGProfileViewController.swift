//
//  YGProfileViewController.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 31/07/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGProfileViewControllerDelegate: class {

}

class YGProfileViewController: UIViewController, YGProfileViewControllerDelegate {

    @IBOutlet weak var rightBarButton: UIBarButtonItem!
    @IBOutlet weak var photoButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var surnameLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var photoImageView: UIImageView!
    
    fileprivate var imagePicker = YGImagePicker()
    
    fileprivate var presenter: YGProfilePresenterDelegate?
    var dataProvider: YGDataProviderProtocol?
    
    //MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = String.localize("Profile")
        
//        self.addRightBarButtonWithImage(UIImage(named:"image_arrow_right")!)
        
        updatePresenter()
        
        imagePicker.delegate = self
        
        tableView.tableFooterView = UIView()
        tableView.estimatedRowHeight = 60
        tableView.rowHeight = UITableViewAutomaticDimension
        
        self.hideKeyboardWhenTappedAround()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        nameLabel.text = presenter?.userName()
        surnameLabel.text = presenter?.userSurname()
        
        photoImageView.layer.cornerRadius = photoImageView.bounds.size.width / 2
        photoImageView.clipsToBounds = true
        
        updatePhotoView()
    }
    
    //MARK: - Actions
    
    @IBAction func rightBarButtonTapped(_ sender: Any) {
        self.toggleLeft()
    }
    
    @IBAction func photoButtonTapped(_ sender: Any) {
        showImagePicker(sender)
    }
    
    
    //MARK: - Private
    
    fileprivate func updatePresenter() {
        let model = YGProfileModel()
        let presenter = YGProfilePresenter.init(view:self, model:model, dataProvider: dataProvider)
        self.presenter = presenter
    }
    
    fileprivate func updatePhotoView(){
        photoImageView.image = presenter?.userPhoto()
    }
    
    fileprivate func showImagePicker(_ sender: Any) {
        view.endEditing(true)
        
        // Create An UIAlertController with Action Sheet
        
        let optionMenuController = UIAlertController(title: nil, message: String.localize("Choose photo source"), preferredStyle: .actionSheet)
        
        // Create UIAlertAction for UIAlertController
        let addAction = UIAlertAction(title: String.localize("Camera"), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.imagePicker.cameraAccessRequest()
            }
        })
        let saveAction = UIAlertAction(title: String.localize("Gallery"), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.requestPermissionAndPresentPicker(sender)
        })
        
        let cancelAction = UIAlertAction(title: String.localize("Cancel"), style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        // Add UIAlertAction in UIAlertController
        optionMenuController.addAction(addAction)
        optionMenuController.addAction(saveAction)
        optionMenuController.addAction(cancelAction)
        
        // Present UIAlertController with Action Sheet
        self.present(optionMenuController, animated: true, completion: nil)
    }
    
    fileprivate func presentImagePicker(sourceType: UIImagePickerControllerSourceType) {
        imagePicker.controller.sourceType = sourceType
        DispatchQueue.main.async {
            self.present(self.imagePicker.controller, animated: true, completion: nil)
        }
    }
    
    fileprivate func requestPermissionAndPresentPicker(_ sender: Any) {
        imagePicker.galleryAccessRequest()
    }
}

//MARK: - YGImagePickerDelegate

extension YGProfileViewController: YGImagePickerDelegate {
  
    func imagePickerDelegate(didSelect image: UIImage, imageName:String, delegatedForm: YGImagePicker) {
        
        let path: String = YGAppUtils.saveImageToFile(kUserPhotoName, image: image)

        presenter?.resetUserPhotoPath(path)

        updatePhotoView()

        imagePicker.dismiss()
    }
    
    func imagePickerDelegate(didCancel delegatedForm: YGImagePicker) {
        imagePicker.dismiss()
    }
    
    func imagePickerDelegate(canUseGallery accessIsAllowed: Bool, delegatedForm: YGImagePicker) {
        if accessIsAllowed {
            presentImagePicker(sourceType: .photoLibrary)
        }
    }
    
    func imagePickerDelegate(canUseCamera accessIsAllowed: Bool, delegatedForm: YGImagePicker) {
        if accessIsAllowed {
            presentImagePicker(sourceType: .camera)
        }
    }
}

//MARK: - UITableViewDataSource & UITableViewDelegate

extension YGProfileViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.5
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame: CGRect(x:0, y:0, width: tableView.frame.width, height: 0.5))
        headerView.backgroundColor = tableView.separatorColor
        
        return headerView
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter!.rowCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:presenter!.cellIdentifierAt(indexPath), for: indexPath)
        
        (cell as! YGProfileTableViewCellProtocol).updateInfoWithModel(model: presenter!.cellModelAt(indexPath))
        
        if (indexPath.row == YGProfileCellType.YGProfileCellTypeSignOut.rawValue) {
            cell.accessoryType = .none
        }
        else if(indexPath.row == YGProfileCellType.YGProfileCellTypeBirthdate.rawValue) {
            (cell as! YGProfileDateTableViewCell).delegate = self
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let cell = tableView.cellForRow(at: indexPath)
        cell?.layoutIfNeeded()
//        presenter?.cellSelectedAt(indexPath)
        
        if(indexPath.row == YGProfileCellType.YGProfileCellTypeSubscription.rawValue) {
            self.performSegue(withIdentifier: kYGSubscriptionVCSegue, sender: self)
        }
    }
}

//MARK: - YGProfileDateTableViewCellDelegate

extension YGProfileViewController: YGProfileDateTableViewCellDelegate {
    
    func datePickerChanged(date:NSDate) {
        presenter?.resetBirthdate(date)
        tableView.reloadData()
    }
}
