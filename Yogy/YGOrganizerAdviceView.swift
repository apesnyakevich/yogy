//
//  YGOrganizerAdviceView.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 10/08/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

class YGOrganizerAdviceView: UIView {

    @IBOutlet fileprivate weak var backgroundView: UIView!
    @IBOutlet fileprivate weak var textLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundView.cornerRadius = 6.0
    }
    
    func updateAdviceInfo(text: String?, hexColor: String?) {
        
        textLabel.text = text
        
        if (!(hexColor ?? "").isEmpty) {
            backgroundView.backgroundColor = UIColor(hex: hexColor!)
        }
        
        animateView()
    }
    
    private func animateView() {
        backgroundView.alpha = 0
        
        var topConstraint: NSLayoutConstraint?
        
        for constraint in self.constraints {
            if constraint.identifier == "topConstraint" {
                topConstraint  = constraint
                topConstraint?.constant += 30
                self.layoutIfNeeded()
            }
        }
        
        UIView.animate(withDuration: 1.0,
                       delay: 0.0,
                       options: .curveEaseInOut,
                       animations: {
                        self.backgroundView.alpha = 1.0
                        topConstraint?.constant -= 30
                        self.layoutIfNeeded()
        })
    }
}
