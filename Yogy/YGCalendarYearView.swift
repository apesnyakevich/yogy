//
//  YGCalendarYearView.swift
//  Yogy
//
//  Created by apesnyakevich on 16.08.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGCalendarYearViewDelegate: class {
    func yearChanged(_ year:Int)
}

class YGCalendarYearView: UIView {

    var yearButtons:[UIButton]!
    var selectedYear: String!
    weak var delegate: YGCalendarYearViewDelegate?
    
    @IBOutlet fileprivate var contentView: UIView!
    @IBOutlet private weak var yearButton0: UIButton!
    @IBOutlet private weak var yearButton1: UIButton!
    @IBOutlet private weak var yearButton2: UIButton!
    @IBOutlet private weak var yearButton3: UIButton!
    @IBOutlet private weak var yearButton4: UIButton!
    @IBOutlet private weak var yearButton5: UIButton!
    
    //MARK: - View lifecycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed(String(describing: YGCalendarYearView.self), owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        yearButtons = [yearButton0!, yearButton1!, yearButton2!, yearButton3!, yearButton4!, yearButton5!]
        
        self.adjustAllButtons()
    }
    
    private func adjustAllButtons() {
        yearButtons[0].isSelected = true
        
        let dateNow: Date = Date()
        let yearCurrent: Int = Calendar.current.component(.year, from: dateNow)
        
        for (index, button) in yearButtons.enumerated() {
            button.cornerRadius = button.frame.height/2.0
            button.setBackgroundImage(UIImage(color:UIColor.customBlueColor(), size:button.size()), for: .selected)
           
            let year: Int = yearCurrent + index
            let yearName: String = String(year)
            button.setTitle(yearName, for: .normal)
        }
        
        selectedYear = yearButtons?[0].titleLabel?.text
        
        for button:UIButton in yearButtons! {
            button.cornerRadius = button.frame.height/2.0
            button.setBackgroundImage(UIImage(color:UIColor.customBlueColor(), size:button.size()), for: .selected)
        }
    }
        
    @IBAction func didButtonTapped(_ sender: UIButton) {
        
        for button in yearButtons! {
            if (button == sender) {
                button.isSelected = true
                selectedYear = sender.titleLabel?.text
                delegate?.yearChanged(Int(selectedYear)!)
                
            } else {
                button.isSelected = false
            }
        }
    }
}
