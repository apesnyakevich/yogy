//
//  YGFriendListModel.swift
//  Yogy
//
//  Created by Anna on 07.08.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

class YGFriendListModel: NSObject {
    var friendList = [YGFriendModel]()
    
    var friendModelForEditing: YGFriendModel?
    
    var testFriendList = [String]()
    
    var currentFriend: YGFriendModel?
}
