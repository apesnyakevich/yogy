//
//  YGSubscriptionPresenter.swift
//  Yogy
//
//  Created by apesnyakevich on 04.09.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGSubscriptionPresenterDelegate {
    
    func cellSelectedAt(_ indexPath: IndexPath)
    func rowCount() -> Int
    func cellIdentifierAt(_ indexPath: IndexPath) -> String
    func cellModelAt(_ indexPath: IndexPath) -> YGSubscriptionTableViewCellModel
}

class YGSubscriptionPresenter: YGSubscriptionPresenterDelegate {
    
    private unowned let view: YGSubscriptionViewControllerDelegate
    private var model: YGSubscriptionModel
    private var dataProvider: YGDataProviderProtocol?
    
    required init(view: YGSubscriptionViewControllerDelegate, model: YGSubscriptionModel, dataProvider: YGDataProviderProtocol?) {
        self.view = view
        self.model = model
        self.dataProvider = dataProvider
    }
    
    
    //MARK: - YGSubscriptionPresenterDelegate
    
    func rowCount() -> Int {
        return YGSubscriptionCellType.YGSubscriptionCellTypeCount.rawValue
    }
    
    func cellSelectedAt(_ indexPath: IndexPath) {
        
    }
    
    func cellIdentifierAt(_ indexPath: IndexPath) -> String {
        
        var identifier = String()
        
        switch indexPath.row {
            
        case YGSubscriptionCellType.YGSubscriptionCellTypeFree.rawValue,
             YGSubscriptionCellType.YGSubscriptionCellTypeStandard.rawValue,
             YGSubscriptionCellType.YGSubscriptionCellTypePremium.rawValue,
             YGSubscriptionCellType.YGSubscriptionCellTypeFull.rawValue:
            
            identifier = String(describing: YGSubscriptionTableViewCell.self)
            break
        default:
            break
        }
        
        return identifier
    }
    
    func cellModelAt(_ indexPath: IndexPath) -> YGSubscriptionTableViewCellModel {
        var model = YGSubscriptionTableViewCellModel()
        
        let subscriptionNumber = YGAppUtils.getSubscriptionNumber()
        if(subscriptionNumber == indexPath.row) {
            model.subscriptActive = true
        } else {
            model.subscriptActive = false
        }
        
        switch indexPath.row {
        case YGSubscriptionCellType.YGSubscriptionCellTypeFree.rawValue:
            model.title = String.localize("Free")
            model.subtitle = String.localize("message_free_subscription")
            model.cost = ("$0.00")
            model.notes = String.localize("")
            break
        case YGSubscriptionCellType.YGSubscriptionCellTypeStandard.rawValue:
            model.title = String.localize("Standard")
            model.subtitle = String.localize("message_standard_subscription")
            model.cost = ("$0.99")
            model.notes = String.localize("message_standard_subscription_note")
            break
        case YGSubscriptionCellType.YGSubscriptionCellTypePremium.rawValue:
            model.title = String.localize("Premium")
            model.subtitle = String.localize("message_premium_subscription")
            model.cost = ("$1.99")
            model.notes = String.localize("message_premium_subscription_note")
            break
        case YGSubscriptionCellType.YGSubscriptionCellTypeFull.rawValue:
            model.title = String.localize("Full")
            model.subtitle = String.localize("message_full_subscription")
            model.cost = ("$2.99")
            model.notes = String.localize("message_full_subscription_note")
            break
        default:
            break
        }
        
        return model
    }
}

