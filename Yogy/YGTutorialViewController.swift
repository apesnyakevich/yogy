//
//  YGTutorialViewController.swift
//  Yogy
//
//  Created by Anna on 04.09.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGTutorialViewControllerDelegate: class {

}

class YGTutorialViewController: UIViewController {

    @IBOutlet fileprivate weak var scrollView: UIScrollView!
    @IBOutlet fileprivate weak var infoLabel: UILabel!
    @IBOutlet fileprivate weak var pageControl: YGPageControl!
    @IBOutlet fileprivate weak var skipButton: UIButton!
    
    fileprivate var presenter: YGTutorialPresenterDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupPresenter()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateTutorialText()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        pageControl.currentPage = 0
        pageControl.updateDots()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func setupPresenter() {
        let model = YGTutorialModel()
        let presenter = YGTutorialPresenter.init(view: self, model: model)
        
        self.presenter = presenter
    }
  
    @IBAction func skip(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

//MARK: - YGTutorialViewControllerDelegate

extension YGTutorialViewController: YGTutorialViewControllerDelegate {
    
    func updateTutorialText() {
        DispatchQueue.main.async {
            let currentIndex = self.pageControl.currentPage
            
            self.skipButton.setTitle(self.presenter?.buttonTitleAt(currentIndex), for: .normal)
            self.infoLabel.text = self.presenter?.tutorialTextAt(currentIndex)
            
            UIView.animate(withDuration: 0.5) {
                self.infoLabel.alpha = 1
            }
        }
    }
}

//MARK: - Scroll View Delegate
extension YGTutorialViewController: UIScrollViewDelegate {
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        UIView.animate(withDuration: 0.5) {
            self.infoLabel.alpha = 0
        }
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let pageWidth = (scrollView.contentSize.width / 3)
        
        let newPageIndex = Int(scrollView.contentOffset.x / pageWidth)
        
        if (pageControl.currentPage != newPageIndex) {
            pageControl.currentPage = newPageIndex
            pageControl.updateDots()
        }
        updateTutorialText()
    }
}

//MARK: - Page Control
extension YGTutorialViewController {
    
    @IBAction func pageControlChangedValue(_ sender: UIPageControl) {
        
        pageControl.updateDots()
        
        let pageWidth = (scrollView.contentSize.width / 3)
        
        scrollView.setContentOffset(CGPoint.init(x: (pageWidth * CGFloat(sender.currentPage)), y: 0), animated: true)
        
        UIView.animate(withDuration: 0.5) {
            self.infoLabel.alpha = 0
        }
        updateTutorialText()
    }
  
}
