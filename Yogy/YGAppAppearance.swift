//
//  YGAppAppearance.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 01/08/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import SlideMenuControllerSwift

struct YGAppAppearance {
    
    static func performStartupConfiguration() {
        UINavigationBar.appearance().tintColor = UIColor(hex:"999DA8")
        
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().isTranslucent = true

        SlideMenuOptions.leftViewWidth = UIScreen.main.bounds.width
        SlideMenuOptions.rightViewWidth = UIScreen.main.bounds.width
        SlideMenuOptions.contentViewDrag = true
        SlideMenuOptions.hideStatusBar = false
    }
}
