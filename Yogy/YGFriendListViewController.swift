//
//  YGFriendListViewController.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 31/07/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit
import SwipeCellKit

protocol YGFriendListViewControllerDelegate: class {
    func updateInfo()
}

class YGFriendListViewController: UIViewController {
    
    var dataProvider: YGDataProviderProtocol?
    fileprivate var presenter: YGFriendListPresenterDelegate?

    @IBOutlet fileprivate var noSubscribeView: YGNoSubscribeView!
    @IBOutlet fileprivate weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate weak var friendListTableView: UITableView!
    @IBOutlet fileprivate weak var labelAndButtonDistanceConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var newFriendButton: UIButton!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updatePresenter()
        navigationController?.navigationBar.isHidden = true
    }

    override func viewWillAppear(_ animated: Bool) {

        self.removeSlideMenuGestures()

        presenter?.loadFriendList()
        let currentUser = dataProvider?.currentUser()
        if (currentUser!.subscriptionEnabled) {
             updateTableViewHeight()
        } else {
            noSubscribeView.isHidden = false
            noSubscribeView.delegate = self
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.addSlideMenuGestures()
    }
    
    private func updatePresenter() {
        let model = YGFriendListModel()
        let presenter = YGFriendListPresenter(view: self, model: model, dataProvider: dataProvider)
        self.presenter = presenter
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.destination.isKind(of: YGFriendEditViewController.self)) {
            (segue.destination as! YGFriendEditViewController).friendModel(presenter!.modelForEditing())
        } else if (segue.destination.isKind(of: YGCalendarViewController.self)) {
            (segue.destination as! YGCalendarViewController).setFriendForCalculations(presenter!.currentFriend())
        }
    }
    
    //MARK: - Actions
    
    @IBAction override func backButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: -  Private
    
    fileprivate func updateTableViewHeight() {
        tableViewHeightConstraint.constant = 0
        
        if (friendListTableView.numberOfRows(inSection: 0) > 0 ) {
            for _ in 0...(friendListTableView.numberOfRows(inSection: 0)-1) {
                tableViewHeightConstraint.constant += friendListTableView.rowHeight
            }
        }
        
        if (view.height() - (tableViewHeightConstraint.constant + infoLabel.frame.size.height + friendListTableView.rowHeight + newFriendButton.frame.size.height) < friendListTableView.rowHeight) {
            friendListTableView.isScrollEnabled = true
        } else {
            friendListTableView.isScrollEnabled = false
        }
        
        self.view.layoutIfNeeded()
    }
    
}

    //MARK: - YGFriendListViewControllerDelegate

extension YGFriendListViewController: YGFriendListViewControllerDelegate {
    
    func updateInfo() {
        weak var weakSelf = self
        DispatchQueue.main.async {
            weakSelf?.friendListTableView.reloadData()
            weakSelf?.updateTableViewHeight()
        }
    }
}

    //MARK: - UITableViewDelegate & UITableViewDataSource

extension YGFriendListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter!.friendsCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String.className(YGFriendListTableViewCell.self), for: indexPath)
        
        if (presenter?.friendBy(indexPath.row) != nil) {
            (cell as! YGFriendListTableViewCell).updateWith(presenter!.friendBy(indexPath.row)!)
            (cell as! YGFriendListTableViewCell).delegate = self
        }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        presenter?.selectFriendBy(indexPath)
        performSegue(withIdentifier: kCalendarViewControllerSegue, sender: nil)
    }
}

    //MARK: - YGNoSubscribeViewDelegate

extension YGFriendListViewController: YGNoSubscribeViewDelegate {
    
    func subscribeButtonDidPressed() {
        noSubscribeView.isHidden = true
    }
}

    //MARK: - SwipeTableViewCellDelegate

extension YGFriendListViewController: SwipeTableViewCellDelegate {
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        let deleteAction = SwipeAction(style: .destructive, title: "Delete") { action, indexPath in
            self.presenter?.removeFriendBy(indexPath.row)
        }
        
        // customize the action appearance
        deleteAction.backgroundColor = UIColor.customRedColor()
        
        let editAction = SwipeAction(style: .default, title: "Edit") { action, indexPath in
            self.presenter?.editFriendBy(indexPath)
            self.performSegue(withIdentifier: kYGFriendEditVCSegue, sender: self)
        }
        
        return [deleteAction, editAction]
    }
    
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeTableOptions {
        var options = SwipeTableOptions()
        
        options.transitionStyle = .border
        return options
    }
}

