//
//  YGThemeDayCalendarView.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 19/08/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGThemeDayCalendarViewDelegate: class {
    func calendarDateChanged(newDate: Date)
}


class YGThemeDayCalendarView: UIView, YGCalendarMonthViewDelegate, YGCalendarYearViewDelegate {
    
    @IBOutlet fileprivate weak var calendarDayView: UIView!
    @IBOutlet fileprivate weak var calendarMonthView: YGCalendarMonthView!
    @IBOutlet fileprivate weak var calendarYearView: YGCalendarYearView!
    
    weak var delegate: YGThemeDayCalendarViewDelegate?
    
    var calendarMode :Int? {
        willSet {
            switch newValue! {
            case YGThemeDayCalendarMode.Day.rawValue:
                calendarDayView.isHidden = false
                calendarYearView.isHidden = true
                calendarMonthView.isHidden = true
                break
            case YGThemeDayCalendarMode.Month.rawValue:
                calendarDayView.isHidden = true
                calendarYearView.isHidden = true
                calendarMonthView.isHidden = false
                break
            case YGThemeDayCalendarMode.Year.rawValue:
                calendarDayView.isHidden = true
                calendarYearView.isHidden = false
                calendarMonthView.isHidden = true
                break
            default:
                break
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        calendarMonthView.delegate = self
        calendarYearView.delegate = self
    }
    
    func monthChanged(_ month:Int) {
        
        let calendar = Calendar.current
        let day = calendar.component(.day, from: Date())
        let year = calendar.component(.year, from: Date())
        
        var components = DateComponents()
        components.day = day
        components.month = month
        components.year = year
        
        let dateOut = calendar.date(from: components)!
        delegate?.calendarDateChanged(newDate: dateOut)
        
    }
    
    func yearChanged(_ year: Int) {
        
        let calendar = Calendar.current
        let day = calendar.component(.day, from: Date())
        let month = calendar.component(.month, from: Date())
        
        var components = DateComponents()
        components.day = day
        components.month = month
        components.year = year
        
        let dateOut = calendar.date(from: components)!
        delegate?.calendarDateChanged(newDate: dateOut)
    }
}
