//
//  YGImagePickerDelegate.swift
//  Yogy
//
//  Created by apesnyakevich on 09.08.17.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import UIKit

protocol YGImagePickerDelegate {
    func imagePickerDelegate(canUseCamera accessIsAllowed:Bool, delegatedForm: YGImagePicker)
    func imagePickerDelegate(canUseGallery accessIsAllowed:Bool, delegatedForm: YGImagePicker)
    func imagePickerDelegate(didSelect image: UIImage, imageName:String, delegatedForm: YGImagePicker)
    func imagePickerDelegate(didCancel delegatedForm: YGImagePicker)
}

extension YGImagePickerDelegate {
    func imagePickerDelegate(canUseCamera accessIsAllowed:Bool, delegatedForm: YGImagePicker) {}
    func imagePickerDelegate(canUseGallery accessIsAllowed:Bool, delegatedForm: YGImagePicker) {}
}
