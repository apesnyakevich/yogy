//
//  YGTaskModel+CoreDataProperties.swift
//  Yogy
//
//  Created by Andrew Pesnyakevich on 04/08/2017.
//  Copyright © 2017 Andrew Pesnyakevich. All rights reserved.
//

import Foundation
import CoreData


extension YGTaskModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<YGTaskModel> {
        return NSFetchRequest<YGTaskModel>(entityName: "YGTaskModel")
    }

    @NSManaged public var completionDate: NSDate?
    @NSManaged public var creationDate: NSDate?
    @NSManaged public var completed: Bool
    @NSManaged public var text: String?
    @NSManaged public var title: String?
    @NSManaged public var category: YGTaskCategoryModel?

}
